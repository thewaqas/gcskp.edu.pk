<?php
/*
Plugin Name:  Custom Post Types (CPT) for gcskp
Plugin URI:   https://thewaqas.com/plugins
Description:  Adding <strong>Custom Post Types</strong> for theme Govt. P/G College Sheikhupura. Using Custom Post Types, we can create your own post type. It is not recommend that you place this functionality in your theme. This type of functionality should be placed/created in a plugin. This ensures the portability of your user’s content, and that if the theme is changed the content stored in the Custom Post Types won’t disappear.
Version:      1.0.0
Author:       Waqas A.
Author URI:   https://thewaqas.com/
License:      GPL2
License URI:  https://www.gnu.org/licenses/gpl-2.0.html
Text Domain:  _themename-_pluginname
Domain Path:  /languages
*/

if( !defined('WPINC')) {
    die;
}

// === Post Types === //


// Setup
define( 'CPT_PLUGIN_URL', __FILE__ );


// Includes
include_once( 'gcskp/post-types/slider.php'      );
include_once( 'gcskp/post-types/news.php'        );
include_once( 'gcskp/post-types/event.php'       );
include_once( 'gcskp/post-types/program.php'     );
include_once( 'gcskp/post-types/professor.php'   );
include_once( 'gcskp/post-types/faq.php'         );
include_once( 'gcskp/post-types/testimonial.php' );
include_once( 'gcskp/post-types/like.php'        );
include_once( 'gcskp/post-types/note.php'        );

// include_once( 'gcskp-post-types/portfolio-post-type.php');
// include_once( 'gcskp-post-types/project-taxonomie.php');
// include_once( 'gcskp-post-types/skills-taxonomie.php');


include_once('gcskp/post-types-init.php');



// Hooks
/* Hook into the 'init' action so that the function
* Containing our post type registration is not 
* unnecessarily executed. 
*/
add_action('init', 'gcskp_custom_post_types_init');
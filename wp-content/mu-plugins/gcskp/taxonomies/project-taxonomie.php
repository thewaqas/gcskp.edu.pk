<?php

function _themename__pluginname_register_post_type_taxonomie(){

    // Set UI labels for Custom Post Type
    $labels = array(
        'name'                => _x( 'Project',         'Project Type General Name',    '_themename' ),
        'singular_name'       => _x( 'Project Type',    'Post Type Singular Name',      '_themename' ),
        'menu_name'           => __( 'Project Types', '_themename' ),
        'parent_item_colon'   => __( 'Parent Project Type', '_themename' ),
        'all_items'           => __( 'All Projects',        '_themename' ),
        'view_item'           => __( 'View Project Types',  '_themename' ),
        'add_new_item'        => __( 'Add New Project Type','_themename' ),
        'add_new'             => __( 'Add New',             '_themename' ),
        'edit_item'           => __( 'Edit Project Type',   '_themename' ),
        'update_item'         => __( 'Update Project Type', '_themename' ),
        'search_items'        => __( 'Search Project Type', '_themename' ),
        'not_found'           => __( 'Not Found',           '_themename' ),
        'not_found_in_trash'  => __( 'Not found in Trash',  '_themename' ),
    );
    $args = array(
        'label'                 => __( 'Project', '_themename' ),
        'description'           => __( 'Project List and All Projects', '_themename' ),
        'labels'                => $labels,
        'hierarchical'          => true,
        'show_admin_coloumn'    => true,
        'rewrite'       => array ( 'slug' => 'project_type' ), 
    );
    register_taxonomy( '_themename_project_type', ['_themename_portfolio'], $args );
}

add_action( 'init', '_themename__pluginname_register_post_type_taxonomie' );
<?php

function _themename__pluginname_register_skills_taxonomie(){

    // Set UI labels for Custom taxonomie
    $labels = array(
        'name'                => _x( 'Skills', 'Skill General Name', '_themename' ),
        'singular_name'       => _x( 'Skill', 'Taxonomie Singular Name', '_themename' ),
        'menu_name'           => __( 'Skills', '_themename' ),
        'parent_item_colon'   => __( 'Parent Skill', '_themename' ),
        'all_items'           => __( 'All Skills', '_themename' ),
        'view_item'           => __( 'View Skills', '_themename' ),
        'add_new_item'        => __( 'Add New Skill', '_themename' ),
        'add_new'             => __( 'Add New', '_themename' ),
        'edit_item'           => __( 'Edit Skill', '_themename' ),
        'update_item'         => __( 'Update Skill', '_themename' ),
        'search_items'        => __( 'Search Skill', '_themename' ),
        'not_found'           => __( 'Not Found', '_themename' ),
        'not_found_in_trash'  => __( 'Not found in Trash', '_themename' ),
    );
    $args = array(
        'label'                 => __( 'Skill', '_themename' ),
        'description'           => __( 'Skill List and All Skills', '_themename' ),
        'labels'                => $labels,
        'hierarchical'          => false,
        'show_admin_coloumn'    => true,
        'rewrite'       => array ( 'slug' => 'skills' ), 
    );
    register_taxonomy( '_themename_skills', ['_themename_portfolio'], $args );
}

add_action( 'init', '_themename__pluginname_register_skills_taxonomie' );
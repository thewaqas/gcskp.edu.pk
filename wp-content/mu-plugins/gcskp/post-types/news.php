<?php

// News Post Type
function gcskp_news_post_type() {

     // Set UI labels for Custom Post Type
     $labels = array(
        'name'                  => _x( 'News',  'Post Type General Name',   '__TP' ),
        'singular_name'         => _x( 'News',  'Post Type Singular Name',  '__TP' ),
        'menu_name'             => __( 'News',               '__TP' ),
        'parent_item_colon'     => __( 'Parent News',        '__TP' ),
        'all_items'             => __( 'All News',           '__TP' ),
        'view_item'             => __( 'View News',          '__TP' ),
        'add_new_item'          => __( 'Add New News',       '__TP' ),
        'add_new'               => __( 'Add New News',       '__TP' ),
        'edit_item'             => __( 'Edit News',          '__TP' ),
        'update_item'           => __( 'Update News',        '__TP' ),
        'search_items'          => __( 'Search News',        '__TP' ),
        'not_found'             => __( 'Not Found',                 '__TP' ),
        'not_found_in_trash'    => __( 'Not found in Trash',        '__TP' ),
    );
    // Set other options for Custom Post Type
    $args = array(
        'labels'                => $labels,
        'description'           => __( 'These are the posts for News',   '__TP' ),
        'public'                => true,
        'publicly_queryable'    => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'query_var'             => true,
        'rewrite'               => array ( 'slug' => 'news' ),     
        'capability'            => 'post',
        'has_archive'           => true,
        'menu_position'         => 40,
        'menu_icon'             => 'dashicons-welcome-widgets-menus',
        'supports'              => array('title', 'excerpt', 'thumbnail', 'editor'),
        'taxonomies'            => array( 'category', 'post_tag' ),
        'show_in_rest'          => true
    );
    // Registering your Custom Post Type
    register_post_type( 'news', $args );

}
<?php

// Like Post Type
function gcskp_like_post_type() {

    
	register_post_type( 'like', array(
		'supports' => array('title'),
		'public' => false,
		'show_ui' => true,
		'menu_icon'=> 'dashicons-heart',
		'labels' => array(
			'name' => 'Likes',
			'add_new_item' => 'Add New Like',
			'edit_item' => 'Edit Like',
			'all_items' => 'All Likes',
			'singular_name' => 'Like'
		),
		
	));

}

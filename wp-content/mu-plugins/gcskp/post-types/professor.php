<?php

// Professors Post Type
function gcskp_professor_post_type() {

     // Set UI labels for Custom Post Type
     $labels = array(
        'name'                  => _x( 'Professors',  'Post Type General Name',   '__TP' ),
        'singular_name'         => _x( 'Professor',   'Post Type Singular Name',  '__TP' ),
        'menu_name'             => __( 'Professors',              '__TP' ),
        'parent_item_colon'     => __( 'Parent Professor',        '__TP' ),
        'all_items'             => __( 'All Professors',          '__TP' ),
        'view_item'             => __( 'View Professor',          '__TP' ),
        'add_new_item'          => __( 'Add New Professor',       '__TP' ),
        'add_new'               => __( 'Add New Professor',       '__TP' ),
        'edit_item'             => __( 'Edit Professor',          '__TP' ),
        'update_item'           => __( 'Update Professor',        '__TP' ),
        'search_items'          => __( 'Search Professor',        '__TP' ),
        'not_found'             => __( 'Not Found',             '__TP' ),
        'not_found_in_trash'    => __( 'Not found in Trash',    '__TP' ),
    );
    // Set other options for Custom Post Type
    $args = array(
        'labels'                => $labels,
        'description'           => __( 'These are the posts for Professors',   '__TP' ),
        'public'                => true,
        'publicly_queryable'    => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'query_var'             => true,
        'rewrite'               => array ( 'slug' => 'professors' ),     
        'capability_type'       => 'post',
        'map_meta_cap'          => true,
        'has_archive'           => true,
        'menu_position'         => 40,
        'menu_icon'             => 'dashicons-welcome-learn-more',
        'supports'              => array('title', 'excerpt', 'thumbnail', 'editor'),
        // 'taxonomies'            => array( 'category', 'post_tag' ),
        'show_in_rest'          => true
    );
    // Registering your Custom Post Type
    register_post_type( 'professor', $args );

}

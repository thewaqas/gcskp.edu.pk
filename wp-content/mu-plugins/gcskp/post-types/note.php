<?php

// Note Post Type
function gcskp_note_post_type() {

     
	register_post_type( 'note', array(
		'capability_type' => 'post',
		'map_meta_cap' => true,
		'show_in_rest' => true,
		'supports' => array('title', 'editor'),
		'public' => false,
		'show_ui' => true,
		'menu_icon'=> 'dashicons-welcome-write-blog',
		'labels' => array(
			'name' => 'Notes',
			'add_new_item' => 'Add New Note',
			'edit_item' => 'Edit Note',
			'all_items' => 'All Notes',
			'singular_name' => 'Note'
		),
		
	));

}

<?php

// FAQs Post Type
function gcskp_faq_post_type() {

     // Set UI labels for Custom Post Type
     $labels = array(
        'name'                  => _x( 'FAQs',  'Post Type General Name',   '__TP' ),
        'singular_name'         => _x( 'FAQ',   'Post Type Singular Name',  '__TP' ),
        'menu_name'             => __( 'FAQs',              '__TP' ),
        'parent_item_colon'     => __( 'Parent FAQ',        '__TP' ),
        'all_items'             => __( 'All FAQs',          '__TP' ),
        'view_item'             => __( 'View FAQ',          '__TP' ),
        'add_new_item'          => __( 'Add New FAQ',       '__TP' ),
        'add_new'               => __( 'Add New FAQ',       '__TP' ),
        'edit_item'             => __( 'Edit FAQ',          '__TP' ),
        'update_item'           => __( 'Update FAQ',        '__TP' ),
        'search_items'          => __( 'Search FAQ',        '__TP' ),
        'not_found'             => __( 'Not Found',             '__TP' ),
        'not_found_in_trash'    => __( 'Not found in Trash',    '__TP' ),
    );
    // Set other options for Custom Post Type
    $args = array(
        'labels'                => $labels,
        'description'           => __( 'These are the posts for FAQs',   '__TP' ),
        'public'                => true,
        'publicly_queryable'    => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'query_var'             => true,
        'rewrite'               => array ( 'slug' => 'faq' ),     
        // 'capability_type'       => 'page',
        // 'map_meta_cap'          => true,
        'has_archive'           => false,
        'menu_position'         => 40,
        'menu_icon'             => 'dashicons-feedback',
        'supports'              => array('title', 'editor'),
        // 'taxonomies'            => array( 'category', 'post_tag' ),
        'show_in_rest'          => true
    );
    // Registering your Custom Post Type
    register_post_type( 'faq', $args );

}

<?php

// Testimonial Post Type
function gcskp_testimonial_post_type() {

     // Set UI labels for Custom Post Type
     $labels = array(
        'name'                  => _x( 'Testimonials',  'Post Type General Name',   '__TP' ),
        'singular_name'         => _x( 'Testimonial',   'Post Type Singular Name',  '__TP' ),
        'menu_name'             => __( 'Testimonial',               '__TP' ),
        'parent_item_colon'     => __( 'Parent Testimonial',        '__TP' ),
        'all_items'             => __( 'All Testimonial',           '__TP' ),
        'view_item'             => __( 'View Testimonial',          '__TP' ),
        'add_new_item'          => __( 'Add New Testimonial',       '__TP' ),
        'add_new'               => __( 'Add New Testimonial',       '__TP' ),
        'edit_item'             => __( 'Edit Testimonial',          '__TP' ),
        'update_item'           => __( 'Update Testimonial',        '__TP' ),
        'search_items'          => __( 'Search Testimonial',        '__TP' ),
        'not_found'             => __( 'Not Found',                 '__TP' ),
        'not_found_in_trash'    => __( 'Not found in Trash',        '__TP' ),
    );
    // Set other options for Custom Post Type
    $args = array(
        'labels'                => $labels,
        'description'           => __( 'These are the posts for Testimonials',   '__TP' ),
        'public'                => true,
        'publicly_queryable'    => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'query_var'             => true,
        'rewrite'               => array ( 'slug' => 'testimonial' ),     
        'capability'            => 'post',
        'has_archive'           => true,
        'menu_position'         => 40,
        'menu_icon'             => 'dashicons-testimonial',
        'supports'              => array('title', 'excerpt', 'thumbnail', 'editor'),
        // 'taxonomies'            => array( 'category', 'post_tag' ),
        'show_in_rest'          => true
    );
    // Registering your Custom Post Type
    register_post_type( 'testimonial', $args );

}

<?php

// Events Post Type
function gcskp_event_post_type() {

     // Set UI labels for Custom Post Type
     $labels = array(
        'name'                  => _x( 'Events',  'Post Type General Name',   '__TP' ),
        'singular_name'         => _x( 'event',  'Post Type Singular Name',  '__TP' ),
        'menu_name'             => __( 'Events',               '__TP' ),
        'parent_item_colon'     => __( 'Parent Event',        '__TP' ),
        'all_items'             => __( 'All Events',           '__TP' ),
        'view_item'             => __( 'View Event',          '__TP' ),
        'add_new_item'          => __( 'Add New Event',       '__TP' ),
        'add_new'               => __( 'Add New Event',       '__TP' ),
        'edit_item'             => __( 'Edit Event',          '__TP' ),
        'update_item'           => __( 'Update Event',        '__TP' ),
        'search_items'          => __( 'Search Event',        '__TP' ),
        'not_found'             => __( 'Not Found',                 '__TP' ),
        'not_found_in_trash'    => __( 'Not found in Trash',        '__TP' ),
    );
    // Set other options for Custom Post Type
    $args = array(
        'labels'                => $labels,
        'description'           => __( 'These are the posts for Events',   '__TP' ),
        'public'                => true,
        'publicly_queryable'    => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'query_var'             => true,
        'rewrite'               => array ( 'slug' => 'events' ),     
        'capability_type'       => 'post',
        'has_archive'           => true,
        'menu_position'         => 40,
        'menu_icon'             => 'dashicons-calendar',
        'supports'              => array('title', 'excerpt', 'thumbnail', 'editor'),
        'taxonomies'            => array( 'category', 'post_tag' ),
        'show_in_rest'          => true
    );
    // Registering your Custom Post Type
    register_post_type( 'event', $args );

}

<?php

/*
* Creating a function to create our CPT
*/
function gcskp_custom_post_types_init()
{
    // 1) Hero Slider
    gcskp_slider_post_type();

	// 2) News
	gcskp_news_post_type();

	// 3) Events
	gcskp_event_post_type();
	
	// 4) Programs
	gcskp_program_post_type();
	
	// 5) Professors
	gcskp_professor_post_type();
	
	// 6) FAQ
	gcskp_faq_post_type();
	
	// 7) Testimonial
	gcskp_testimonial_post_type();
	
	// 8) Like
	gcskp_like_post_type();
	
	// 9) Note
	gcskp_note_post_type();

}
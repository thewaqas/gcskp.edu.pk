<?php 
/*
Plugin Name: Theme Custom Taxonomies
Description: This plugin <strong>create all custom taxonomies in website</strong>.
Author: Muhammad Waqas
Author URI: https://www.thewaqas.com/
Version: 1.0
License: GPLv3
License URI: https://www.gnu.org/licenses/gpl-3.0.html
*/

//hook into the init action and call create_book_taxonomies when it fires
 
add_action( 'init', 'create_subjects_hierarchical_taxonomy', 0 );
 
//create a custom taxonomy name it subjects for your posts
 
function create_subjects_hierarchical_taxonomy() {
 
// Add new taxonomy, make it hierarchical like categories
//first do the translations part for GUI
 
  $labels = array(
    'name' => _x( 'Subjects', 'taxonomy general name' ),
    'singular_name' => _x( 'Subject', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Subjects' ),
    'all_items' => __( 'All Subjects' ),
    'parent_item' => __( 'Parent Subject' ),
    'parent_item_colon' => __( 'Parent Subject:' ),
    'edit_item' => __( 'Edit Subject' ), 
    'update_item' => __( 'Update Subject' ),
    'add_new_item' => __( 'Add New Subject' ),
    'new_item_name' => __( 'New Subject Name' ),
    'menu_name' => __( 'Subjects' ),
  );    
 
// Now register the taxonomy
  register_taxonomy('subjects', array('programs'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_in_rest' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'programs' ),
  ));
 
}





	
/**
 * Create two taxonomies, genres and writers for the post type "book".
 *
 * @see register_post_type() for registering custom post types.
 */
function wpdocs_create_book_taxonomies() {
    // Add new taxonomy, make it hierarchical (like categories)
    $labels = array(
        'name'              => _x( 'Genres', 'taxonomy general name', 'gcskp' ),
        'singular_name'     => _x( 'Genre', 'taxonomy singular name', 'gcskp' ),
        'search_items'      => __( 'Search Genres', 'gcskp' ),
        'all_items'         => __( 'All Genres', 'gcskp' ),
        'parent_item'       => __( 'Parent Genre', 'gcskp' ),
        'parent_item_colon' => __( 'Parent Genre:', 'gcskp' ),
        'edit_item'         => __( 'Edit Genre', 'gcskp' ),
        'update_item'       => __( 'Update Genre', 'gcskp' ),
        'add_new_item'      => __( 'Add New Genre', 'gcskp' ),
        'new_item_name'     => __( 'New Genre Name', 'gcskp' ),
        'menu_name'         => __( 'Genre', 'gcskp' ),
    );
 
    $args = array(
        'hierarchical'      => true,
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'programs' ),
    );
 
    register_taxonomy( 'genre', array( 'programs' ), $args );
 
    unset( $args );
    unset( $labels );
 
    // Add new taxonomy, NOT hierarchical (like tags)
    $labels = array(
        'name'                       => _x( 'Writers', 'taxonomy general name', 'gcskp' ),
        'singular_name'              => _x( 'Writer', 'taxonomy singular name', 'gcskp' ),
        'search_items'               => __( 'Search Writers', 'gcskp' ),
        'popular_items'              => __( 'Popular Writers', 'gcskp' ),
        'all_items'                  => __( 'All Writers', 'gcskp' ),
        'parent_item'                => null,
        'parent_item_colon'          => null,
        'edit_item'                  => __( 'Edit Writer', 'gcskp' ),
        'update_item'                => __( 'Update Writer', 'gcskp' ),
        'add_new_item'               => __( 'Add New Writer', 'gcskp' ),
        'new_item_name'              => __( 'New Writer Name', 'gcskp' ),
        'separate_items_with_commas' => __( 'Separate writers with commas', 'gcskp' ),
        'add_or_remove_items'        => __( 'Add or remove writers', 'gcskp' ),
        'choose_from_most_used'      => __( 'Choose from the most used writers', 'gcskp' ),
        'not_found'                  => __( 'No writers found.', 'gcskp' ),
        'menu_name'                  => __( 'Writers', 'gcskp' ),
    );
 
    $args = array(
        'hierarchical'          => false,
        'labels'                => $labels,
        'show_ui'               => true,
        'show_admin_column'     => true,
        'update_count_callback' => '_update_post_term_count',
        'query_var'             => true,
        'rewrite'               => array( 'slug' => 'programs' ),
    );
 
    register_taxonomy( 'writer', 'programs', $args );
}
// hook into the init action and call create_book_taxonomies when it fires
add_action( 'init', 'wpdocs_create_book_taxonomies', 0 );
<?php
/*
Plugin Name:  _themename _pluginname
Plugin URI:   http://thewaqas.com/plugins/
Description:  This is an <strong>Custom Widgets </strong>  for _themename
Version:      1.0.0
Author:       Waqas A.
Author URI:   http://thewaqas.com/
License:      GPL2
License URI:  https://www.gnu.org/licenses/gpl-2.0.html
Text Domain:  __TP
Domain Path:  /languages
*/
//  __TP =  _themename-_pluginname

if( !defined('WPINC')) {
    die;
}


// Setup ===========================================================================
define( 'PLUGIN_URL', __FILE__ );





// Includes ========================================================================
include ( 'includes/activate.php' );
// include ( 'includes/enqueue.php' );

include ( 'includes/cron.php' );
include ( 'includes/utility.php' );

include ( 'includes/widgets.php' );
include ( 'includes/widgets/top-post.php' );
include ( 'includes/widgets/recent-posts.php' );



// Hooks ===========================================================================
register_activation_hook  ( __FILE__, '__TP_ativate_widget_plugin'  );
register_deactivation_hook( __FILE__, '__TP_deativate_widget_plugin');

// add_action( 'wp_enqueue_scripts', '__TP_enqueue_styles',  100 );
// add_action( 'wp_enqueue_scripts', '__TP_enqueue_scripts', 100 );

add_action( 'widgets_init', '__TP_widget_init' );
add_action( '__TP_top_post_hook', '__TP_generate_top_post_hook' );




// Shortcodes =======================================================================



/**
 *  Normal Comments
 *  * Success Information
 *  ! This is an Alert message
 *  ? Info about the block
 *  TODO: Work to do
 *  @param myPram My parameteres for thsi function
 */

//  Normal Comments
//* Success Information
//! This is an Alert message
//? Info about the block
// TODO: Work to do
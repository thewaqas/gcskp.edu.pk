<?php

/*
/*   Creating a function to check the compatibilty before activation
*/

function __TP_ativate_widget_plugin() {

    // 5.8 < 5.0
    if( version_compare( get_bloginfo( 'version' ), '5.0', '<' ) ){
        wp_die( __( "You must update WordPress to use this plugin.", 'recipe' ) );
    }


    // !Cron Job Implimentation
    // ?wp_schedule_event
    wp_schedule_event( time(), 'daily', '__TP_top_post_hook' );

}
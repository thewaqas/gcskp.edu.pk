<?php


class __TP_Top_Post_Widget extends WP_Widget {

    /**
	 * Sets up the widgets name etc
	 */
	public function __construct() {
		$widget_ops = array( 
			'description' => 'Display Top Post for Today',
		);
		parent::__construct( '__TP_Top_Post_Widget', 'WS - Top Post', $widget_ops );
	}

	

	/**
	 * Outputs the options form on admin
	 *
	 * @param array $instance The widget options
	 */
	public function form( $instance ) {
        // outputs the options form on admin

        $default        =   ['title' => 'Top post of the day!' ];
        $instance       =   wp_parse_args( (array) $instance, $defaults );

        ?>

        <p>
            <label for="<?php echo $this->get_field_id('title') ?>">Title: </label>
            <input type="text" class="widefat" 
                id      = "<?php echo $this->get_field_id('title')  ?> " 
                name    = "<?php echo $this->get_field_name('title')?> "  
                value   = "<?php echo esc_attr( $instance['title'] )?> " 
            />
        </p>

        <?php
	}

	/**
	 * Processing widget options on save
	 *
	 * @param array $new_instance The new options
	 * @param array $old_instance The previous options
	 *
	 * @return array
	 */
	public function update( $new_instance, $old_instance ) {
		// processes widget options to be saved
		$instance 			= array();
        $instance['title'] 	=  strip_tags( $new_instance['title'] );
        return $instance;
    }
    


    /**
	 * Outputs the content of the widget
	 *
	 * @param array $args
	 * @param array $instance
	 */
	public function widget( $args, $instance ) {
		// outputs the content of the widget

		extract( $args );
		extract( $instance );

		$title		= apply_filters( 'widget_title', $title );

		echo $args['before_widget'];

		echo $args['before_title'] . $title . $args['after_title'];
		
		$post_id	= get_transient( '__TP_top_post' );

		if ( !$post_id ) {

			$post_id = __TP_get_random_post();

			set_transient( 
				'__TP_top_post',
				__TP_get_random_post(),
				DAY_IN_SECONDS
			);
		}
		
		?> 

		<div class="widget-body">
			<!-- Post simple-->
			<div class="post post-simple post-sm">
				<a class="post-img-link" href="<?php echo get_the_permalink( $post_id ) ?>">
					<!-- <img class="lazy-img" src="" data-src="" alt="" width="512" height="330" style=""> -->
					<?php echo get_the_post_thumbnail( $post_id, 'large' ) ?>
				</a>
				<h5 class="post-title">
					<a href="<?php echo get_the_permalink( $post_id ) ?>"><?php echo get_the_title( $post_id ) ?></a>
				</h5>
				<div class="post-text">At Intense, we offer cutting-edge web design solutions to the clients who are looking for creative web design solutions as well as for the ways of improving their website customization.</div>
				<a class="btn btn-secondary post-btn" href="<?php echo get_the_permalink( $post_id ) ?>">Read more</a>
			</div>
		</div>

		<?php

		

        echo $args['after_widget'];
    }
}
<?php

// *Query for random top post
function __TP_get_random_post() {

    global $wpdb;

    return $wpdb->get_var(
        "SELECT `ID` FROM `" . $wpdb->posts . "`
        WHERE post_ststus='publish' AND post_type='event'
        ORDER BY rand() LIMIT 1"
    );

}


// *Input sanatization for custom recent posts
function __TP_sanitize_sort_by( $input ) {

    $valid = array( 'date', 'rand', 'comment_count' );
    if( in_array( $input, $valid, true) ) {
        return $input;
    }
    return 'date';
}
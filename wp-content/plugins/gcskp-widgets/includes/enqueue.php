<?php

/*
*   Creating two function to enqueue Styles and Scripts
*/

//  function to enqueue Styles
function _themename__pluginname_enqueue_styles() {

    wp_register_style( '_themename-_pluginname-stylesheet' ,  
        plugins_url('/dist/assets/css/bundle.css', PLUGIN_URL), 
        array(), '1.0.0', 'all' );
    wp_register_style( '_themename-_pluginname-admin-stylesheet' ,  
        plugins_url('/dist/assets/css/admin.css', PLUGIN_URL), 
        array(), '1.0.0', 'all' );

    wp_enqueue_style( '_themename-_pluginname-stylesheet' );
    wp_enqueue_style( '_themename-_pluginname-admin-stylesheet' );
    
}

//  function to enqueue Scripts
function _themename__pluginname_enqueue_scripts() {

    wp_register_script( '_themename-_pluginname-scripts', 
        plugins_url( '/dist/assets/js/bundle.js', PLUGIN_URL),  
        array( 'jquery' ), '1.0.0', true);
    wp_register_script( '_themename-_pluginname-admin-scripts', 
        plugins_url( '/dist/assets/js/admin.js', PLUGIN_URL),  
        array( 'jquery' ), '1.0.0', true);

    wp_enqueue_script( '_themename-_pluginname-scripts' );
    wp_enqueue_script( '_themename-_pluginname-admin-scripts' );

}


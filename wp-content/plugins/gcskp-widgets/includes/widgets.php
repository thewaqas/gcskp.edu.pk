<?php

/**
 * Widget initilization and Registration
 */
function __TP_widget_init() {

    /* Top Post Widget */
    register_widget( '__TP_Top_Post_Widget' );

    /* Recent Post Widget */
    register_widget( '__TP_Recent_Post_widget' );
    
}

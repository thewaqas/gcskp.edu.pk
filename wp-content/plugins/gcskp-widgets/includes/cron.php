<?php

function __TP_generate_top_post_hook() {

    set_transient( 
        '__TP_top_post',
        __TP_get_random_post(),
        DAY_IN_SECONDS
    );

}
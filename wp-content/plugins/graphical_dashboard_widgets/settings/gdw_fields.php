<?php

/**
 * WordPress settings API demo class
 *
 * @author Tareq Hasan
 */
if ( !class_exists('Gdw_Settings_API_Test' ) ):
class Gdw_Settings_API_Test {

    private $settings_api;

    function __construct() {
        $this->settings_api = new Gdw_Settings_API;

        add_action( 'admin_init', array($this, 'admin_init') );
        add_action( 'admin_menu', array($this, 'admin_menu') );
    }

    function admin_init() {

        //set the settings
        $this->settings_api->set_sections( $this->get_settings_sections() );
        $this->settings_api->set_fields( $this->get_settings_fields() );

        //initialize settings
        $this->settings_api->admin_init();
    }

    function admin_menu() {
        add_menu_page( 'Dashboard Widgets', __('Dashboard Widgets','gdwlang'), 'delete_posts', 'gdw_settings', array($this, 'plugin_page') );
    }

    function get_settings_sections() {
        $sections = array(
            /*array(
                'id'    => 'gdwids_basics',
                'title' => __( 'Basic Settings', 'gdwlang' )
            ),*/
            array(
                'id'    => 'gdwids_options',
                'title' => __( 'Graphical Dashboard Widgets - Settings', 'gdwlang' )
            )
        );
        return $sections;
    }

    /**
     * Returns all the settings fields
     *
     * @return array settings fields
     */
    function get_settings_fields() {
        $settings_fields = array(
            /*'gdwids_basics' => array(
                
            ),*/
            'gdwids_options' => array(
                /*array(
                    'name'              => 'text_val',
                    'label'             => __( 'Text Input', 'gdwlang' ),
                    'desc'              => __( 'Text input description', 'gdwlang' ),
                    'placeholder'       => __( 'Text Input placeholder', 'gdwlang' ),
                    'type'              => 'text',
                    'default'           => 'Title',
                    'sanitize_callback' => 'sanitize_text_field'
                ),
                array(
                    'name'              => 'number_input',
                    'label'             => __( 'Number Input', 'gdwlang' ),
                    'desc'              => __( 'Number field with validation callback `floatval`', 'gdwlang' ),
                    'placeholder'       => __( '1.99', 'gdwlang' ),
                    'min'               => 0,
                    'max'               => 100,
                    'step'              => '0.01',
                    'type'              => 'number',
                    'default'           => 'Title',
                    'sanitize_callback' => 'floatval'
                ),
                array(
                    'name'        => 'textarea',
                    'label'       => __( 'Textarea Input', 'gdwlang' ),
                    'desc'        => __( 'Textarea description', 'gdwlang' ),
                    'placeholder' => __( 'Textarea placeholder', 'gdwlang' ),
                    'type'        => 'textarea'
                ),
                array(
                    'name'        => 'html',
                    'desc'        => __( 'HTML area description. You can use any <strong>bold</strong> or other HTML elements.', 'gdwlang' ),
                    'type'        => 'html'
                ),
                array(
                    'name'    => 'selectbox',
                    'label'   => __( 'A Dropdown', 'gdwlang' ),
                    'desc'    => __( 'Dropdown description', 'gdwlang' ),
                    'type'    => 'select',
                    'default' => 'no',
                    'options' => array(
                        'yes' => 'Yes',
                        'no'  => 'No'
                    )
                ),
                array(
                    'name'    => 'password',
                    'label'   => __( 'Password', 'gdwlang' ),
                    'desc'    => __( 'Password description', 'gdwlang' ),
                    'type'    => 'password',
                    'default' => ''
                ),
                array(
                    'name'    => 'file',
                    'label'   => __( 'File', 'gdwlang' ),
                    'desc'    => __( 'File description', 'gdwlang' ),
                    'type'    => 'file',
                    'default' => '',
                    'options' => array(
                        'button_label' => 'Choose Image'
                    )
                ),
                array(
                    'name'    => 'color',
                    'label'   => __( 'Color', 'gdwlang' ),
                    'desc'    => __( 'Color description', 'gdwlang' ),
                    'type'    => 'color',
                    'default' => ''
                ),

                array(
                    'name'  => 'checkbox',
                    'label' => __( 'Checkbox', 'gdwlang' ),
                    'desc'  => __( 'Checkbox Label', 'gdwlang' ),
                    'type'  => 'checkbox'
                ),*/

                array(
                    'name'    => 'dashboard-widget-colors',
                    'label'   => __( 'Pick Dashboard Widget Colors', 'gdwlang' ),
                    'desc'    => __( 'Pick colors for dashboard widgets. Pick <strong>minimum 6</strong> colors, else default plugin colors will be used.', 'gdwlang' ),
                    'type'    => 'text_color',
                    'default' => '#7986CB,#4dd0e1,#9575CD,#4FC3F7,#64B5F6,#4DB6AC'
                ),
                array(
                    'name'    => 'front-usertracking',
                    'label'   => __( 'Enable User Location Tracking', 'gdwlang' ),
                    'desc'    => __( 'Check ON to enable user tracking based on IP. This option records user IP, country, city and region etc., OFF to disable.', 'gdwlang' ),
                    'type'    => 'radio',
                    'default' => 'yes',
                    'options' => array(
                        'yes' => __('Yes', 'gdwlang' ),
                        'no'  => __('No', 'gdwlang' )
                    )
                ),/*
                array(
                    'name'    => 'password',
                    'label'   => __( 'Password', 'gdwlang' ),
                    'desc'    => __( 'Password description', 'gdwlang' ),
                    'type'    => 'password',
                    'default' => ''
                ),
                array(
                    'name'    => 'wysiwyg',
                    'label'   => __( 'Advanced Editor', 'gdwlang' ),
                    'desc'    => __( 'WP_Editor description', 'gdwlang' ),
                    'type'    => 'wysiwyg',
                    'default' => ''
                ),*/
                array(
                    'name'    => 'dashboard-widgets',
                    'label'   => __( 'Enable Dashboard Widgets', 'gdwlang' ),
                    'desc'    => __( 'Select the widgets to display on dashboard. These widgets are added by this plugin.', 'gdwlang' ),
                    'type'    => 'multicheck',
                    "default" => array(
                            "gdw_visitors_type" => "gdw_visitors_type",
                            "gdw_user_type" => "gdw_user_type",
                            "gdw_browser_type" => "gdw_browser_type",
                            "gdw_platform_type" => "gdw_platform_type",
                            "gdw_country_type" => "gdw_country_type",
                            "gdw_today_visitors" => "gdw_today_visitors",
                            "gdw_pagestats_add_dashboard" => "gdw_pagestats_add_dashboard",
                            "gdw_poststats_add_dashboard" => "gdw_poststats_add_dashboard",
                            "gdw_commentstats_add_dashboard" => "gdw_commentstats_add_dashboard",
                            "gdw_catstats_add_dashboard" => "gdw_catstats_add_dashboard",
                            "gdw_userstats_add_dashboard" => "gdw_userstats_add_dashboard",
                            "gdw_browser_type" => "gdw_browser_type",
                        ),
                    'options' => array(
                            'gdw_visitors_type' => __('Visitors in Last 15 days','gdwlang'),
                            'gdw_user_type' =>__('Users in last 15 days','gdwlang'),
                            'gdw_browser_type' => __('Browsers Used','gdwlang'),
                            'gdw_platform_type' => __('Platforms Used','gdwlang'),
                            'gdw_country_type' => __('Visits by Country','gdwlang'),
                            'gdw_today_visitors' => __('Today Page Views & Online Users','gdwlang'),

                            'gdw_pagestats_add_dashboard' => __('Pages Count & Type','gdwlang'),
                            'gdw_poststats_add_dashboard' => __('Posts Statistics','gdwlang'),
                            'gdw_commentstats_add_dashboard' => __('User Comments','gdwlang'),
                            'gdw_catstats_add_dashboard' => __('Category Statistics','gdwlang'),
                            'gdw_userstats_add_dashboard' => __('User Statistics','gdwlang'),
                    )
                ),



                array(
                    'name'    => 'dashboard-default-widgets',
                    'label'   => __( 'Check Default Dashboard Widgets', 'gdwlang' ),
                    'desc'    => __( 'Select the widgets to display on dashboard. This includes default dashboard widgets<br>Some default widgets might not be found in your current wordpress version.', 'gdwlang' ),
                    'type'    => 'multicheck',
                    "default" => array(
                            'welcome_panel' => 'welcome_panel',
                            'dashboard_primary' => 'dashboard_primary',
                            'dashboard_quick_press' => 'dashboard_quick_press',
                            'dashboard_recent_drafts' => 'dashboard_recent_drafts',
                            'dashboard_recent_comments' => 'dashboard_recent_comments',
                            'dashboard_right_now' => 'dashboard_right_now',
                            'dashboard_activity' => 'dashboard_activity',
                            'dashboard_incoming_links' => 'dashboard_incoming_links',
                            'dashboard_plugins' => 'dashboard_plugins',
                            'dashboard_secondary' => 'dashboard_secondary'
                        ),
                    'options' => array(
                            'welcome_panel' => __('Welcome Panel', 'gdwlang' ),
                            'dashboard_primary' => __('WordPress News', 'gdwlang' ),
                            'dashboard_quick_press' => __('Quick Draft', 'gdwlang' ),
                            'dashboard_recent_drafts' => __('Recent Drafts', 'gdwlang' ),
                            'dashboard_recent_comments' => __('Recent Comments', 'gdwlang' ),
                            'dashboard_right_now' => __('At a Glance', 'gdwlang' ),
                            'dashboard_activity' => __('Activity', 'gdwlang' ),
                            'dashboard_incoming_links' => __('Incoming Links', 'gdwlang' ),
                            'dashboard_plugins' => __('Plugins Widget', 'gdwlang' ),
                            'dashboard_secondary' => __('Secondary Widget', 'gdwlang' )
                    )
                ),
            )
        );

        return $settings_fields;
    }

    function plugin_page() {
        echo '<div class="wrap">';

        $this->settings_api->show_navigation();
        $this->settings_api->show_forms();

        echo '</div>';
    }

    /**
     * Get all the pages
     *
     * @return array page names with key value pairs
     */
    function get_pages() {
        $pages = get_pages();
        $pages_options = array();
        if ( $pages ) {
            foreach ($pages as $page) {
                $pages_options[$page->ID] = $page->post_title;
            }
        }

        return $pages_options;
    }

}
endif;

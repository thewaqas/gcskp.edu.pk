<?php

/*
 * Plugin Name: Student Email Rating
 * Description: This plugin extends the student plugin
 * 
 */


add_action( 'student_rated', function( $arr ) {

    $post       =   get_the_post( $arr['post_id'] );
    $usr_email  =   get_the_author_meta( 'user_email', $post->post_author );
    $subject    =   'Student receive new rating';
    $message    =   'Student'. $post->post_title .
                    'has receive a rating'. $arr['rating']. '.';
    wp_mail( $usr_email, $subject, $message );
});
// console.log( wp );

import block_icons from '../icons/index';
import './editor.scss';

const { registerBlockType }     =   wp.blocks;
const { __ }                    =   wp.i18n;
const { InspectorControls, 
        BlockControls,
        AlignmentToolbar,
        BlockAlignmentToolbar}  =   wp.blockEditor;
const { PanelBody, 
        PanelRow, 
        TextControl, 
        SelectControl }         =   wp.components;    

registerBlockType( 'gcskp/students', {
    title           :   __( 'Students', '__TP' ),
    description     :   __( 'Provides information about student', '__TP' ),
    // common, formatting, layout, widgets, embed
    category        :   'common',
    icon            :   block_icons.wapuu,
    keywords        : 
    [
        __( 'Students',     '__TP' ),
        __( 'Admission',    '__TP' ),
        __( 'User',         '__TP' )
    ], 
    supports    : 
    {
        html    :       false
    },
    attributes  : 
    {
        reg_no      : 
        { 
            type    :   'string',   //Optional   
            source  :   'text', 
            default :   '2016-KS-339',
            selector:   '.reg_no-ph'
        },
        roll_no     : 
        { 
            type    :   'string',   //Optional   
            source  :   'text', 
            default :   '2016-019522',
            selector:   '.roll_no-ph'
        },
        full_name   : 
        { 
            type    :   'string',   //Optional   
            source  :   'text', 
            default :   'Muhammad Waqas',
            selector:   '.full_name-ph'
        },
        department  : 
        { 
            type    :   'string',   //Optional   
            source  :   'text', 
            default :   'Computer Science',
            selector:   '.department-ph'
        },
        program     : 
        { 
            type    :   'string',   //Optional   
            source  :   'text', 
            default :   'BS Information Technology',
            selector:   '.program-ph'
        },
        text_align  :
        {
            type    :   'string',   //Optional 
        },
        block_align :
        {
            type    :   'string',   //Optional 
            default :   'wide'
        }
    },
    getEditWrapperProps : ( { block_align } ) => {
        if ( 'left' === block_align || 'right' === block_align || 'full' === block_align ) {
            return { 'data-align': block_align };
        }
    },
    edit        : ( props ) => {
        // console.log( props );
        // const updateIngredients = ( new_val ) => {
        //     props.setAttributes({ ingredients: new_val })
        // }
        const onChange_reg_no       = ( new_val ) => { props.setAttributes({ reg_no     : new_val }) }
        const onChange_roll_no      = ( new_val ) => { props.setAttributes({ roll_no    : new_val }) }
        const onChange_full_name    = ( new_val ) => { props.setAttributes({ full_name  : new_val }) }
        const onChange_department   = ( new_val ) => { props.setAttributes({ department : new_val }) }
        const onChange_program      = ( new_val ) => { props.setAttributes({ program    : new_val }) }
        const onChange_text_align   = ( new_val ) => { props.setAttributes({ text_align : new_val }) }
        const onChange_block_align  = ( new_val ) => { props.setAttributes({ block_align: new_val }) }

        return [
            <InspectorControls>
                <PanelBody title={ __( 'Basics', '__TP' ) }>
                    <PanelRow>
                        <p>{ __( 'Configure the contents of your block here.' ,'__TP' ) }</p>
                    </PanelRow>

                    <TextControl 
                        label   =   { __( 'Registration No', '__TP' ) } 
                        help    =   { __( 'Example: 2016-KS-339', '__TP' ) } 
                        value   =   { props.attributes.reg_no }
                        onChange=   { onChange_reg_no } 
                    />

                    <TextControl 
                        label   =   { __( 'Roll No', '__TP' ) } 
                        help    =   { __( 'Example: 2016-019522', '__TP' ) } 
                        value   =   { props.attributes.roll_no }
                        onChange=   { onChange_roll_no } 
                    />

                    <TextControl 
                        label   =   { __( 'Full Name', '__TP' ) } 
                        help    =   { __( 'Example: Muhammad Waqas', '__TP' ) } 
                        value   =   { props.attributes.full_name }
                        onChange=   { onChange_full_name } 
                    />

                    <SelectControl 
                        label   =   { __( 'Department', '__TP' ) }
                        help    =   { __( 'Select the dapartment of the student', '__TP' ) }
                        value   =   { props.attributes.department } 
                        options =   { [
                            { value: 'Computer Science',    label: 'Computer Science'   },
                            { value: 'Physics',             label: 'Physics'            },
                            { value: 'Chemistry',           label: 'Chemistry'          },
                            { value: 'Zoology',             label: 'Zoology'            },
                            { value: 'English',             label: 'English'            },
                            { value: 'Mathamatics',         label: 'Mathamatics'        }
                        ] }
                        onChange={ onChange_department } 
                    />


                    <SelectControl 
                        label   =   { __( 'Program', '__TP' ) }
                        help    =   { __( 'Select the Program of the student', '__TP' ) }
                        value   =   { props.attributes.program } 
                        options =   { [
                            { value: 'BS (IT)',         label: 'BS ( IT )'     },
                            { value: 'BS Physics',      label: 'BS Physics'     },
                            { value: 'BS Chemistry',    label: 'BS Chemistry'   },
                            { value: 'BS Zoology',      label: 'BS Zoology'     },
                            { value: 'BS English',      label: 'BS English'     },
                            { value: 'BS Mathamatics',  label: 'BS Mathamatics' }
                        ] }
                        onChange={ onChange_program } 
                    />
                    
            
                </PanelBody>
            </InspectorControls>,
            <div className={ props.className }>
                <BlockControls>
                    <AlignmentToolbar 
                        value   =   { props.attributes.text_align }
                        onChange=   { onChange_text_align } 
                    />
                    <BlockAlignmentToolbar 
                        value   =   { props.attributes.block_align }
                        onChange=   { onChange_block_align } 
                    />
                </BlockControls>
                <ul class="list-unstyled" style={ { textAlign: props.attributes.text_align } }>
                    <li>
                        <strong>{ __( 'Reg No',     '__TP' ) }: </strong> 
                        <span className="reg_no-ph">    { props.attributes.reg_no     } </span>
                    </li>
                    <li>
                        <strong>{ __( 'Roll No',    '__TP' ) }: </strong> 
                        <span className="roll_no-ph">   { props.attributes.roll_no    } </span>
                    </li>
                    <li>
                        <strong>{ __( 'Full Name',  '__TP' ) }: </strong> 
                        <span className="full_name-ph"> { props.attributes.full_name  } </span>
                    </li>
                    <li>
                        <strong>{ __( 'Department', '__TP' ) }: </strong> 
                        <span className="department-ph">{ props.attributes.department } </span>
                    </li>
                    <li>
                        <strong>{ __( 'Program',    '__TP' ) }: </strong> 
                        <span className="program-ph">   { props.attributes.program    } </span>
                    </li>
                </ul>
            </div>
        ];
    },
    save        : ( props ) => {
        return (    
            <div class={ `align${props.attributes.block_align}` } >
                <ul class="list-unstyled" style={ { textAlign: props.attributes.text_align } }>
                    <li>
                        <strong>{ __( 'Reg No',     '__TP' ) }: </strong> 
                        <span className="reg_no-ph">    { props.attributes.reg_no     } </span>
                    </li>
                    <li>
                        <strong>{ __( 'Roll No',    '__TP' ) }: </strong> 
                        <span className="roll_no-ph">   { props.attributes.roll_no    } </span>
                    </li>
                    <li>
                        <strong>{ __( 'Full Name',  '__TP' ) }: </strong> 
                        <span className="full_name-ph"> { props.attributes.full_name  } </span>
                    </li>
                    <li>
                        <strong>{ __( 'Department', '__TP' ) }: </strong> 
                        <span className="department-ph">{ props.attributes.department } </span>
                    </li>
                    <li>
                        <strong>{ __( 'Program',    '__TP' ) }: </strong> 
                        <span className="program-ph">   { props.attributes.program    } </span>
                    </li>
                </ul>
            </div>
        )
    }

});
import classnames   from 'classnames';
import block_icons  from '../icons/index';
import btn_icon     from './icon';
import './editor.scss';

/*
* myMethods
* * Impotant
TODO: Word to do
? API
!Des
* INfo
@param 

*/ 
//TODO: Word to do
//? API
//!Des
//* INfo
//@param 

const { registerBlockType }         =   wp.blocks;
const { __ }                        =   wp.i18n;
const { 
    BlockControls ,
    InspectorControls }             =   wp.blockEditor;
const { 
    PanelBody, 
    PanelRow,
    FormToggle,
    ToolbarGroup , 
    ToolbarButton , 
    Tooltip  }                      =   wp.components;

registerBlockType( 'gcskp/night-mode', {
    title:                              __( 'Night Mode', '__TP' ),
    description:                        __( 'Content with night mode.', '__TP'),
    category:                           'common',
    icon:                               block_icons.wapuu,
    attributes  : {
        night_mode  :   {
            type    :                   'boolean',
            default :                   false
        }
    },
    edit: ( props ) => { 

        const toggle_night_mode = () => { props.setAttributes({night_mode: !props.attributes.night_mode}) }

        return [
            <InspectorControls>
                <PanelBody title={ __( 'Night Mode', '__TP')}>
                    <PanelRow>
                    <label htmlFor="gcskp-student-night-mode-toggle">
                            { __( 'Night Mode', '__TP') }
                        </label>
                        <FormToggle id='gcskp-student-night-mode-toggle'
                                    checked={ props.attributes.night_mode }
                                    onChange={ toggle_night_mode } />
                    </PanelRow>
                    
                </PanelBody>
            </InspectorControls>,
            <div className={ props.className }>
                <BlockControls>
                    <ToolbarGroup>
                        <Tooltip text={ __( 'Night Mode', '__TP' ) }>
                            <ToolbarButton 
                                className={ classnames(
                                        'components-icon-button', 
                                        'components-tooltip__control',
                                        { 'is-active': props.attributes.night_mode }
                                    )}
                                onClick={ toggle_night_mode }>
                                    { btn_icon }

                            </ToolbarButton>
                        </Tooltip>
                    </ToolbarGroup>
                </BlockControls>
                <div className={ classnames(
                        'content-example', 
                        { 'night': props.attributes.night_mode }
                )}>
                    This is an example of a block with night mode.
                </div>
            </div>
        ]
    },
    save: ( props ) => {
        return ( 
            <div>
                <div className = { classnames(
                        'content-example', 
                        { 'night': props.attributes.night_mode }
                    )}>
                    This is an example of a block with night mode.
                </div>
            </div>
        )
    },

});

<?php 


function __TP_enqueue_block_editor_assets() {

    wp_register_script( '__TP-blocks-bundle', 
        plugins_url( '/blocks/dist/bundle.js', STUDENTS_PLUGIN_URL ),  
        array( 
            'wp-i18n', 
            'wp-element', 
            'wp-blocks', 
            'wp-components', 
            'wp-editor', 
            'wp-api'
        ),
        filemtime( plugin_dir_path( STUDENTS_PLUGIN_URL ) . '/blocks/dist/bundle.js' )
    );

    wp_enqueue_script( '__TP-blocks-bundle' );

    
}


function __TP_enqueue_block_assets() {

    wp_register_style( '__TP-blocks-assets', 
        plugins_url( '/blocks/dist/blocks-main.css', STUDENTS_PLUGIN_URL )
    );

    wp_enqueue_style( '__TP-blocks-assets' );

    
}
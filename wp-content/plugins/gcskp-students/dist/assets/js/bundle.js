/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/assets/js/bundle.js":
/*!*********************************!*\
  !*** ./src/assets/js/bundle.js ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports) {

(function ($) {
  $("#student_rating").bind('rated', function () {
    $(this).rateit('readonly', true);
    var form = {
      action: '__TP_rate_student',
      rid: $(this).data('rid'),
      rating: $(this).rateit('value')
    };
    $.post(student_obj.ajax_url, form, function (data) {});
  });
  var featured_frame = wp.media({
    title: 'Select or Upload Media',
    button: {
      text: 'Use this media'
    },
    multiple: false
  });
  featured_frame.on('select', function () {
    var attachment = featured_frame.state().get('selection').first().toJSON();
    $("#student-img-preview").attr('src', attachment.url);
    $("#__TP_inputImgID").val(attachment.id);
  });
  $(document).on('click', '#student-img-upload-btn', function (e) {
    e.preventDefault();
    featured_frame.open();
  });
  $("#student-form").on('submit', function (e) {
    e.preventDefault();
    $(this).hide();
    document.querySelector("#notifications-area").notification({
      text: "Please wait! We are submiting your data",
      icon: '<span class="icon snackbar-icon novi-icon fa-circle-o-notch fa-spin"></span>',
      cls: "snackbar-secondary"
    });
    $("#student-status").html("<div class=\"snackbar snackbar-secondary\">\n                <div class=\"snackbar-inner\">\n                    <div class=\"snackbar-title\" id=\"student-status\">\n                        <span class=\"icon snackbar-icon novi-icon fa-circle-o-notch fa-spin\"></span>\n                        Please wait! We are submiting your data\n                    </div>\n                </div>\n            </div>");
    var form = {
      action: '__TP_submit_student_data',
      title: $("#__TP_input_title").val(),
      content: tinymce.activeEditor.getContent(),
      attachment_id: $("#__TP_inputImgID").val()
    };
    console.log('working');
    $.post(student_obj.ajax_url, form, function (data) {
      if (data.status == 2) {
        $("#student-status").html("<div class=\"snackbar snackbar-primary\">\n                        <div class=\"snackbar-inner\">\n                            <div class=\"snackbar-title\">\n                                <span class=\"icon snackbar-icon novi-icon int-check\"></span>\n                                Submitted Successfully\n                            </div>\n                        </div>\n                    </div>");
      } else {
        $("#student-status").html("<div class=\"snackbar snackbar-danger\">\n                        <div class=\"snackbar-inner\">\n                            <div class=\"snackbar-title\">\n                                <span class=\"icon snackbar-icon novi-icon int-warning\"></span>\n                                Form submission failed\n                            </div>\n                        </div>\n                    </div>");
        $("#student-form").show();
      }
    });
  });
  $("#register-form").on('submit', function (e) {
    e.preventDefault();
    $(this).hide();
    document.querySelector("#notifications-area").notification({
      text: "Processing........",
      icon: '<span class="icon snackbar-icon novi-icon fa-circle-o-notch fa-spin"></span>',
      cls: "snackbar-secondary"
    });
    var form = {
      _wpnonce: $('#_wpnonce').val(),
      action: '__TP_create_account',
      name: $("#register-form-name").val(),
      username: $("#register-form-username").val(),
      email: $("#register-form-email").val(),
      pass: $("#register-form-pass").val(),
      confirm_pass: $("#register-form-confirm_pass").val()
    };
    $.post(student_obj.ajax_url, form, function (data) {
      if (data.status == 2) {
        document.querySelector("#notifications-area").notification({
          text: "Account Created !",
          icon: '<span class="icon snackbar-icon novi-icon int-check"></span>',
          cls: "snackbar-primary"
        });
        location.href = student_obj.home_url;
      } else {
        document.querySelector("#notifications-area").notification({
          text: "Unable to create an account",
          icon: '<span class="icon snackbar-icon novi-icon int-warning"></span>',
          cls: "snackbar-danger"
        });
        $("#register-form").show();
      }
    });
  });
  $("#login-form").on('submit', function (e) {
    e.preventDefault();
    $(this).hide();
    document.querySelector("#notifications-area").notification({
      text: "Processing........",
      icon: '<span class="icon snackbar-icon novi-icon fa-circle-o-notch fa-spin"></span>',
      cls: "snackbar-secondary"
    });
    var form = {
      _wpnonce: $('#_wpnonce').val(),
      action: '__TP_user_login',
      username: $("#login-form-username").val(),
      pass: $("#login-form-password").val()
    };
    $.post(student_obj.ajax_url, form, function (data) {
      if (data.status == 2) {
        document.querySelector("#notifications-area").notification({
          text: "Login Successfully !",
          icon: '<span class="icon snackbar-icon novi-icon int-check"></span>',
          cls: "snackbar-primary"
        });
        location.href = student_obj.home_url;
      } else {
        document.querySelector("#notifications-area").notification({
          text: "Unable to login",
          icon: '<span class="icon snackbar-icon novi-icon int-warning"></span>',
          cls: "snackbar-danger"
        });
        $("#login-form").show();
      }
    });
  });
})(jQuery);

/***/ }),

/***/ 1:
/*!***************************************!*\
  !*** multi ./src/assets/js/bundle.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! D:\Local Sites\gcskp\app\public\wp-content\plugins\gcskp-students\src\assets\js\bundle.js */"./src/assets/js/bundle.js");


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vc3JjL2Fzc2V0cy9qcy9idW5kbGUuanMiXSwibmFtZXMiOlsiJCIsImJpbmQiLCJyYXRlaXQiLCJmb3JtIiwiYWN0aW9uIiwicmlkIiwiZGF0YSIsInJhdGluZyIsInBvc3QiLCJzdHVkZW50X29iaiIsImFqYXhfdXJsIiwiZmVhdHVyZWRfZnJhbWUiLCJ3cCIsIm1lZGlhIiwidGl0bGUiLCJidXR0b24iLCJ0ZXh0IiwibXVsdGlwbGUiLCJvbiIsImF0dGFjaG1lbnQiLCJzdGF0ZSIsImdldCIsImZpcnN0IiwidG9KU09OIiwiYXR0ciIsInVybCIsInZhbCIsImlkIiwiZG9jdW1lbnQiLCJlIiwicHJldmVudERlZmF1bHQiLCJvcGVuIiwiaGlkZSIsInF1ZXJ5U2VsZWN0b3IiLCJub3RpZmljYXRpb24iLCJpY29uIiwiY2xzIiwiaHRtbCIsImNvbnRlbnQiLCJ0aW55bWNlIiwiYWN0aXZlRWRpdG9yIiwiZ2V0Q29udGVudCIsImF0dGFjaG1lbnRfaWQiLCJjb25zb2xlIiwibG9nIiwic3RhdHVzIiwic2hvdyIsIl93cG5vbmNlIiwibmFtZSIsInVzZXJuYW1lIiwiZW1haWwiLCJwYXNzIiwiY29uZmlybV9wYXNzIiwibG9jYXRpb24iLCJocmVmIiwiaG9tZV91cmwiLCJqUXVlcnkiXSwibWFwcGluZ3MiOiI7UUFBQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTs7O1FBR0E7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLDBDQUEwQyxnQ0FBZ0M7UUFDMUU7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSx3REFBd0Qsa0JBQWtCO1FBQzFFO1FBQ0EsaURBQWlELGNBQWM7UUFDL0Q7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBLHlDQUF5QyxpQ0FBaUM7UUFDMUUsZ0hBQWdILG1CQUFtQixFQUFFO1FBQ3JJO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsMkJBQTJCLDBCQUEwQixFQUFFO1FBQ3ZELGlDQUFpQyxlQUFlO1FBQ2hEO1FBQ0E7UUFDQTs7UUFFQTtRQUNBLHNEQUFzRCwrREFBK0Q7O1FBRXJIO1FBQ0E7OztRQUdBO1FBQ0E7Ozs7Ozs7Ozs7OztBQ2xGQSxDQUFDLFVBQVNBLENBQVQsRUFBWTtBQUVUQSxHQUFDLENBQUMsaUJBQUQsQ0FBRCxDQUFxQkMsSUFBckIsQ0FBMkIsT0FBM0IsRUFBb0MsWUFBVztBQUMzQ0QsS0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRRSxNQUFSLENBQWdCLFVBQWhCLEVBQTRCLElBQTVCO0FBRUEsUUFBSUMsSUFBSSxHQUFHO0FBQ1BDLFlBQU0sRUFBSSxtQkFESDtBQUVQQyxTQUFHLEVBQU9MLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUU0sSUFBUixDQUFjLEtBQWQsQ0FGSDtBQUdQQyxZQUFNLEVBQUlQLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUUUsTUFBUixDQUFnQixPQUFoQjtBQUhILEtBQVg7QUFNQUYsS0FBQyxDQUFDUSxJQUFGLENBQVFDLFdBQVcsQ0FBQ0MsUUFBcEIsRUFBOEJQLElBQTlCLEVBQW9DLFVBQVNHLElBQVQsRUFBZSxDQUVsRCxDQUZEO0FBSUgsR0FiRDtBQWVBLE1BQUlLLGNBQWMsR0FBT0MsRUFBRSxDQUFDQyxLQUFILENBQVM7QUFDOUJDLFNBQUssRUFBVyx3QkFEYztBQUU5QkMsVUFBTSxFQUFNO0FBQ1JDLFVBQUksRUFBUTtBQURKLEtBRmtCO0FBSzlCQyxZQUFRLEVBQVE7QUFMYyxHQUFULENBQXpCO0FBUUFOLGdCQUFjLENBQUNPLEVBQWYsQ0FBbUIsUUFBbkIsRUFBNkIsWUFBVztBQUNwQyxRQUFJQyxVQUFVLEdBQVVSLGNBQWMsQ0FBQ1MsS0FBZixHQUF1QkMsR0FBdkIsQ0FBMkIsV0FBM0IsRUFBd0NDLEtBQXhDLEdBQWdEQyxNQUFoRCxFQUF4QjtBQUVBdkIsS0FBQyxDQUFDLHNCQUFELENBQUQsQ0FBMEJ3QixJQUExQixDQUFnQyxLQUFoQyxFQUF1Q0wsVUFBVSxDQUFDTSxHQUFsRDtBQUNBekIsS0FBQyxDQUFDLGtCQUFELENBQUQsQ0FBc0IwQixHQUF0QixDQUEyQlAsVUFBVSxDQUFDUSxFQUF0QztBQUVILEdBTkQ7QUFRQTNCLEdBQUMsQ0FBQzRCLFFBQUQsQ0FBRCxDQUFZVixFQUFaLENBQWdCLE9BQWhCLEVBQXlCLHlCQUF6QixFQUFvRCxVQUFTVyxDQUFULEVBQVk7QUFDNURBLEtBQUMsQ0FBQ0MsY0FBRjtBQUVBbkIsa0JBQWMsQ0FBQ29CLElBQWY7QUFDSCxHQUpEO0FBTUEvQixHQUFDLENBQUMsZUFBRCxDQUFELENBQW1Ca0IsRUFBbkIsQ0FBc0IsUUFBdEIsRUFBZ0MsVUFBU1csQ0FBVCxFQUFZO0FBQ3hDQSxLQUFDLENBQUNDLGNBQUY7QUFFQTlCLEtBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUWdDLElBQVI7QUFFQUosWUFBUSxDQUFDSyxhQUFULENBQXVCLHFCQUF2QixFQUE4Q0MsWUFBOUMsQ0FBMkQ7QUFDdkRsQixVQUFJLEVBQUUseUNBRGlEO0FBRXZEbUIsVUFBSSxFQUFFLDhFQUZpRDtBQUd2REMsU0FBRyxFQUFFO0FBSGtELEtBQTNEO0FBTUFwQyxLQUFDLENBQUMsaUJBQUQsQ0FBRCxDQUFxQnFDLElBQXJCO0FBWUEsUUFBSWxDLElBQUksR0FBZ0I7QUFFcEJDLFlBQU0sRUFBYywwQkFGQTtBQUdwQlUsV0FBSyxFQUFlZCxDQUFDLENBQUMsbUJBQUQsQ0FBRCxDQUF1QjBCLEdBQXZCLEVBSEE7QUFJcEJZLGFBQU8sRUFBYUMsT0FBTyxDQUFDQyxZQUFSLENBQXFCQyxVQUFyQixFQUpBO0FBS3BCQyxtQkFBYSxFQUFPMUMsQ0FBQyxDQUFDLGtCQUFELENBQUQsQ0FBc0IwQixHQUF0QjtBQUxBLEtBQXhCO0FBUUFpQixXQUFPLENBQUNDLEdBQVIsQ0FBWSxTQUFaO0FBRUE1QyxLQUFDLENBQUNRLElBQUYsQ0FBUUMsV0FBVyxDQUFDQyxRQUFwQixFQUFnQ1AsSUFBaEMsRUFBd0MsVUFBVUcsSUFBVixFQUFpQjtBQUVyRCxVQUFLQSxJQUFJLENBQUN1QyxNQUFMLElBQWUsQ0FBcEIsRUFBd0I7QUFDcEI3QyxTQUFDLENBQUMsaUJBQUQsQ0FBRCxDQUFxQnFDLElBQXJCO0FBVUgsT0FYRCxNQVdPO0FBQ0hyQyxTQUFDLENBQUMsaUJBQUQsQ0FBRCxDQUFxQnFDLElBQXJCO0FBVUFyQyxTQUFDLENBQUMsZUFBRCxDQUFELENBQW1COEMsSUFBbkI7QUFDSDtBQUNKLEtBMUJEO0FBNEJILEdBN0REO0FBK0RBOUMsR0FBQyxDQUFDLGdCQUFELENBQUQsQ0FBb0JrQixFQUFwQixDQUF1QixRQUF2QixFQUFpQyxVQUFTVyxDQUFULEVBQVk7QUFDekNBLEtBQUMsQ0FBQ0MsY0FBRjtBQUVBOUIsS0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRZ0MsSUFBUjtBQUVBSixZQUFRLENBQUNLLGFBQVQsQ0FBdUIscUJBQXZCLEVBQThDQyxZQUE5QyxDQUEyRDtBQUN2RGxCLFVBQUksRUFBSSxvQkFEK0M7QUFFdkRtQixVQUFJLEVBQUksOEVBRitDO0FBR3ZEQyxTQUFHLEVBQUs7QUFIK0MsS0FBM0Q7QUFRQSxRQUFJakMsSUFBSSxHQUFZO0FBRWhCNEMsY0FBUSxFQUFRL0MsQ0FBQyxDQUFDLFdBQUQsQ0FBRCxDQUFlMEIsR0FBZixFQUZBO0FBR2hCdEIsWUFBTSxFQUFVLHFCQUhBO0FBSWhCNEMsVUFBSSxFQUFZaEQsQ0FBQyxDQUFDLHFCQUFELENBQUQsQ0FBeUIwQixHQUF6QixFQUpBO0FBS2hCdUIsY0FBUSxFQUFRakQsQ0FBQyxDQUFDLHlCQUFELENBQUQsQ0FBNkIwQixHQUE3QixFQUxBO0FBTWhCd0IsV0FBSyxFQUFXbEQsQ0FBQyxDQUFDLHNCQUFELENBQUQsQ0FBMEIwQixHQUExQixFQU5BO0FBT2hCeUIsVUFBSSxFQUFZbkQsQ0FBQyxDQUFDLHFCQUFELENBQUQsQ0FBeUIwQixHQUF6QixFQVBBO0FBUWhCMEIsa0JBQVksRUFBSXBELENBQUMsQ0FBQyw2QkFBRCxDQUFELENBQWlDMEIsR0FBakM7QUFSQSxLQUFwQjtBQVlBMUIsS0FBQyxDQUFDUSxJQUFGLENBQVFDLFdBQVcsQ0FBQ0MsUUFBcEIsRUFBZ0NQLElBQWhDLEVBQXdDLFVBQVVHLElBQVYsRUFBaUI7QUFFckQsVUFBS0EsSUFBSSxDQUFDdUMsTUFBTCxJQUFlLENBQXBCLEVBQXdCO0FBRXBCakIsZ0JBQVEsQ0FBQ0ssYUFBVCxDQUF1QixxQkFBdkIsRUFBOENDLFlBQTlDLENBQTJEO0FBQ3ZEbEIsY0FBSSxFQUFJLG1CQUQrQztBQUV2RG1CLGNBQUksRUFBSSw4REFGK0M7QUFHdkRDLGFBQUcsRUFBSztBQUgrQyxTQUEzRDtBQUtBaUIsZ0JBQVEsQ0FBQ0MsSUFBVCxHQUFvQjdDLFdBQVcsQ0FBQzhDLFFBQWhDO0FBQ0gsT0FSRCxNQVFPO0FBQ0gzQixnQkFBUSxDQUFDSyxhQUFULENBQXVCLHFCQUF2QixFQUE4Q0MsWUFBOUMsQ0FBMkQ7QUFDdkRsQixjQUFJLEVBQUksNkJBRCtDO0FBRXZEbUIsY0FBSSxFQUFJLGdFQUYrQztBQUd2REMsYUFBRyxFQUFLO0FBSCtDLFNBQTNEO0FBTUFwQyxTQUFDLENBQUMsZ0JBQUQsQ0FBRCxDQUFvQjhDLElBQXBCO0FBQ0g7QUFDSixLQW5CRDtBQXFCSCxHQTlDRDtBQWdEQTlDLEdBQUMsQ0FBQyxhQUFELENBQUQsQ0FBaUJrQixFQUFqQixDQUFvQixRQUFwQixFQUE4QixVQUFTVyxDQUFULEVBQVk7QUFDdENBLEtBQUMsQ0FBQ0MsY0FBRjtBQUVBOUIsS0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRZ0MsSUFBUjtBQUVBSixZQUFRLENBQUNLLGFBQVQsQ0FBdUIscUJBQXZCLEVBQThDQyxZQUE5QyxDQUEyRDtBQUN2RGxCLFVBQUksRUFBSSxvQkFEK0M7QUFFdkRtQixVQUFJLEVBQUksOEVBRitDO0FBR3ZEQyxTQUFHLEVBQUs7QUFIK0MsS0FBM0Q7QUFNQSxRQUFJakMsSUFBSSxHQUFZO0FBRWhCNEMsY0FBUSxFQUFRL0MsQ0FBQyxDQUFDLFdBQUQsQ0FBRCxDQUFlMEIsR0FBZixFQUZBO0FBR2hCdEIsWUFBTSxFQUFVLGlCQUhBO0FBSWhCNkMsY0FBUSxFQUFRakQsQ0FBQyxDQUFDLHNCQUFELENBQUQsQ0FBMEIwQixHQUExQixFQUpBO0FBS2hCeUIsVUFBSSxFQUFZbkQsQ0FBQyxDQUFDLHNCQUFELENBQUQsQ0FBMEIwQixHQUExQjtBQUxBLEtBQXBCO0FBU0ExQixLQUFDLENBQUNRLElBQUYsQ0FBUUMsV0FBVyxDQUFDQyxRQUFwQixFQUFnQ1AsSUFBaEMsRUFBd0MsVUFBVUcsSUFBVixFQUFpQjtBQUVyRCxVQUFLQSxJQUFJLENBQUN1QyxNQUFMLElBQWUsQ0FBcEIsRUFBd0I7QUFFcEJqQixnQkFBUSxDQUFDSyxhQUFULENBQXVCLHFCQUF2QixFQUE4Q0MsWUFBOUMsQ0FBMkQ7QUFDdkRsQixjQUFJLEVBQUksc0JBRCtDO0FBRXZEbUIsY0FBSSxFQUFJLDhEQUYrQztBQUd2REMsYUFBRyxFQUFLO0FBSCtDLFNBQTNEO0FBS0FpQixnQkFBUSxDQUFDQyxJQUFULEdBQW9CN0MsV0FBVyxDQUFDOEMsUUFBaEM7QUFDSCxPQVJELE1BUU87QUFDSDNCLGdCQUFRLENBQUNLLGFBQVQsQ0FBdUIscUJBQXZCLEVBQThDQyxZQUE5QyxDQUEyRDtBQUN2RGxCLGNBQUksRUFBSSxpQkFEK0M7QUFFdkRtQixjQUFJLEVBQUksZ0VBRitDO0FBR3ZEQyxhQUFHLEVBQUs7QUFIK0MsU0FBM0Q7QUFNQXBDLFNBQUMsQ0FBQyxhQUFELENBQUQsQ0FBaUI4QyxJQUFqQjtBQUNIO0FBQ0osS0FuQkQ7QUFxQkgsR0F6Q0Q7QUE0Q0gsQ0FsTUQsRUFrTUdVLE1BbE1ILEUiLCJmaWxlIjoiYnVuZGxlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7IGVudW1lcmFibGU6IHRydWUsIGdldDogZ2V0dGVyIH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBkZWZpbmUgX19lc01vZHVsZSBvbiBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIgPSBmdW5jdGlvbihleHBvcnRzKSB7XG4gXHRcdGlmKHR5cGVvZiBTeW1ib2wgIT09ICd1bmRlZmluZWQnICYmIFN5bWJvbC50b1N0cmluZ1RhZykge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBTeW1ib2wudG9TdHJpbmdUYWcsIHsgdmFsdWU6ICdNb2R1bGUnIH0pO1xuIFx0XHR9XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnX19lc01vZHVsZScsIHsgdmFsdWU6IHRydWUgfSk7XG4gXHR9O1xuXG4gXHQvLyBjcmVhdGUgYSBmYWtlIG5hbWVzcGFjZSBvYmplY3RcbiBcdC8vIG1vZGUgJiAxOiB2YWx1ZSBpcyBhIG1vZHVsZSBpZCwgcmVxdWlyZSBpdFxuIFx0Ly8gbW9kZSAmIDI6IG1lcmdlIGFsbCBwcm9wZXJ0aWVzIG9mIHZhbHVlIGludG8gdGhlIG5zXG4gXHQvLyBtb2RlICYgNDogcmV0dXJuIHZhbHVlIHdoZW4gYWxyZWFkeSBucyBvYmplY3RcbiBcdC8vIG1vZGUgJiA4fDE6IGJlaGF2ZSBsaWtlIHJlcXVpcmVcbiBcdF9fd2VicGFja19yZXF1aXJlX18udCA9IGZ1bmN0aW9uKHZhbHVlLCBtb2RlKSB7XG4gXHRcdGlmKG1vZGUgJiAxKSB2YWx1ZSA9IF9fd2VicGFja19yZXF1aXJlX18odmFsdWUpO1xuIFx0XHRpZihtb2RlICYgOCkgcmV0dXJuIHZhbHVlO1xuIFx0XHRpZigobW9kZSAmIDQpICYmIHR5cGVvZiB2YWx1ZSA9PT0gJ29iamVjdCcgJiYgdmFsdWUgJiYgdmFsdWUuX19lc01vZHVsZSkgcmV0dXJuIHZhbHVlO1xuIFx0XHR2YXIgbnMgPSBPYmplY3QuY3JlYXRlKG51bGwpO1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIobnMpO1xuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkobnMsICdkZWZhdWx0JywgeyBlbnVtZXJhYmxlOiB0cnVlLCB2YWx1ZTogdmFsdWUgfSk7XG4gXHRcdGlmKG1vZGUgJiAyICYmIHR5cGVvZiB2YWx1ZSAhPSAnc3RyaW5nJykgZm9yKHZhciBrZXkgaW4gdmFsdWUpIF9fd2VicGFja19yZXF1aXJlX18uZChucywga2V5LCBmdW5jdGlvbihrZXkpIHsgcmV0dXJuIHZhbHVlW2tleV07IH0uYmluZChudWxsLCBrZXkpKTtcbiBcdFx0cmV0dXJuIG5zO1xuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IDEpO1xuIiwiKGZ1bmN0aW9uKCQpIHtcclxuICAgIFxyXG4gICAgJChcIiNzdHVkZW50X3JhdGluZ1wiKS5iaW5kKCAncmF0ZWQnLCBmdW5jdGlvbigpIHtcclxuICAgICAgICAkKHRoaXMpLnJhdGVpdCggJ3JlYWRvbmx5JywgdHJ1ZSApO1xyXG5cclxuICAgICAgICB2YXIgZm9ybSA9IHtcclxuICAgICAgICAgICAgYWN0aW9uICA6ICdfX1RQX3JhdGVfc3R1ZGVudCcsXHJcbiAgICAgICAgICAgIHJpZCAgICAgOiAkKHRoaXMpLmRhdGEoICdyaWQnICksXHJcbiAgICAgICAgICAgIHJhdGluZyAgOiAkKHRoaXMpLnJhdGVpdCggJ3ZhbHVlJyApXHJcbiAgICAgICAgfVxyXG4gICAgICAgIFxyXG4gICAgICAgICQucG9zdCggc3R1ZGVudF9vYmouYWpheF91cmwsIGZvcm0sIGZ1bmN0aW9uKGRhdGEpIHtcclxuICAgICAgICAgICAgXHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgXHJcbiAgICB9KTtcclxuXHJcbiAgICB2YXIgZmVhdHVyZWRfZnJhbWUgICA9ICAgd3AubWVkaWEoe1xyXG4gICAgICAgIHRpdGxlICAgICAgIDogICAnU2VsZWN0IG9yIFVwbG9hZCBNZWRpYScsXHJcbiAgICAgICAgYnV0dG9uOiAgICAge1xyXG4gICAgICAgICAgICB0ZXh0OiAgICAgICAnVXNlIHRoaXMgbWVkaWEnXHJcbiAgICAgICAgfSxcclxuICAgICAgICBtdWx0aXBsZTogICAgICAgZmFsc2VcclxuICAgIH0pO1xyXG5cclxuICAgIGZlYXR1cmVkX2ZyYW1lLm9uKCAnc2VsZWN0JywgZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgdmFyIGF0dGFjaG1lbnQgICAgICA9ICAgZmVhdHVyZWRfZnJhbWUuc3RhdGUoKS5nZXQoJ3NlbGVjdGlvbicpLmZpcnN0KCkudG9KU09OKCk7XHJcblxyXG4gICAgICAgICQoXCIjc3R1ZGVudC1pbWctcHJldmlld1wiKS5hdHRyKCAnc3JjJywgYXR0YWNobWVudC51cmwgKTsgXHJcbiAgICAgICAgJChcIiNfX1RQX2lucHV0SW1nSURcIikudmFsKCBhdHRhY2htZW50LmlkICk7XHJcblxyXG4gICAgfSk7XHJcblxyXG4gICAgJChkb2N1bWVudCkub24oICdjbGljaycsICcjc3R1ZGVudC1pbWctdXBsb2FkLWJ0bicsIGZ1bmN0aW9uKGUpIHtcclxuICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcblxyXG4gICAgICAgIGZlYXR1cmVkX2ZyYW1lLm9wZW4oKTtcclxuICAgIH0pO1xyXG5cclxuICAgICQoXCIjc3R1ZGVudC1mb3JtXCIpLm9uKCdzdWJtaXQnLCBmdW5jdGlvbihlKSB7XHJcbiAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG5cclxuICAgICAgICAkKHRoaXMpLmhpZGUoKTtcclxuXHJcbiAgICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiNub3RpZmljYXRpb25zLWFyZWFcIikubm90aWZpY2F0aW9uKHtcclxuICAgICAgICAgICAgdGV4dDogXCJQbGVhc2Ugd2FpdCEgV2UgYXJlIHN1Ym1pdGluZyB5b3VyIGRhdGFcIixcclxuICAgICAgICAgICAgaWNvbjogJzxzcGFuIGNsYXNzPVwiaWNvbiBzbmFja2Jhci1pY29uIG5vdmktaWNvbiBmYS1jaXJjbGUtby1ub3RjaCBmYS1zcGluXCI+PC9zcGFuPicsXHJcbiAgICAgICAgICAgIGNsczogXCJzbmFja2Jhci1zZWNvbmRhcnlcIlxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIFxyXG4gICAgICAgICQoXCIjc3R1ZGVudC1zdGF0dXNcIikuaHRtbChcclxuICAgICAgICAgICAgYDxkaXYgY2xhc3M9XCJzbmFja2JhciBzbmFja2Jhci1zZWNvbmRhcnlcIj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJzbmFja2Jhci1pbm5lclwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJzbmFja2Jhci10aXRsZVwiIGlkPVwic3R1ZGVudC1zdGF0dXNcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJpY29uIHNuYWNrYmFyLWljb24gbm92aS1pY29uIGZhLWNpcmNsZS1vLW5vdGNoIGZhLXNwaW5cIj48L3NwYW4+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFBsZWFzZSB3YWl0ISBXZSBhcmUgc3VibWl0aW5nIHlvdXIgZGF0YVxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDwvZGl2PmBcclxuICAgICAgICApO1xyXG5cclxuXHJcbiAgICAgICAgdmFyIGZvcm0gICAgICAgICAgICA9ICAge1xyXG5cclxuICAgICAgICAgICAgYWN0aW9uICAgICAgICAgIDogICAnX19UUF9zdWJtaXRfc3R1ZGVudF9kYXRhJyxcclxuICAgICAgICAgICAgdGl0bGUgICAgICAgICAgIDogICAkKFwiI19fVFBfaW5wdXRfdGl0bGVcIikudmFsKCksXHJcbiAgICAgICAgICAgIGNvbnRlbnQgICAgICAgICA6ICAgdGlueW1jZS5hY3RpdmVFZGl0b3IuZ2V0Q29udGVudCgpLFxyXG4gICAgICAgICAgICBhdHRhY2htZW50X2lkICAgOiAgICQoXCIjX19UUF9pbnB1dEltZ0lEXCIpLnZhbCgpICAgXHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zb2xlLmxvZygnd29ya2luZycpO1xyXG5cclxuICAgICAgICAkLnBvc3QoIHN0dWRlbnRfb2JqLmFqYXhfdXJsLCAgIGZvcm0sICAgZnVuY3Rpb24oIGRhdGEgKSB7XHJcblxyXG4gICAgICAgICAgICBpZiAoIGRhdGEuc3RhdHVzID09IDIgKSB7XHJcbiAgICAgICAgICAgICAgICAkKFwiI3N0dWRlbnQtc3RhdHVzXCIpLmh0bWwoXHJcbiAgICAgICAgICAgICAgICAgICAgYDxkaXYgY2xhc3M9XCJzbmFja2JhciBzbmFja2Jhci1wcmltYXJ5XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJzbmFja2Jhci1pbm5lclwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cInNuYWNrYmFyLXRpdGxlXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJpY29uIHNuYWNrYmFyLWljb24gbm92aS1pY29uIGludC1jaGVja1wiPjwvc3Bhbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBTdWJtaXR0ZWQgU3VjY2Vzc2Z1bGx5XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+YFxyXG4gICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICQoXCIjc3R1ZGVudC1zdGF0dXNcIikuaHRtbChcclxuICAgICAgICAgICAgICAgICAgICBgPGRpdiBjbGFzcz1cInNuYWNrYmFyIHNuYWNrYmFyLWRhbmdlclwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwic25hY2tiYXItaW5uZXJcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJzbmFja2Jhci10aXRsZVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwiaWNvbiBzbmFja2Jhci1pY29uIG5vdmktaWNvbiBpbnQtd2FybmluZ1wiPjwvc3Bhbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBGb3JtIHN1Ym1pc3Npb24gZmFpbGVkXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+YFxyXG4gICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgICAgICQoXCIjc3R1ZGVudC1mb3JtXCIpLnNob3coKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pXHJcblxyXG4gICAgfSk7XHJcblxyXG4gICAgJChcIiNyZWdpc3Rlci1mb3JtXCIpLm9uKCdzdWJtaXQnLCBmdW5jdGlvbihlKSB7XHJcbiAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG5cclxuICAgICAgICAkKHRoaXMpLmhpZGUoKTtcclxuICAgICAgICBcclxuICAgICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI25vdGlmaWNhdGlvbnMtYXJlYVwiKS5ub3RpZmljYXRpb24oe1xyXG4gICAgICAgICAgICB0ZXh0OiAgIFwiUHJvY2Vzc2luZy4uLi4uLi4uXCIsXHJcbiAgICAgICAgICAgIGljb246ICAgJzxzcGFuIGNsYXNzPVwiaWNvbiBzbmFja2Jhci1pY29uIG5vdmktaWNvbiBmYS1jaXJjbGUtby1ub3RjaCBmYS1zcGluXCI+PC9zcGFuPicsXHJcbiAgICAgICAgICAgIGNsczogICAgXCJzbmFja2Jhci1zZWNvbmRhcnlcIlxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgXHJcblxyXG5cclxuICAgICAgICB2YXIgZm9ybSAgICAgICAgPSAgIHtcclxuXHJcbiAgICAgICAgICAgIF93cG5vbmNlICAgIDogICAkKCcjX3dwbm9uY2UnKS52YWwoKSxcclxuICAgICAgICAgICAgYWN0aW9uICAgICAgOiAgICdfX1RQX2NyZWF0ZV9hY2NvdW50JyxcclxuICAgICAgICAgICAgbmFtZSAgICAgICAgOiAgICQoXCIjcmVnaXN0ZXItZm9ybS1uYW1lXCIpLnZhbCgpLFxyXG4gICAgICAgICAgICB1c2VybmFtZSAgICA6ICAgJChcIiNyZWdpc3Rlci1mb3JtLXVzZXJuYW1lXCIpLnZhbCgpLFxyXG4gICAgICAgICAgICBlbWFpbCAgICAgICA6ICAgJChcIiNyZWdpc3Rlci1mb3JtLWVtYWlsXCIpLnZhbCgpLFxyXG4gICAgICAgICAgICBwYXNzICAgICAgICA6ICAgJChcIiNyZWdpc3Rlci1mb3JtLXBhc3NcIikudmFsKCksXHJcbiAgICAgICAgICAgIGNvbmZpcm1fcGFzczogICAkKFwiI3JlZ2lzdGVyLWZvcm0tY29uZmlybV9wYXNzXCIpLnZhbCgpLFxyXG4gICAgICAgICAgICBcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgICQucG9zdCggc3R1ZGVudF9vYmouYWpheF91cmwsICAgZm9ybSwgICBmdW5jdGlvbiggZGF0YSApIHtcclxuXHJcbiAgICAgICAgICAgIGlmICggZGF0YS5zdGF0dXMgPT0gMiApIHtcclxuICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiNub3RpZmljYXRpb25zLWFyZWFcIikubm90aWZpY2F0aW9uKHtcclxuICAgICAgICAgICAgICAgICAgICB0ZXh0OiAgIFwiQWNjb3VudCBDcmVhdGVkICFcIixcclxuICAgICAgICAgICAgICAgICAgICBpY29uOiAgICc8c3BhbiBjbGFzcz1cImljb24gc25hY2tiYXItaWNvbiBub3ZpLWljb24gaW50LWNoZWNrXCI+PC9zcGFuPicsXHJcbiAgICAgICAgICAgICAgICAgICAgY2xzOiAgICBcInNuYWNrYmFyLXByaW1hcnlcIlxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICBsb2NhdGlvbi5ocmVmICAgPSAgIHN0dWRlbnRfb2JqLmhvbWVfdXJsO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiNub3RpZmljYXRpb25zLWFyZWFcIikubm90aWZpY2F0aW9uKHtcclxuICAgICAgICAgICAgICAgICAgICB0ZXh0OiAgIFwiVW5hYmxlIHRvIGNyZWF0ZSBhbiBhY2NvdW50XCIsXHJcbiAgICAgICAgICAgICAgICAgICAgaWNvbjogICAnPHNwYW4gY2xhc3M9XCJpY29uIHNuYWNrYmFyLWljb24gbm92aS1pY29uIGludC13YXJuaW5nXCI+PC9zcGFuPicsXHJcbiAgICAgICAgICAgICAgICAgICAgY2xzOiAgICBcInNuYWNrYmFyLWRhbmdlclwiXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgICAgICAkKFwiI3JlZ2lzdGVyLWZvcm1cIikuc2hvdygpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSlcclxuXHJcbiAgICB9KTtcclxuXHJcbiAgICAkKFwiI2xvZ2luLWZvcm1cIikub24oJ3N1Ym1pdCcsIGZ1bmN0aW9uKGUpIHtcclxuICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcblxyXG4gICAgICAgICQodGhpcykuaGlkZSgpO1xyXG4gICAgICAgIFxyXG4gICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjbm90aWZpY2F0aW9ucy1hcmVhXCIpLm5vdGlmaWNhdGlvbih7XHJcbiAgICAgICAgICAgIHRleHQ6ICAgXCJQcm9jZXNzaW5nLi4uLi4uLi5cIixcclxuICAgICAgICAgICAgaWNvbjogICAnPHNwYW4gY2xhc3M9XCJpY29uIHNuYWNrYmFyLWljb24gbm92aS1pY29uIGZhLWNpcmNsZS1vLW5vdGNoIGZhLXNwaW5cIj48L3NwYW4+JyxcclxuICAgICAgICAgICAgY2xzOiAgICBcInNuYWNrYmFyLXNlY29uZGFyeVwiXHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIHZhciBmb3JtICAgICAgICA9ICAge1xyXG5cclxuICAgICAgICAgICAgX3dwbm9uY2UgICAgOiAgICQoJyNfd3Bub25jZScpLnZhbCgpLFxyXG4gICAgICAgICAgICBhY3Rpb24gICAgICA6ICAgJ19fVFBfdXNlcl9sb2dpbicsXHJcbiAgICAgICAgICAgIHVzZXJuYW1lICAgIDogICAkKFwiI2xvZ2luLWZvcm0tdXNlcm5hbWVcIikudmFsKCksXHJcbiAgICAgICAgICAgIHBhc3MgICAgICAgIDogICAkKFwiI2xvZ2luLWZvcm0tcGFzc3dvcmRcIikudmFsKCksXHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgJC5wb3N0KCBzdHVkZW50X29iai5hamF4X3VybCwgICBmb3JtLCAgIGZ1bmN0aW9uKCBkYXRhICkge1xyXG5cclxuICAgICAgICAgICAgaWYgKCBkYXRhLnN0YXR1cyA9PSAyICkge1xyXG4gICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI25vdGlmaWNhdGlvbnMtYXJlYVwiKS5ub3RpZmljYXRpb24oe1xyXG4gICAgICAgICAgICAgICAgICAgIHRleHQ6ICAgXCJMb2dpbiBTdWNjZXNzZnVsbHkgIVwiLFxyXG4gICAgICAgICAgICAgICAgICAgIGljb246ICAgJzxzcGFuIGNsYXNzPVwiaWNvbiBzbmFja2Jhci1pY29uIG5vdmktaWNvbiBpbnQtY2hlY2tcIj48L3NwYW4+JyxcclxuICAgICAgICAgICAgICAgICAgICBjbHM6ICAgIFwic25hY2tiYXItcHJpbWFyeVwiXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIGxvY2F0aW9uLmhyZWYgICA9ICAgc3R1ZGVudF9vYmouaG9tZV91cmw7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI25vdGlmaWNhdGlvbnMtYXJlYVwiKS5ub3RpZmljYXRpb24oe1xyXG4gICAgICAgICAgICAgICAgICAgIHRleHQ6ICAgXCJVbmFibGUgdG8gbG9naW5cIixcclxuICAgICAgICAgICAgICAgICAgICBpY29uOiAgICc8c3BhbiBjbGFzcz1cImljb24gc25hY2tiYXItaWNvbiBub3ZpLWljb24gaW50LXdhcm5pbmdcIj48L3NwYW4+JyxcclxuICAgICAgICAgICAgICAgICAgICBjbHM6ICAgIFwic25hY2tiYXItZGFuZ2VyXCJcclxuICAgICAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgICAgICQoXCIjbG9naW4tZm9ybVwiKS5zaG93KCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KVxyXG5cclxuICAgIH0pO1xyXG5cclxuXHJcbn0pKGpRdWVyeSk7XHJcbiJdLCJzb3VyY2VSb290IjoiIn0=
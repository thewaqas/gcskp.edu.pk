<?php 




function __TP_course_add_form_fields() {

    ?>

    <div class="form-field form-required term-name-wrap">
        <label for="tag-info"> <?php _e( 'More Info URL', '__TP' )?> </label>
        <input name="__TP_more_info_url" id="tag-info" type="text">
        <p class="description">
            <?php 
                esc_html_e( 'A URL a user can click to learn more information about the course', '__TP' )
            ?>
        </p>
    </div>

    <?php
}


function __TP_course_edit_form_fields( $term ) {

    $url     =   get_term_meta( $term->term_id, 'more_info_url', true );

    ?>

    <tr class="form-field form-required term-name-wrap">
        <th scope="row">
            <label for="more_info_url"><?php _e( 'More Info URL', '__TP' )?></label>
        </th>
        <td>
            <input name="__TP_more_info_url" id="more_info_url" type="text" value="<?php echo esc_attr( $url )?>">
        
            <p class="description">
                <?php 
                    esc_html_e( 'A URL a user can click to learn more information about the course', '__TP' )
                ?>
            </p>
        </td>
    </tr>

    <?php
}


<?php

function __TP_settings_api() {

    register_setting( '__TP_opts_group', '__TP_opts', '__TP_opts_sanitize' );

    add_settings_section( 
        'student_settings', 
        'Students Setting', 
        '__TP_settings_section',
        '__TP_opts_sections',
    );

    add_settings_field( 
        'rating_login_required', 
        'User login in is required for rating', 
        'rating_login_required_input_cb', 
        '__TP_opts_sections',
        'student_settings' 
    );

    add_settings_field( 
        'submission_login_required', 
        'User login in is required for submitting students', 
        'submission_login_required_input_cb', 
        '__TP_opts_sections', 
        'student_settings'
    );

}

function __TP_settings_section() {
    echo '<p>You can change settings here.</p>';
}

function rating_login_required_input_cb() {

    $student_opts          =   get_option( '__TP_opts' );

    ?>
 

    <select name="__TP_opts[rating_login_required]">
        <option selected>Open this select menu</option>
        <option value="1">No</option>
        <option value="2" <?php echo $student_opts['rating_login_required'] == 2 ? 'selected' : '' ?> >Yes</option>
    </select>
    
    <?php
}

function submission_login_required_input_cb() {

    $student_opts          =   get_option( '__TP_opts' );

    ?>
    <select name="__TP_opts[submission_login_required]">
        <option selected>Open this select menu</option>
        <option value="1">No</option>
        <option value="2" <?php echo $student_opts['submission_login_required'] == 2 ? 'selected' : '' ?> >Yes</option>
    </select>
    
    <?php
}

function __TP_opts_sanitize( $input ) {

    $input['rating_login_required']     =  absint( $input['rating_login_required'] );
    $input['submission_login_required'] =  absint( $input['submission_login_required'] );

    return $input;
}
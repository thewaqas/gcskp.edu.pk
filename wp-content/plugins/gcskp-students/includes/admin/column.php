<?php 


function __TP_add_new_students_columns( $columns ) {

    $new_columns                =   [];
    $new_columns['cb']          =  '<input type="checkbox">';
    $new_columns['title']       =  __( 'Title',     '__TP' );
    $new_columns['author']      =  __( 'Author',    '__TP' );
    $new_columns['categories']  =  __( 'Categories','__TP' );
    $new_columns['count']       =  __( 'Count',     '__TP' );
    $new_columns['rating']      =  __( 'Rating',    '__TP' );
    $new_columns['date']        =  __( 'Date',      '__TP' );

    return $new_columns;
}

function __TP_manage_students_columns( $column, $post_id ) {

    switch ( $column ) {
        case 'count':
            $student_data = get_post_meta( $post_id, 'student_data', true );
            echo isset( $student_data['rating_count'] ) ? $student_data['rating_count'] : 0 ;
            break;
        case 'rating':
            $student_data = get_post_meta( $post_id, 'student_data', true );
            echo isset( $student_data['rating'] ) ? $student_data['rating'] : 'N/A' ;
            break;
        
        default:
            break;
    }

}
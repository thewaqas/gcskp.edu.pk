<?php 


function __TP_plugin_opts_page() {

    $student_opts   =   get_option( '__TP_opts' );

    ?>

    <div class="wrap">
    
        <div class="card">
            <div class="card-body">
                <h3 class="card-title"><?php _e('Recipe Settings', '__TP' ); ?></h3>

                <?php
                
                if( isset( $_GET['status'] ) && $_GET['status'] == 1 ) {
                    ?>
                    <div class="alert alert-success">Option Updated Successfully!</div>
                    <?php
                }
                
                ?>

                <form method="POST" action="admin-post.php">
                    <input type="hidden" name="action" value="__TP_save_options">
                    <?php wp_nonce_field( '__TP_options_verify' ) ?>
                    <div class="mb-3">
                        <label class="form-label"><?php _e('User login required for ratting student', '__TP'); ?></label>
                        <select class="form-select form-select-lg mb-3" name="__TP_rating_login_required" aria-label="<?php _e('User login required for rating student', '__TP'); ?>">
                            <option selected>Open this select menu</option>
                            <option value="1">No</option>
                            <option value="2" <?php echo $student_opts['rating_login_required'] == 2 ? 'selected' : '' ?> >Yes</option>
                        </select>
                    </div>
                    <div class="mb-3">
                        <label class="form-label"><?php _e('User login required for submitting student', '__TP'); ?></label>
                        <select class="form-select form-select-lg mb-3" name="__TP_submission_login_required" aria-label="<?php _e('User login required for rating student', '__TP'); ?>">
                            <option selected>Open this select menu</option>
                            <option value="1">No</option>
                            <option value="2" <?php echo $student_opts['submission_login_required'] == 2 ? 'selected' : '' ?> >Yes</option>
                        </select>
                    </div>
                    <div class="mb-3">
                        <button type="submit" class="btn btn-primary"><?php _e('Update', '__TP'); ?></button>
                    </div>
                </form>
            </div>
        </div><!-- #END card -->

        <hr>

        <form method="POST" action="options.php">
        
        <?php
        
        //settings_fields( '__TP_opts_group' );

        //do_settings_sections( '__TP_opts_sections' );
        
        //submit_button();
        
        ?>

        </form>
    


    </div><!-- #END warp -->


    <?php
}
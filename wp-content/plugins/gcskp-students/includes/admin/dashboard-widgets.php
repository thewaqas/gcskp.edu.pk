<?php


function __TP_dashboard_widgets() {

    wp_add_dashboard_widget( '__TP_latest_rating', 'Letest Rating', '__TP_latest_rating_display' );
}
function __TP_latest_rating_display() {

    global $wpdb;

    $latest_rating      =   $wpdb->get_results (
        "SELECT * FROM `".$wpdb->prefix ."student_ratings` 
        ORDER BY `ID` DESC LIMIT 5"    

    );

    echo '<ul>';

    foreach ( $latest_rating  as $rating ) {
        
        $title              =   get_the_title( $rating->student_id );
        $parmalink          =   get_the_permalink( $rating->student_id );

        ?>
        
        <li>
            <a href="<?php echo $parmalink ?>"><?php echo $title ?></a>
            received the ratind of <?php echo $rating->rating ?>

        </li>


        <?php
    }

    echo '</ul>';
}
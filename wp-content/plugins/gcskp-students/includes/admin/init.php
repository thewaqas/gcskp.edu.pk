<?php

function __TP_students_admin_init() {

    include( 'column.php'   );
    include( 'enqueue.php'  );
    include( 'settings-api.php');



    add_filter( 'manage_students_posts_columns',        '__TP_add_new_students_columns' );

    add_action( 'manage_students_posts_custom_column',  '__TP_manage_students_columns' , 10, 2);

    add_action( 'admin_enqueue_scripts',                '__TP_admin_enqueue' );

    add_action( 'admin_post___TP_save_options',         '__TP_save_options' );    




    __TP_settings_api();

}
<?php 



function __TP_twitter_follow_shortcode( $atts, $content = null ) {

    $atts           =   shortcode_atts([
        'handle'    =>  'thewaqaspro'
    ], $atts);
    
    return '<a  class="btn btn-lg btn-primary" 
                href="https://twitter.com/' . $atts['handle'] . '" 
                target="_blank"><span class="btn-icon navbar-info-icon icon-social int-twitter novi-icon"></span>
                <span>'. do_shortcode($content) . '</span>
            </a>';
}


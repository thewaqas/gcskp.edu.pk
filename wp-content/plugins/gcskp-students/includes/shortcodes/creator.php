<?php

function __TP_student_creator_shortcode() {

	$student_option =   get_option( '__TP_opts' );

	if ( !is_user_logged_in() && $student_option['submission_login_required'] == 2 ) {
        return 'You must be login to submit student';
    }

	$creatorHTML		=	file_get_contents( 'creator-template.php',	true );
	$editorHTML			=	__TP_generate_content_editor();
	$creatorHTML		=	str_replace('CONTENT_EDITOR', $editorHTML, $creatorHTML );

	return $creatorHTML;
}


function __TP_generate_content_editor() {

	ob_start();
	wp_editor( '', 'studentcontenteditor' );
	$editor_content		= ob_get_clean();
	return $editor_content;
}
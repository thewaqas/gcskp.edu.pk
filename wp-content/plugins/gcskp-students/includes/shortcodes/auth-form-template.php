<!-- Login register-->
<section class="section section-lg bg-transparent" data-preset='{"title":"Login Register","category":"one screen page, form","reload":true,"id":"login-register"}'>
    <div class="container">
        <div class="row row-40 justify-content-center">
            <div class="col-md-8 col-lg-6 col-xl-5 col-xxl-4">
                <div class="tab">
                    <ul class="nav nav-classic justify-content-center" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link h4 active" data-toggle="tab" href="#login" role="tab" aria-selected="true">Login</a>
                        </li>
                        <li class="nav-item" SHOW_REG_FORM>
                            <a class="nav-link h4" data-toggle="tab" href="#register" role="tab" aria-selected="false">Registration</a>
                        </li>
                    </ul>
                    <div class="tab-content">

                        <div id="authentication-status"></div>
                        
                        NONCE_FIELD_PH

                        <div class="tab-pane fade show active" id="login" role="tabpanel">

                            <form id="login-form" class="">
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text int-user novi-icon"></span>
                                        </div>
                                        <input id="login-form-username" class="form-control" type="text" name="login" placeholder="Your login Username" data-constraints="@Required">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text int-user novi-icon"></span>
                                        </div>
                                        <input id="login-form-password" class="form-control input-password password-login" type="text" name="password" placeholder="Password" data-constraints="@Required">
                                        <div class="input-group-append">
                                            <span class="input-group-text input-group-text-nodivider input-password-icon" data-multi-switch='{"targets":".password-login"}'></span>
                                        </div>
                                    </div>
                                </div>
                                <button class="btn btn-block btn-lg btn-primary" type="submit">Sign in</button>
                                <div class="offset-sm group-15 small">
                                    <div>
                                        <u class="font-weight-medium">
                                            <a class="link-inherit" href="#">Forgot your password?</a>
                                        </u>
                                    </div>
                                    <div>
                                        Haven’t an account? 
                                        <u class="font-weight-medium">
                                            <a class="link-inherit" href="register.html">Sign up here.</a>
                                        </u>
                                    </div>
                                </div>
                            </form>

                        </div>

                        <div class="tab-pane fade" id="register" role="tabpanel" SHOW_REG_FORM>

                            <form id="register-form" class="">
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text int-user novi-icon"></span>
                                        </div>
                                        <input id="register-form-name" class="form-control" type="text" name="name" placeholder="Your name" data-constraints="@Required">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text int-user novi-icon"></span>
                                        </div>
                                        <input id="register-form-username" class="form-control" type="text" name="name" placeholder="Username" data-constraints="@Required">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text int-user novi-icon"></span>
                                        </div>
                                        <input id="register-form-email" class="form-control" type="email" name="email" placeholder="E-mail" data-constraints="@Email @Required">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text int-user novi-icon"></span>
                                        </div>
                                        <input id="register-form-pass" class="form-control input-password password-register-1" type="text" name="password" placeholder="Password" data-constraints="@Required">
                                        <div class="input-group-append">
                                            <span class="input-group-text input-group-text-nodivider input-password-icon" data-multi-switch='{"targets":".password-register-1"}'></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text int-user novi-icon"></span>
                                        </div>
                                        <input id="register-form-confirm_pass" class="form-control input-password password-register-2" type="text" name="password" placeholder="Confirm password" data-constraints="@Required">
                                        <div class="input-group-append">
                                            <span class="input-group-text input-group-text-nodivider input-password-icon" data-multi-switch='{"targets":".password-register-2"}'></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="offset-xs">
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" type="checkbox" id="check1">
                                        <label class="custom-control-label" for="check1">
                                            I agree with the
                                            <u class="font-weight-medium">
                                                <a class="link-inherit" href="#">Terms of use.</a>
                                            </u>
                                        </label>
                                    </div>
                                </div>
                                <button class="btn btn-block btn-lg btn-secondary" type="submit">Sign in</button>
                            </form>

                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

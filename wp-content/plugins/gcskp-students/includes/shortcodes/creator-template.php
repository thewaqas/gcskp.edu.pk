<!-- Snackbar-->
<div id="student-status"></div>
<form id="student-form">
    <div class="form-group">
        <label for="__TP_input_title">Title</label>
        <div class="position-relative">
            <input class="form-control" id="__TP_input_title" type="text" placeholder="Enter Title">
        </div>
    </div>
    <br>
    <!-- Tiny Content Editor -->
    CONTENT_EDITOR
    <hr>
    <div class="form-group">
        <label>Feature Image <a href="#" id="student-img-upload-btn">Upload</a></label>
        <br>
        <img id="student-img-preview">
        <input type="hidden" id="__TP_inputImgID">
    </div>
 

    <button class="btn btn-lg btn-secondary" type="submit">Submit Student Deta</button>
</form>
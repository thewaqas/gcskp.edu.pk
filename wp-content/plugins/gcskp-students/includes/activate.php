<?php

function __TP_ativate_plugin() {

    // 5.8 < 5.0
    if( version_compare( get_bloginfo( 'version' ), '5.0', '<' ) ){
        wp_die( __( "You must update WordPress to use this plugin.", '__TP' ) );
    }

    __TP_init();
    flush_rewrite_rules();

    global $wpdb;
    $createSQL      =   "
    CREATE TABLE `" . $wpdb->prefix . "student_ratings` (
        `ID` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
        `student_id` BIGINT(20) UNSIGNED NOT NULL,
        `rating` FLOAT(3,2) UNSIGNED NOT NULL,
        `user_ip` VARCHAR(50) NOT NULL,
        PRIMARY KEY (`ID`)
    ) ENGINE=InnoDB " . $wpdb->get_charset_collate() . ";";


    require( ABSPATH . "/wp-admin/includes/upgrade.php" );
    dbDelta( $createSQL );


    // Get option if exist
    $student_opts         =   get_option( '__TP_opts' );

    // if not exist then add option
    if ( !$student_opts ) {

        $opts             =   [
            'rating_login_required'             =>  1,
            'submission_login_required' =>  1,
        ];

        add_option( '__TP_opts', $opts );
    }



    global $wp_roles;

    add_role( 
        'student_author', 
        __('Student Author', '__TP'), 
        [
            'read'          =>  true,
            'edit_posts'    =>  true,
            'upload_files'  =>  true,
        ] 
        );

}
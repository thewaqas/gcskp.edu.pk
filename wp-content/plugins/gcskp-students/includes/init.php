<?php

/*
* Creating a function to create our CPT
*/

function __TP_init() {

    // Set UI labels for Custom Post Type
    $labels = array(
        'name'                  => _x( 'Students', 'Post Type General Name',  '__TP' ),
        'singular_name'         => _x( 'Student', 'Post Type Singular Name',  '__TP' ),
        'menu_name'             => __( 'Students',            '__TP' ),
        'parent_item_colon'     => __( 'Parent Student',      '__TP' ),
        'all_items'             => __( 'All Students',        '__TP' ),
        'view_item'             => __( 'View Student',        '__TP' ),
        'add_new_item'          => __( 'Add New Student',     '__TP' ),
        'add_new'               => __( 'Add New',             '__TP' ),
        'edit_item'             => __( 'Edit Student',        '__TP' ),
        'update_item'           => __( 'Update Student',      '__TP' ),
        'search_items'          => __( 'Search Student',      '__TP' ),
        'not_found'             => __( 'Not Found',           '__TP' ),
        'not_found_in_trash'    => __( 'Not found in Trash',  '__TP' ),
    );
    // Set other options for Custom Post Type
    $args = array(
        'labels'                => $labels,
        'description'           => __( 'Students Record',   '__TP' ),
        'public'                => true,
        'publicly_queryable'    => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'query_var'             => true,
        'rewrite'               => array ( 'slug' => 'students' ),     
        'capability'            => 'post',
        'has_archive'           => true,
        'menu_position'         => 20,
        'menu_icon'             => 'dashicons-groups',
        'supports'              => array( 'title', 'editor', 'author', 'thumbnail', 'custom-fields'),
        'taxonomies'            => array( 'category', 'post_tag' ),
        'show_in_rest'          => true
    );
    // Registering your Custom Post Type
    register_post_type( 'students', $args );


    register_taxonomy( 'course','students', [
        'label'         =>  __( 'Course', '__TP' ),
        'rewrite'       =>  [ 'slug'  =>  'course' ],
        'show_in_rest'  =>  true

    ] );
}


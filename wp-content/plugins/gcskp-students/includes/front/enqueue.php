<?php



function __TP_enqueue_scripts() {

    wp_register_style( '__TP-stylesheet' ,  
        plugins_url( '/dist/assets/css/bundle.css', STUDENTS_PLUGIN_URL ), 
        array(), '1.0.0', 'all' );

    wp_register_style( '__TP-rateit-stylesheet' ,  
        plugins_url( '/dist/vender/rateit/rateit.css', STUDENTS_PLUGIN_URL ), 
        array(), '1.0.0', 'all' );

    wp_enqueue_style( '__TP-stylesheet' );
    wp_enqueue_style( '__TP-rateit-stylesheet' );
    wp_enqueue_media();
    


    wp_register_script( '__TP-scripts', 
        plugins_url( '/dist/assets/js/bundle.js', STUDENTS_PLUGIN_URL ),  
        array( 'jquery' ), '1.0.0', true);
    wp_register_script( '__TP-rateit-scripts', 
        plugins_url( '/dist/vender/rateit/jquery.rateit.min.js', STUDENTS_PLUGIN_URL ),  
        array( 'jquery' ), '1.0.0', true);

    wp_enqueue_script( '__TP-scripts' );
    wp_enqueue_script( '__TP-rateit-scripts' );

    

    wp_localize_script( '__TP-scripts', 'student_obj', array (

            'ajax_url' => admin_url( 'admin-ajax.php' ),
            'home_url' => home_url( '/' ),
        )
    );
    


}

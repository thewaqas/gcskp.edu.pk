<?php


function __TP_rate_student() {
    
    global $wpdb;

    $output         =   [ 'status' => 1 ];

    $student_option =   get_option( '__TP_opts' );

    if ( !is_user_logged_in() && $student_option['rating_login_required'] == 2 ) {
        wp_send_json( $output );
    }


    $post_ID        =   absint( $_POST['rid'] );
    $rating         =   round( $_POST['rating'], 1 );
    $user_IP        =   $_SERVER['REMOTE_ADDR'];

    $rating_count   = $wpdb->get_var(
        $wpdb->prepare(
            "SELECT COUNT(*) FROM `".$wpdb->prefix ."student_ratings` 
            WHERE student_id=%d AND user_ip=%s",
            $post_ID, $user_IP
        )    
    );

    if ( $rating_count > 0 ) {
        wp_send_json( $output );
    }

    // Insert Rating into database
    $wpdb->insert(
        $wpdb->prefix . 'student_ratings',
        [
            'student_id'    => $post_ID,
            'rating'        => $rating,
            'user_ip'       => $user_IP,
        ],
        [ '%d', '%f', '%s' ]
    );

    // Update Student Metadata
    $student_data           =   get_post_meta( $post_ID, 'student_data', true );
    $student_data['rating_count']++;   
    $student_data['rating'] =  round( $wpdb->get_var(
        "SELECT AVG(`rating`) FROM `".$wpdb->prefix ."student_ratings` 
        WHERE student_id='" . $post_ID . "'" ), 1
    );  
    update_post_meta( $post_ID, 'student_data', $student_data );

    do_action( 'student_rated', array(
        'post_id'       =>  $post_ID,
        'rating'        =>  $rating_count,
        'user_IP'       =>  $user_IP
    ) );

    $output['status'] = 2;
    wp_send_json( $output );

 
    // Don't forget to stop execution afterward.
    wp_die();
}
<?php

function __TP_filter_student_content( $content ) {

    if ( !is_singular( 'students' ) ) {
        return $content;
    }

    global $post, $wpdb;
    $student_data       =   get_post_meta( $post->ID, 'student_data', true );

    $student_html       =   file_get_contents( 'student-template.php', true );

    $program            =   wp_get_post_terms( $post->ID, 'program' );
    $more_info_url      =   isset( $program[0] ) ? get_term_meta( $program[0]->term_id, 'more_info_url', true ) : '';

    $student_html       =   str_replace( 'RATE_I18N', __("Rating", "student"), $student_html );
    $student_html       =   str_replace( 'STUDENT_ID', $post->ID, $student_html );
    $student_html       =   str_replace( 'STUDENT_RATING', $student_data['rating'], $student_html );
    
    $student_html       =   str_replace(
        "PROGRAM_PH", isset($program[0]) ? $program[0]->name : 'None', $student_html
    );

    if( !empty($more_info_url) ) {
        $student_html   =   str_replace( 
            "MORE_INFO_URL_PH", '<a href="' . $more_info_url . '">More Info</a>', $student_html 
        );
    }else{
        $student_html  =   str_replace( "MORE_INFO_URL_PH", '', $student_html );
    }

    $user_IP            =   $_SERVER['REMOTE_ADDR'];

    $rating_count       = $wpdb->get_var( 
        $wpdb->prepare(
            "SELECT COUNT(*) FROM `".$wpdb->prefix ."student_ratings` 
            WHERE student_id=%d AND user_ip=%s",
            $post->ID, $user_IP       
        )
    );

    if ( $rating_count > 0 ) {
        $student_html = str_replace( 
            'READONLY_PLACEHOLDER', 'data-rateit-READONLY="true"', $student_html 
        );
    } else {
        $student_html = str_replace( 
            'READONLY_PLACEHOLDER', '', $student_html 
        );
    }

    return $student_html . $content;
}
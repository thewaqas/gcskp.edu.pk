<?php

function __TP_save_post_admin( $post_id, $post, $update ) {

    $student_data                   = get_post_meta( $post_id, 'student_data', true );
    $student_data                   = empty( $student_data ) ? [] : $student_data;
    $student_data['rating']         = isset( $student_data['rating'] ) ? absint( $student_data['rating'] ) : 0;
    $student_data['rating_count']   = isset( $student_data['rating_count'] ) ? absint( $student_data['rating_count'] ) : 0;

    update_post_meta( $post_id, 'student_data', $student_data );
}
<?php



function __TP_save_options() {
    
    if ( !current_user_can( 'edit_theme_options' ) ) {
        wp_die( __( 'You are not allowed to be on this page.', '__TP' ) );
    }

    check_admin_referer( '__TP_options_verify' ); 

    $student_opts                               =   get_option( '__TP_opts' );
    $student_opts['rating_login_required']      =   absint( $_POST['__TP_rating_login_required'] );
    $student_opts['submission_login_required']  =   absint( $_POST['__TP_submission_login_required'] );

    update_option( '__TP_opts', $student_opts );

    wp_redirect( admin_url( '/admin.php?page=__TP_plugin_opts&status=1' ) );
}
<div class="col-sm-6 col-lg-4">
    <ul class="list list-marked-arrow">
        <li class="list-item">PROGRAM_PH</li>
        <li class="list-item">MORE_INFO_URL_PH</li>
        <li class="list-item">

            <strong>RATE_I18N:</strong>
            <div 
                id="student_rating" 
                class="rateit" 
                READONLY_PLACEHOLDER data-rateit-value="STUDENT_RATING"
                date-rateit-resetable="false"
                data-rid="STUDENT_ID">
            </div>
        </li>
    </ul>
</div>
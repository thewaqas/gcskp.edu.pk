<?php


function __TP_submit_student_data() {

    $output         =   ['status' => 1 ];

    if( empty( $_POST['title'] ) ) {
        wp_send_json( $output );
    } 

    global $wpdb;

    $title                          =   sanitize_text_field( $_POST['title'] );
    $content                        =   wp_kses_post( $_POST['content'] );
    $student_data                   =   [];
    $student_data['rating']         =   0;
    $student_data['rating_count']   =   0;

    $post_id    =   wp_insert_post( 
        array(
            'post_name'     =>  $title,
            'post_title'    =>  $title,
            'post_content'  =>  $content,
            'post_status'   =>  'pending',
            'post_type'     =>  'students',
        )
    );

    update_post_meta( $post_id, 'student_data', $student_data);

    if ( isset($_POST['attachment_id']) && !empty($_POST['attachment_id']) ) {
        
        require_once( ABSPATH. 'wp-admin/includes/image.php' );

        set_post_thumbnail( $post_id, absint($_POST['attachment_id']) );
    }

    $output['status']       =   2;
    wp_send_json( $output );
}
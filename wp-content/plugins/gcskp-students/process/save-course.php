<?php 


function __TP_save_course_meta( $term_id ) {

    if( !isset($_POST['__TP_more_info_url']) ) {
        return;
    }

    update_term_meta( 
        $term_id, 
        'more_info_url', 
        esc_url( 
            $_POST['__TP_more_info_url'] 
        ), 
    );

}
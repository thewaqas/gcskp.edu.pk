<?php
/* 
Plugin Name:  _themename _pluginname
Plugin URI:   https://thewaqas.com/plugins/
Description:  This is an <strong>Admissions Management</strong> plugin for _themename
Version:      1.0.0
Author:       Waqas A.
Author URI:   https://thewaqas.com/
License:      GPL2
License URI:  https://www.gnu.org/licenses/gpl-2.0.html
Text Domain:  _themename-_pluginname
Domain Path:  /languages
*/
/*
Before bundling this plugin I need to replace these values
__TP     =>  _themename__pluginname

*/ 

if( !defined('WPINC')) {
    die;
}


// !<===============================> Setup <====================================>! // 

define( 'STUDENTS_PLUGIN_URL', __FILE__ );
 


// !<===============================> Includes <====================================>! // 

include_once( 'includes/activate.php'       );
include_once( 'includes/init.php'           );

include_once( 'includes/front/enqueue.php'  );
include_once( 'includes/front/logout-link.php' );

include_once( 'includes/admin/init.php'     );
include_once( 'includes/admin/dashboard-widgets.php');
include_once( 'includes/admin/menus.php'    );
include_once( 'includes/admin/options-page.php');
include_once( 'includes/admin/course-fields.php');


include_once( 'process/save-post.php'       );
include_once( 'process/filter-content.php'  );
include_once( 'process/rate-student.php'    );
include_once( 'process/submit-student-data.php' );
include_once( 'process/create-account.php'  );
include_once( 'process/login.php'           );
include_once( 'process/save-options.php'    );
include_once( 'process/save-course.php'    );

include_once( 'includes/shortcodes/creator.php'     );
include_once( 'includes/shortcodes/auth-form.php'   );
// ?include_once( 'includes/shortcodes/auth-from-alt.php');
include_once( 'includes/shortcodes/twitter-follow.php');

include_once( 'blocks/enqueue.php' );



// !<===============================> Hooks <====================================>! // 
register_activation_hook    ( __FILE__, '__TP_ativate_plugin'  );
register_deactivation_hook  ( __FILE__, '__TP_deativate_plugin');


/* Disable WordPress Admin Bar for all users */
add_filter( 'show_admin_bar',                   '__return_false' );

add_action( 'init',                             '__TP_init'                     );
add_action( 'save_post_students',               '__TP_save_post_admin', 10, 3   );
add_filter( 'the_content',                      '__TP_filter_student_content'   );
add_action( 'wp_enqueue_scripts',               '__TP_enqueue_scripts', 100     );

add_action( 'wp_ajax___TP_rate_student',        '__TP_rate_student'             );
add_action( 'wp_ajax_nopriv___TP_rate_student', '__TP_rate_student'             );

add_action( 'admin_init',                       '__TP_students_admin_init'      );
add_action( 'enqueue_block_editor_assets',      '__TP_enqueue_block_editor_assets');
add_action( 'enqueue_block_assets',             '__TP_enqueue_block_assets'     );

add_action( 'wp_ajax___TP_submit_student_data',         '__TP_submit_student_data' );
add_action( 'wp_ajax_nopriv___TP_submit_student_data',  '__TP_submit_student_data' );

add_action( 'wp_ajax___TP_create_account',              '__TP_create_account' );
add_action( 'wp_ajax_nopriv___TP_create_account',       '__TP_create_account' );

add_action( 'wp_ajax___TP_user_login',                  '__TP_user_login' );
add_action( 'wp_ajax_nopriv___TP_user_login',           '__TP_user_login' );

// ?add_filter( 'authenticate', '__TP_alt_authenticate', 100, 3 );
// ?add_filter( 'wp_nav_menu_secondary_items', 'ju_new_nav_menu_items', 999 );
add_filter( 'wp_nav_menu_secondary_items', 'ju_new_nav_menu_items', 999 );

add_action( 'wp_dashboard_setup',       '__TP_dashboard_widgets' );

add_action( 'admin_menu',               '__TP_admin_menu' );

add_action( 'course_add_form_fields',  '__TP_course_add_form_fields' );
add_action( 'course_edit_form_fields', '__TP_course_edit_form_fields' );
add_action( 'create_course',           '__TP_save_course_meta' );
add_action( 'edited_course',           '__TP_save_course_meta' );


// !<===============================> Shortcodes <====================================>! // 
add_shortcode( 'student_creator',       '__TP_student_creator_shortcode' );
add_shortcode( 'student_auth_form',     '__TP_student_auth_form_shortcode' );
// ?add_shortcode( 'student_auth_alt_form', '__TP_student_auth_alt_form_shortcode' );
add_shortcode( 'twitter_follow',       '__TP_twitter_follow_shortcode' );

/**
 *  Normal Comments
 *  * Success Information
 *  ! This is an Alert message
 *  ? Info about the block
 *  TODO: Work to do
 *  @param myPram My parameteres for thsi function
 */

//  Normal Comments
//* Success Information
//! This is an Alert message
//? Info about the block
// TODO: Work to do
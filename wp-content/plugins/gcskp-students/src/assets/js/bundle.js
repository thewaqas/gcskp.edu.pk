(function($) {
    
    $("#student_rating").bind( 'rated', function() {
        $(this).rateit( 'readonly', true );

        var form = {
            action  : '__TP_rate_student',
            rid     : $(this).data( 'rid' ),
            rating  : $(this).rateit( 'value' )
        }
        
        $.post( student_obj.ajax_url, form, function(data) {
            
        });
        
    });

    var featured_frame   =   wp.media({
        title       :   'Select or Upload Media',
        button:     {
            text:       'Use this media'
        },
        multiple:       false
    });

    featured_frame.on( 'select', function() {
        var attachment      =   featured_frame.state().get('selection').first().toJSON();

        $("#student-img-preview").attr( 'src', attachment.url ); 
        $("#__TP_inputImgID").val( attachment.id );

    });

    $(document).on( 'click', '#student-img-upload-btn', function(e) {
        e.preventDefault();

        featured_frame.open();
    });

    $("#student-form").on('submit', function(e) {
        e.preventDefault();

        $(this).hide();

        document.querySelector("#notifications-area").notification({
            text: "Please wait! We are submiting your data",
            icon: '<span class="icon snackbar-icon novi-icon fa-circle-o-notch fa-spin"></span>',
            cls: "snackbar-secondary"
        });
        
        $("#student-status").html(
            `<div class="snackbar snackbar-secondary">
                <div class="snackbar-inner">
                    <div class="snackbar-title" id="student-status">
                        <span class="icon snackbar-icon novi-icon fa-circle-o-notch fa-spin"></span>
                        Please wait! We are submiting your data
                    </div>
                </div>
            </div>`
        );


        var form            =   {

            action          :   '__TP_submit_student_data',
            title           :   $("#__TP_input_title").val(),
            content         :   tinymce.activeEditor.getContent(),
            attachment_id   :   $("#__TP_inputImgID").val()   
        }

        console.log('working');

        $.post( student_obj.ajax_url,   form,   function( data ) {

            if ( data.status == 2 ) {
                $("#student-status").html(
                    `<div class="snackbar snackbar-primary">
                        <div class="snackbar-inner">
                            <div class="snackbar-title">
                                <span class="icon snackbar-icon novi-icon int-check"></span>
                                Submitted Successfully
                            </div>
                        </div>
                    </div>`
                );
            } else {
                $("#student-status").html(
                    `<div class="snackbar snackbar-danger">
                        <div class="snackbar-inner">
                            <div class="snackbar-title">
                                <span class="icon snackbar-icon novi-icon int-warning"></span>
                                Form submission failed
                            </div>
                        </div>
                    </div>`
                );
                $("#student-form").show();
            }
        })

    });

    $("#register-form").on('submit', function(e) {
        e.preventDefault();

        $(this).hide();
        
        document.querySelector("#notifications-area").notification({
            text:   "Processing........",
            icon:   '<span class="icon snackbar-icon novi-icon fa-circle-o-notch fa-spin"></span>',
            cls:    "snackbar-secondary"
        });
       


        var form        =   {

            _wpnonce    :   $('#_wpnonce').val(),
            action      :   '__TP_create_account',
            name        :   $("#register-form-name").val(),
            username    :   $("#register-form-username").val(),
            email       :   $("#register-form-email").val(),
            pass        :   $("#register-form-pass").val(),
            confirm_pass:   $("#register-form-confirm_pass").val(),
            
        }

        $.post( student_obj.ajax_url,   form,   function( data ) {

            if ( data.status == 2 ) {
                
                document.querySelector("#notifications-area").notification({
                    text:   "Account Created !",
                    icon:   '<span class="icon snackbar-icon novi-icon int-check"></span>',
                    cls:    "snackbar-primary"
                });
                location.href   =   student_obj.home_url;
            } else {
                document.querySelector("#notifications-area").notification({
                    text:   "Unable to create an account",
                    icon:   '<span class="icon snackbar-icon novi-icon int-warning"></span>',
                    cls:    "snackbar-danger"
                });

                $("#register-form").show();
            }
        })

    });

    $("#login-form").on('submit', function(e) {
        e.preventDefault();

        $(this).hide();
        
        document.querySelector("#notifications-area").notification({
            text:   "Processing........",
            icon:   '<span class="icon snackbar-icon novi-icon fa-circle-o-notch fa-spin"></span>',
            cls:    "snackbar-secondary"
        });

        var form        =   {

            _wpnonce    :   $('#_wpnonce').val(),
            action      :   '__TP_user_login',
            username    :   $("#login-form-username").val(),
            pass        :   $("#login-form-password").val(),
            
        }

        $.post( student_obj.ajax_url,   form,   function( data ) {

            if ( data.status == 2 ) {
                
                document.querySelector("#notifications-area").notification({
                    text:   "Login Successfully !",
                    icon:   '<span class="icon snackbar-icon novi-icon int-check"></span>',
                    cls:    "snackbar-primary"
                });
                location.href   =   student_obj.home_url;
            } else {
                document.querySelector("#notifications-area").notification({
                    text:   "Unable to login",
                    icon:   '<span class="icon snackbar-icon novi-icon int-warning"></span>',
                    cls:    "snackbar-danger"
                });

                $("#login-form").show();
            }
        })

    });


})(jQuery);

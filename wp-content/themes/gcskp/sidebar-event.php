<!--===== Sidebar =====-->
<div class="col-md-4 col-xxl-3 widget-area">

    <?php
    
    if ( is_active_sidebar( 'event-sidebar' ) ) {
        
        dynamic_sidebar( 'event-sidebar' );
    }

    ?>

    
  
    <div class="widget">
        <h6 class="widget-title">Top post</h6>
        <div class="widget-body">
            <!-- Post simple-->
            <div class="post post-simple post-sm">
                <a class="post-img-link" href="post-intro.html">
                    <img class="lazy-img" src="<?php echo get_theme_file_uri( 'dist/assets/images/image-09-620x398.jpg' ) ?>" data-src="<?php echo get_theme_file_uri( 'dist/assets/images/image-09-620x398.jpg' ) ?>" alt="" width="512" height="330">
                </a>
                <h5 class="post-title">
                    <a href="post-intro.html">Intense is an innovative web design studio</a>
                </h5>
                <div class="post-text">At Intense, we offer cutting-edge web design solutions to the clients who are looking for creative web design solutions as well as for the ways of improving their website customization.</div>
                <a class="btn btn-secondary post-btn" href="post-intro.html">Read more</a>
            </div>
        </div>
    </div>
    
    <div class="widget">
        <h6 class="widget-title">Our gallery</h6>
        <div class="widget-body">
            <div class="group-5" data-lightgallery>
                <a class="thumbnail thumbnail-small lightgallery-item" href="<?php echo get_theme_file_uri( 'dist/assets/images/image-09-620x398.jpg' ) ?>">
                    <img class="lazy-img" src="<?php echo get_theme_file_uri( 'dist/assets/images/image-09-620x398.jpg' ) ?>" data-src="<?php echo get_theme_file_uri( 'dist/assets/images/image-09-620x398.jpg' ) ?>" alt="" width="68" height="68">
                </a>
                <a class="thumbnail thumbnail-small lightgallery-item" href="<?php echo get_theme_file_uri( 'dist/assets/images/image-09-620x398.jpg' ) ?>">
                    <img class="lazy-img" src="<?php echo get_theme_file_uri( 'dist/assets/images/image-09-620x398.jpg' ) ?>" data-src="<?php echo get_theme_file_uri( 'dist/assets/images/image-09-620x398.jpg' ) ?>" alt="" width="68" height="68">
                </a>
                <a class="thumbnail thumbnail-small lightgallery-item" href="<?php echo get_theme_file_uri( 'dist/assets/images/image-09-620x398.jpg' ) ?>">
                    <img class="lazy-img" src="<?php echo get_theme_file_uri( 'dist/assets/images/image-09-620x398.jpg' ) ?>" data-src="<?php echo get_theme_file_uri( 'dist/assets/images/image-09-620x398.jpg' ) ?>" alt="" width="68" height="68">
                </a>
                <a class="thumbnail thumbnail-small lightgallery-item" href="<?php echo get_theme_file_uri( 'dist/assets/images/image-09-620x398.jpg' ) ?>">
                    <img class="lazy-img" src="<?php echo get_theme_file_uri( 'dist/assets/images/image-09-620x398.jpg' ) ?>" data-src="<?php echo get_theme_file_uri( 'dist/assets/images/image-09-620x398.jpg' ) ?>" alt="" width="68" height="68">
                </a>
                <a class="thumbnail thumbnail-small lightgallery-item" href="<?php echo get_theme_file_uri( 'dist/assets/images/image-09-620x398.jpg' ) ?>">
                    <img class="lazy-img" src="<?php echo get_theme_file_uri( 'dist/assets/images/image-09-620x398.jpg' ) ?>" data-src="<?php echo get_theme_file_uri( 'dist/assets/images/image-09-620x398.jpg' ) ?>" alt="" width="68" height="68">
                </a>
                <a class="thumbnail thumbnail-small lightgallery-item" href="<?php echo get_theme_file_uri( 'dist/assets/images/image-09-620x398.jpg' ) ?>">
                    <img class="lazy-img" src="<?php echo get_theme_file_uri( 'dist/assets/images/image-09-620x398.jpg' ) ?>" data-src="<?php echo get_theme_file_uri( 'dist/assets/images/image-09-620x398.jpg' ) ?>" alt="" width="68" height="68">
                </a>
                <a class="thumbnail thumbnail-small lightgallery-item" href="<?php echo get_theme_file_uri( 'dist/assets/images/image-09-620x398.jpg' ) ?>">
                    <img class="lazy-img" src="<?php echo get_theme_file_uri( 'dist/assets/images/image-09-620x398.jpg' ) ?>" data-src="<?php echo get_theme_file_uri( 'dist/assets/images/image-09-620x398.jpg' ) ?>" alt="" width="68" height="68">
                </a>
                <a class="thumbnail thumbnail-small lightgallery-item" href="<?php echo get_theme_file_uri( 'dist/assets/images/image-09-620x398.jpg' ) ?>">
                    <img class="lazy-img" src="<?php echo get_theme_file_uri( 'dist/assets/images/image-09-620x398.jpg' ) ?>" data-src="<?php echo get_theme_file_uri( 'dist/assets/images/image-09-620x398.jpg' ) ?>" alt="" width="68" height="68">
                </a>
            </div>
        </div>
    </div>
    <div class="widget widget-subscribe">
        <h6 class="widget-title">Discover the latest trends in web design and development</h6>
        <div class="widget-body">
            <p class="small">Trying to stay on top of it all? Get the best tools, resources and inspiration sent to your inbox weekly.</p>
            <form class="rd-mailform widget-subscribe-form" data-form-type="subscribe" method="post" action="components/rd-mailform/rd-mailform.php">
                <div class="form-group">
                    <input class="form-control" type="email" name="email" placeholder="Enter your email address" data-constraints="@Email @Required">
                </div>
                <button class="btn btn-secondary btn-block widget-subscribe-btn">Subscribe</button>
            </form>
        </div>
    </div>
    

</div>
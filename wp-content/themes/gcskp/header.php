<!DOCTYPE html>
<html <?php language_attributes() ?>>
    <head>
        <meta charset="<?php bloginfo('charset') ?>">
        <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta property="og:title" content="Govt College Sheikhupura">
        <meta property="og:description" content="College Website">
        <meta property="og:image" content="#">
        <meta property="og:url" content="<?php site_url() ?>">
        <link rel="manifest" crossorigin="use-credentials" href="<?php echo get_theme_file_uri( 'dist/pwa/manifest.json' ) ?>"/>
        <?php wp_head() ?>

        <style>
        .btn-primary { 
            border-color: <?php echo get_theme_mod( '_themename_to_top_color' )?> !important;
            background-color: <?php echo get_theme_mod( '_themename_to_top_color' )?> !important;
        }
        </style>

    </head>
    <body <?php body_class() ?>>
    
        <div class="page">

            <!-- <a class="section section-banner d-none d-xl-flex" href="https://www.templatemonster.com/intense-multipurpose-html-template.html" style="background-image: url(<?php echo get_theme_file_uri( 'dist/assets/images/banners/background-04-1920x60.jpg' ) ?>); background-image: -webkit-image-set( url(<?php echo get_theme_file_uri( 'dist/assets/images/banners/background-04-1920x60.jpg' ) ?>) 1x, url(<?php echo get_theme_file_uri( 'dist/assets/images/banners/background-04-3840x120.jpg' ) ?>) 2x )" target="_blank">
                <img src="<?php echo get_theme_file_uri( 'dist/assets/images/banners/foreground-04-1600x60.png' ) ?>" srcset="<?php echo get_theme_file_uri( 'dist/assets/images/banners/foreground-04-1600x60.png' ) ?> 1x, <?php echo get_theme_file_uri( 'dist/assets/images/banners/foreground-04-3200x120.png' ) ?> 2x" alt="" width="1600" height="310">
            </a> -->

         
            <!-- header (RD Navbar) -->
            <header class="section rd-navbar-wrap" data-preset='{"title":"Navbar Blog","category":"header","reload":true,"id":"navbar-blog"}'>
                <nav class="rd-navbar navbar-blog" data-rd-navbar='{"responsive":{"1200":{"stickUpOffset":"60px"}}}'>
                    
                    <div class="navbar-section navbar-non-stuck">
                        <div class="navbar-container">
                            <div class="navbar-cell flex-grow-1">
                                <div class="navbar-info">
                                    
                                    <?php  

                                    $defaults = array(
                                        'theme_location'  => 'secondary',
                                        'container'       => 'div',
                                        'container_class' => '',
                                        'fallback_cb'     => false,
                                        'items_wrap'      => '<ul class="navbar-navigation rd-navbar-nav">%3$s</ul>',
                                        'depth'           => 1,
                                        'walker'          => new _themename_Custom_Nav_Walker()
                                    );
                                    
                                    if ( has_nav_menu( 'secondary' ) ) {
                                        wp_nav_menu( $defaults ); 
                                    }

                                    ?>
                                    <a class="navbar-info-link" href="mailto:#">
                                        <span class="navbar-info-icon int-email novi-icon"></span>
                                        mail@demolink.org 
                                    </a>
                                    <div class="group-30 d-flex">

                                        <?php if ( get_theme_mod( '_themename_facebook_handle' ) ) : ?>
                                            <a class="navbar-info-icon icon-social int-facebook novi-icon" href="https://facebook.com/<?php echo get_theme_mod( '_themename_facebook_handle' ) ?>" target="_blank"></a>
                                        <?php endif ?>

                                        <?php if ( get_theme_mod( '_themename_twitter_handle' ) ) : ?>
                                            <a class="navbar-info-icon icon-social int-twitter novi-icon" href="https://twitter.com/<?php echo get_theme_mod( '_themename_twitter_handle' ) ?>" target="_blank"></a>
                                        <?php endif ?>

                                        <?php if ( get_theme_mod( '_themename_instagram_handle' ) ) : ?>
                                            <a class="navbar-info-icon icon-social int-instagram novi-icon" href="https://instagram.com/<?php echo get_theme_mod( '_themename_instagram_handle' ) ?>" target="_blank"></a>
                                        <?php endif ?>

                                        <?php if ( get_theme_mod( '_themename_linkedin_handle' ) ) : ?>
                                            <a class="navbar-info-icon icon-social int-linkedin novi-icon" href="https://linkedin.com/in/<?php echo get_theme_mod( '_themename_linkedin_handle' ) ?>" target="_blank"></a>
                                        <?php endif ?>

                                        <?php if ( get_theme_mod( '_themename_youtube_handle' ) ) : ?>
                                            <a class="navbar-info-icon icon-social int-youtube novi-icon" href="https://youtube.com/<?php echo get_theme_mod( '_themename_youtube_handle' ) ?>" target="_blank"></a>
                                        <?php endif ?>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="navbar-section">
                        <div class="navbar-container">
                            <div class="navbar-cell">
                                <div class="navbar-panel">
                                    <button class="navbar-switch int-hamburger novi-icon" data-multi-switch='{"targets":".rd-navbar","scope":".rd-navbar","isolate":"[data-multi-switch]"}'></button>
                                    <div class="navbar-logo">

                                        <?php if ( has_custom_logo() ) : the_custom_logo(); else : ?>
                                            <a class="navbar-logo-link" href="<?php echo home_url('/') ?>">
                                                <?php echo esc_html(bloginfo( 'name' ))  ?>
                                            </a>
                                            <!-- <a class="navbar-logo-link" href="<?php //echo home_url('/') ?>">
                                                <img class="lazy-img navbar-logo-default" src="<?php //echo get_theme_file_uri( 'dist/assets/images/logo-default-114x27.svg' ) ?>" data-src="<?php //echo get_theme_file_uri( 'dist/assets/images/logo-default-114x27.svg' ) ?>" alt="Govt. College Sheikhupura" width="114" height="27" style="">
                                                <img class="lazy-img navbar-logo-inverse" src="<?php //echo get_theme_file_uri( 'dist/assets/images/logo-inverse-114x27.svg' ) ?>" data-src="<?php //echo get_theme_file_uri( 'dist/assets/images/logo-inverse-114x27.svg' ) ?>" alt="Govt. College Sheikhupura" width="114" height="27" style="">
                                            </a> -->
                                        <?php endif ?>
                                    </div>
                                </div>
                            </div>

                            <?php  

                            $defaults = array(
                                'theme_location'  => 'primary',
                                'container'       => 'div',
                                'container_class' => 'navbar-cell navbar-sidebar',
                                'fallback_cb'     => false,
                                'items_wrap'      => '<ul class="navbar-navigation rd-navbar-nav">%3$s</ul>',
                                'depth'           => 0,
                                'walker'          => new _themename_Custom_Nav_Walker()
                            );
                            
                            if ( has_nav_menu( 'primary' ) ) {
                                wp_nav_menu( $defaults ); 
                            }

                            ?>

                            <div class="navbar-cell">
                                <div class="navbar-subpanel">

                                    <div class="navbar-subpanel-item">
                                        <button class="navbar-button navbar-info-button mdi-dots-vertical novi-icon" data-multi-switch='{"targets":".rd-navbar","scope":".rd-navbar","class":"navbar-info-active","isolate":"[data-multi-switch]"}'></button>
                                    </div>

                                    <?php if( get_theme_mod( '_themename_header_show_search') ) : ?>

                                        <div id="header_search" class="navbar-subpanel-item">
                                            <div class="navbar-search">
                                                <div class="navbar-search-container">
                                                    <form class="navbar-search-form" action="<?php echo esc_url( home_url() )?>" method="GET">
                                                        <input class="navbar-search-input" type="text" placeholder="<?php esc_attr_e( 'Enter search terms...', 'label', '_themename' )?>" autocomplete="off" name="s" value="<?php get_search_query() ?>">
                                                        <button class="navbar-search-btn int-search novi-icon"></button>
                                                        <button class="navbar-search-close search-switch int-close novi-icon" type="button" data-multi-switch='{"targets":".rd-navbar","scope":".rd-navbar","class":"navbar-search-active","isolate":"[data-multi-switch]:not(.search-switch)"}'></button>
                                                    </form>
                                                </div>
                                            </div>
                                            <div class="navbar-search-results">No results</div>
                                            <button class="navbar-button search-switch int-search novi-icon" data-multi-switch='{"targets":".rd-navbar","scope":".rd-navbar","class":"navbar-search-active","isolate":"[data-multi-switch]:not(.search-switch)"}'></button>
                                        </div>

                                    <?php endif ?>

                                    <?php if( get_theme_mod( '_themename_header_show_user') ) : ?>

                                        <div id="header_user" class="navbar-subpanel-item">
                                            <button class="navbar-button int-user novi-icon" data-multi-switch='{"targets":".rd-navbar","scope":".rd-navbar","class":"navbar-cart-active","isolate":"[data-multi-switch]"}'></button>
                                            <div class="navbar-cart">
                                                <div class="col-sm-12 text-center">
                                                    <img class="lazy-img rounded-circle" src="<?php echo get_theme_file_uri( 'dist/assets/images/product-03-80x103.jpg' ) ?>" data-src="<?php echo get_theme_file_uri( 'dist/assets/images/product-03-80x103.jpg' ) ?>" alt="" width="200" height="auto">
                                                    <a class="btn btn-lg btn-primary" href="contact-me.html">Free Consultation</a>
                                                    <div class="offset-sm">
                                                        <div class="d-inline-flex align-items-center">
                                                            <span class="icon icon-xs novi-icon int-phone mr-2"></span>
                                                            <div class="h5">
                                                                <a href="tel:#">+1 234 567 8901</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="offset-xxs">
                                                        <a class="font-weight-normal" href="mailto:#">jwilson@demolink.org</a>
                                                    </div>
                                                    <div class="offset-sm">
                                                        <div class="group-30 d-inline-flex text-900">
                                                            <a class="icon icon-sm icon-social int-youtube novi-icon" href="#"></a>
                                                            <a class="icon icon-sm icon-social int-facebook novi-icon" href="#"></a>
                                                            <a class="icon icon-sm icon-social int-instagram novi-icon" href="#"></a>
                                                            <a class="icon icon-sm icon-social int-twitter novi-icon" href="#"></a>
                                                            <a class="icon icon-sm icon-social int-linkedin novi-icon" href="#"></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    <?php endif ?>

                                </div>
                            </div>

                        </div>
                    </div>
                </nav>
            </header>
            <!-- #end header -->

            <?php 
                if ( is_page() || site_url( '/blog' ) ) {
                    //get_template_part( '/template-parts/breadcrumb' );
                }
            ?>
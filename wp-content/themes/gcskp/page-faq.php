<?php get_header() ?>



<!-- Intro-->
<section class="section section-sm context-dark bg-image" style="padding-bottom: 8%; background-image: url(<?php echo get_theme_file_uri('dist/assets/images/image-29-1920x553.jpg') ?>);" data-preset='{"title":"Intro","category":"intro","reload":false,"id":"intro-2"}'>
    <div class="container">
        <!-- Breadcrumb-->
        <ul class="breadcrumb d-inline-flex justify-content-center text-center">
            <li class="breadcrumb-item">
                <a class="breadcrumb-link" href="index.html">Home</a>
            </li>
            <!-- <li class="breadcrumb-item">
                <a class="breadcrumb-link" href="#">Pages</a>
            </li> -->
            <li class="breadcrumb-item">
                <span class="breadcrumb-text breadcrumb-active">FAQ</span>
            </li>
        </ul>
        <h1>
            Frequently asked<br>questions
        </h1>
        <h5>All you need to know about Intense design studio and how to get Support.</h5>
    </div>
</section>

<!-- General Questions-->
<section class="section section-lg bg-transparent novi-background" data-preset='{"title":"General Questions","category":"accordion, faq","reload":true,"id":"general-questions"}'>
    <div class="container">
        <h6 class="text-primary">General Questions</h6>
        <h2>
            All you need to know about Intense design<br>studio and how to get Support.
        </h2>
        <div class="accordion accordion-boxed">
            <div class="accordion-item accordion-general-question-1">
                <a class="accordion-header accordion-general-question-group" href="#" data-multi-switch='{"targets":".accordion-general-question-1","isolate":".accordion-general-question-group","state":true}'>Who can take advantage of Intense?</a>
                <div class="accordion-body accordion-general-question-1" data-multi-switch-target-slide>
                    <div class="row">
                        <div class="col-lg-9 movable">
                            <h4>Intense is the most powerful Bootstrap theme on TemplateMonster</h4>
                            <p>Intense is a flexible instrument for professional developers. It comes packed with 250+ pre-designed pages, 20 premium plugins, 50+ scalable shortcodes, and an extended Bootstrap toolkit.</p>
                            <ul class="list list-marked-arrow ml-5">
                                <li class="list-item">Fully responsive design</li>
                                <li class="list-item">Social integration</li>
                                <li class="list-item">30+ predesigned pages</li>
                                <li class="list-item">Regular content updates</li>
                                <li class="list-item">Lots of child themes</li>
                            </ul>
                            <hr class="divider divider-sm">
                            <h4>Intense is the most powerful Bootstrap theme on TemplateMonster</h4>
                            <p>Intense is a flexible instrument for professional developers. It comes packed with 250+ pre-designed pages, 20 premium plugins, 50+ scalable shortcodes, and an extended Bootstrap toolkit.</p>
                            <ul class="list list-marked-arrow ml-5">
                                <li class="list-item">Fully responsive design</li>
                                <li class="list-item">Social integration</li>
                                <li class="list-item">30+ predesigned pages</li>
                                <li class="list-item">Regular content updates</li>
                                <li class="list-item">Lots of child themes</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="accordion-item accordion-general-question-2">
                <a class="accordion-header accordion-general-question-group" href="#" data-multi-switch='{"targets":".accordion-general-question-2","isolate":".accordion-general-question-group"}'>How can web developers benefit from Intense?</a>
                <div class="accordion-body accordion-general-question-2" data-multi-switch-target-slide>
                    <div class="row">
                        <div class="col-lg-9 movable">
                            <h4>Intense is the most powerful Bootstrap theme on TemplateMonster</h4>
                            <p>Intense is a flexible instrument for professional developers. It comes packed with 250+ pre-designed pages, 20 premium plugins, 50+ scalable shortcodes, and an extended Bootstrap toolkit.</p>
                            <ul class="list list-marked-arrow ml-5">
                                <li class="list-item">Fully responsive design</li>
                                <li class="list-item">Social integration</li>
                                <li class="list-item">30+ predesigned pages</li>
                                <li class="list-item">Regular content updates</li>
                                <li class="list-item">Lots of child themes</li>
                            </ul>
                            <hr class="divider divider-sm">
                            <h4>Intense is the most powerful Bootstrap theme on TemplateMonster</h4>
                            <p>Intense is a flexible instrument for professional developers. It comes packed with 250+ pre-designed pages, 20 premium plugins, 50+ scalable shortcodes, and an extended Bootstrap toolkit.</p>
                            <ul class="list list-marked-arrow ml-5">
                                <li class="list-item">Fully responsive design</li>
                                <li class="list-item">Social integration</li>
                                <li class="list-item">30+ predesigned pages</li>
                                <li class="list-item">Regular content updates</li>
                                <li class="list-item">Lots of child themes</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="accordion-item accordion-general-question-3">
                <a class="accordion-header accordion-general-question-group" href="#" data-multi-switch='{"targets":".accordion-general-question-3","isolate":".accordion-general-question-group"}'>Who can take advantage of Intense?</a>
                <div class="accordion-body accordion-general-question-3" data-multi-switch-target-slide>
                    <div class="row">
                        <div class="col-lg-9 movable">
                            <h4>Intense is the most powerful Bootstrap theme on TemplateMonster</h4>
                            <p>Intense is a flexible instrument for professional developers. It comes packed with 250+ pre-designed pages, 20 premium plugins, 50+ scalable shortcodes, and an extended Bootstrap toolkit.</p>
                            <ul class="list list-marked-arrow ml-5">
                                <li class="list-item">Fully responsive design</li>
                                <li class="list-item">Social integration</li>
                                <li class="list-item">30+ predesigned pages</li>
                                <li class="list-item">Regular content updates</li>
                                <li class="list-item">Lots of child themes</li>
                            </ul>
                            <hr class="divider divider-sm">
                            <h4>Intense is the most powerful Bootstrap theme on TemplateMonster</h4>
                            <p>Intense is a flexible instrument for professional developers. It comes packed with 250+ pre-designed pages, 20 premium plugins, 50+ scalable shortcodes, and an extended Bootstrap toolkit.</p>
                            <ul class="list list-marked-arrow ml-5">
                                <li class="list-item">Fully responsive design</li>
                                <li class="list-item">Social integration</li>
                                <li class="list-item">30+ predesigned pages</li>
                                <li class="list-item">Regular content updates</li>
                                <li class="list-item">Lots of child themes</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="accordion-item accordion-general-question-4">
                <a class="accordion-header accordion-general-question-group" href="#" data-multi-switch='{"targets":".accordion-general-question-4","isolate":".accordion-general-question-group"}'>Who can take advantage of Intense?</a>
                <div class="accordion-body accordion-general-question-4" data-multi-switch-target-slide>
                    <div class="row">
                        <div class="col-lg-9 movable">
                            <h4>Intense is the most powerful Bootstrap theme on TemplateMonster</h4>
                            <p>Intense is a flexible instrument for professional developers. It comes packed with 250+ pre-designed pages, 20 premium plugins, 50+ scalable shortcodes, and an extended Bootstrap toolkit.</p>
                            <ul class="list list-marked-arrow ml-5">
                                <li class="list-item">Fully responsive design</li>
                                <li class="list-item">Social integration</li>
                                <li class="list-item">30+ predesigned pages</li>
                                <li class="list-item">Regular content updates</li>
                                <li class="list-item">Lots of child themes</li>
                            </ul>
                            <hr class="divider divider-sm">
                            <h4>Intense is the most powerful Bootstrap theme on TemplateMonster</h4>
                            <p>Intense is a flexible instrument for professional developers. It comes packed with 250+ pre-designed pages, 20 premium plugins, 50+ scalable shortcodes, and an extended Bootstrap toolkit.</p>
                            <ul class="list list-marked-arrow ml-5">
                                <li class="list-item">Fully responsive design</li>
                                <li class="list-item">Social integration</li>
                                <li class="list-item">30+ predesigned pages</li>
                                <li class="list-item">Regular content updates</li>
                                <li class="list-item">Lots of child themes</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="accordion-item accordion-general-question-5">
                <a class="accordion-header accordion-general-question-group" href="#" data-multi-switch='{"targets":".accordion-general-question-5","isolate":".accordion-general-question-group"}'>How can web developers benefit from Intense?</a>
                <div class="accordion-body accordion-general-question-5" data-multi-switch-target-slide>
                    <div class="row">
                        <div class="col-lg-9 movable">
                            <h4>Intense is the most powerful Bootstrap theme on TemplateMonster</h4>
                            <p>Intense is a flexible instrument for professional developers. It comes packed with 250+ pre-designed pages, 20 premium plugins, 50+ scalable shortcodes, and an extended Bootstrap toolkit.</p>
                            <ul class="list list-marked-arrow ml-5">
                                <li class="list-item">Fully responsive design</li>
                                <li class="list-item">Social integration</li>
                                <li class="list-item">30+ predesigned pages</li>
                                <li class="list-item">Regular content updates</li>
                                <li class="list-item">Lots of child themes</li>
                            </ul>
                            <hr class="divider divider-sm">
                            <h4>Intense is the most powerful Bootstrap theme on TemplateMonster</h4>
                            <p>Intense is a flexible instrument for professional developers. It comes packed with 250+ pre-designed pages, 20 premium plugins, 50+ scalable shortcodes, and an extended Bootstrap toolkit.</p>
                            <ul class="list list-marked-arrow ml-5">
                                <li class="list-item">Fully responsive design</li>
                                <li class="list-item">Social integration</li>
                                <li class="list-item">30+ predesigned pages</li>
                                <li class="list-item">Regular content updates</li>
                                <li class="list-item">Lots of child themes</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- FAQ-->
<section class="section section-lg bg-transparent novi-background" data-preset='{"title":"FAQ","category":"faq, accordion","reload":true,"id":"faq-4"}'>
    <div class="container">
        <h6 class="text-primary">Other Questions</h6>
        <h2>
            The answers on most common questions are<br>described bellow.
        </h2>
        <div class="accordion accordion-boxed">
            <div class="accordion-item accordion-6">
                <a class="accordion-header accordion-group-2" href="#" data-multi-switch='{"targets":".accordion-6","isolate":".accordion-group-2"}'>Who can take advantage of Intense?</a>
                <div class="accordion-body accordion-6" data-multi-switch-target-slide>
                    <div class="row">
                        <div class="col-lg-9">
                            <h4>Intense is the most powerful Bootstrap theme on TemplateMonster</h4>
                            <p>Intense is a flexible instrument for professional developers. It comes packed with 250+ pre-designed pages, 20 premium plugins, 50+ scalable shortcodes, and an extended Bootstrap toolkit.</p>
                            <ul class="list list-marked-arrow ml-5">
                                <li class="list-item">Fully responsive design</li>
                                <li class="list-item">Social integration</li>
                                <li class="list-item">30+ predesigned pages</li>
                                <li class="list-item">Regular content updates</li>
                                <li class="list-item">Lots of child themes</li>
                            </ul>
                            <hr class="divider divider-sm">
                            <h4>Intense is the most powerful Bootstrap theme on TemplateMonster</h4>
                            <p>Intense is a flexible instrument for professional developers. It comes packed with 250+ pre-designed pages, 20 premium plugins, 50+ scalable shortcodes, and an extended Bootstrap toolkit.</p>
                            <ul class="list list-marked-arrow ml-5">
                                <li class="list-item">Fully responsive design</li>
                                <li class="list-item">Social integration</li>
                                <li class="list-item">30+ predesigned pages</li>
                                <li class="list-item">Regular content updates</li>
                                <li class="list-item">Lots of child themes</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="accordion-item accordion-7">
                <a class="accordion-header accordion-group-2" href="#" data-multi-switch='{"targets":".accordion-7","isolate":".accordion-group-2"}'>How can web developers benefit from Intense?</a>
                <div class="accordion-body accordion-7" data-multi-switch-target-slide>
                    <div class="row">
                        <div class="col-lg-9">
                            <h4>Intense is the most powerful Bootstrap theme on TemplateMonster</h4>
                            <p>Intense is a flexible instrument for professional developers. It comes packed with 250+ pre-designed pages, 20 premium plugins, 50+ scalable shortcodes, and an extended Bootstrap toolkit.</p>
                            <ul class="list list-marked-arrow ml-5">
                                <li class="list-item">Fully responsive design</li>
                                <li class="list-item">Social integration</li>
                                <li class="list-item">30+ predesigned pages</li>
                                <li class="list-item">Regular content updates</li>
                                <li class="list-item">Lots of child themes</li>
                            </ul>
                            <hr class="divider divider-sm">
                            <h4>Intense is the most powerful Bootstrap theme on TemplateMonster</h4>
                            <p>Intense is a flexible instrument for professional developers. It comes packed with 250+ pre-designed pages, 20 premium plugins, 50+ scalable shortcodes, and an extended Bootstrap toolkit.</p>
                            <ul class="list list-marked-arrow ml-5">
                                <li class="list-item">Fully responsive design</li>
                                <li class="list-item">Social integration</li>
                                <li class="list-item">30+ predesigned pages</li>
                                <li class="list-item">Regular content updates</li>
                                <li class="list-item">Lots of child themes</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="accordion-item accordion-8">
                <a class="accordion-header accordion-group-2" href="#" data-multi-switch='{"targets":".accordion-8","isolate":".accordion-group-2"}'>Who can take advantage of Intense?</a>
                <div class="accordion-body accordion-8" data-multi-switch-target-slide>
                    <div class="row">
                        <div class="col-lg-9">
                            <h4>Intense is the most powerful Bootstrap theme on TemplateMonster</h4>
                            <p>Intense is a flexible instrument for professional developers. It comes packed with 250+ pre-designed pages, 20 premium plugins, 50+ scalable shortcodes, and an extended Bootstrap toolkit.</p>
                            <ul class="list list-marked-arrow ml-5">
                                <li class="list-item">Fully responsive design</li>
                                <li class="list-item">Social integration</li>
                                <li class="list-item">30+ predesigned pages</li>
                                <li class="list-item">Regular content updates</li>
                                <li class="list-item">Lots of child themes</li>
                            </ul>
                            <hr class="divider divider-sm">
                            <h4>Intense is the most powerful Bootstrap theme on TemplateMonster</h4>
                            <p>Intense is a flexible instrument for professional developers. It comes packed with 250+ pre-designed pages, 20 premium plugins, 50+ scalable shortcodes, and an extended Bootstrap toolkit.</p>
                            <ul class="list list-marked-arrow ml-5">
                                <li class="list-item">Fully responsive design</li>
                                <li class="list-item">Social integration</li>
                                <li class="list-item">30+ predesigned pages</li>
                                <li class="list-item">Regular content updates</li>
                                <li class="list-item">Lots of child themes</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer() ?>
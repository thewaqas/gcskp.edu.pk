<?php if( have_posts() ) : ?>

    <?php while( have_posts() ) : the_post() ?>

        <?php get_template_part( 'template-parts/posts/content', get_post_format() ) ?>

    <?php endwhile ?>

    
<?php else : ?>

    <?php get_template_part( 'template-parts/posts/content', 'none' ) ?>

<?php endif ?> 
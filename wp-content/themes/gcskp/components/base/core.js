"use strict"
;(function () {
  const coreDefaults = {
      root: document.documentElement,
      logs: false,
      iePolyfill: "https://polyfill.io/v3/polyfill.min.js?features=Array.from%2CCustomEvent%2CNodeList.prototype.forEach%2CObject.assign%2CIntersectionObserver%2CPromise",
      ieHandler: null,
      onScriptsReady: null,
      onStylesReady: null,
      onReady: null,
      onError: errorHandler
    },
    componentDefaults = {
      owner: null,
      name: null,
      selector: null,
      style: null,
      script: null,
      init: null,
      priority: 2
    }
  function objectTag(data) {
    return Object.prototype.toString.call(data).slice(8, -1)
  }
  function merge(source, merged) {
    for (let key in merged) {
      if (objectTag(merged[key]) === "Object") {
        if (typeof source[key] !== "object") source[key] = {}
        source[key] = merge(source[key], merged[key])
      } else {
        source[key] = merged[key]
      }
    }
    return source
  }
  function ieDetect() {
    let ua = window.navigator.userAgent,
      msie = ua.indexOf("MSIE "),
      trident = ua.indexOf("Trident/"),
      edge = ua.indexOf("Edge/")
    if (msie > 0) {
      return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)), 10)
    }
    if (trident > 0) {
      let rv = ua.indexOf("rv:")
      return parseInt(ua.substring(rv + 3, ua.indexOf(".", rv)), 10)
    }
    if (edge > 0) {
      return parseInt(ua.substring(edge + 5, ua.indexOf(".", edge)), 10)
    }
    return null
  }
  function errorHandler() {
    if (!document.querySelector("#gcskp-core-error")) {
      let node = document.createElement("div")
      node.setAttribute("id", "gcskp-core-error")
      node.setAttribute("style", "position: fixed; bottom: 1vh; left: 1vw; z-index: 1000; max-width: 98vw; padding: 10px 15px; border-radius: 4px; font-family: monospace; background: #f2564d; color: white;")
      node.innerText = "There was an error on this page, please try again later."
      document.body.appendChild(node)
    }
  }
  function parallel(params, cb) {
    let inclusions = []
    params.forEach(function (path) {
      inclusions.push(cb(path))
    })
    return Promise.all(inclusions)
  }
  function series(params, cb) {
    let chain = Promise.resolve()
    params.forEach(function (path) {
      chain = chain.then(cb.bind(null, path))
    })
    return chain
  }
  function includeStyle(path) {
    return new Promise(function (resolve, reject) {
      if (document.querySelector(`link[href="${path}"]`)) {
        resolve()
      } else {
        let link = document.createElement("link")
        link.setAttribute("rel", "stylesheet")
        link.setAttribute("href", path)
        link.addEventListener("load", resolve)
        link.addEventListener("error", reject)
        document.querySelector("head").appendChild(link)
      }
    })
  }
  function includeScript(path) {
    return new Promise(function (resolve, reject) {
      let node = document.querySelector(`script[src="${path}"]`)
      if (node) {
        if (node.getAttribute("data-loaded") === "true") {
          resolve()
        } else {
          node.addEventListener("load", resolve)
          node.addEventListener("error", reject)
        }
      } else {
        let script = document.createElement("script")
        script.src = path
        script.addEventListener("load", function () {
          script.setAttribute("data-loaded", "true")
          resolve()
        })
        script.addEventListener("error", reject)
        document.querySelector("head").appendChild(script)
      }
    })
  }
  function inViewport(node) {
    let bound = node.getBoundingClientRect()
    return !(bound.bottom < 0 || bound.top > window.innerHeight)
  }
  function nearViewport(node) {
    let bound = node.getBoundingClientRect()
    return !(bound.bottom < 0 - window.innerHeight || bound.top > 0) || !(bound.bottom < window.innerHeight || bound.top > window.innerHeight * 2)
  }
  function GcskpComponent(params) {
    merge(this, componentDefaults)
    merge(this, params)
    this.state = "pending"
    if (!this.node.GcskpComponent) this.node.GcskpComponent = {}
    this.node.GcskpComponent[this.name] = this
  }
  GcskpComponent.prototype.log = function (...args) {
    if (this.owner.logs) console.log(`%c[${this.name}]`, "color: lightgreen; font-weight: 900;", ...args)
  }
  GcskpComponent.prototype.error = function (...args) {
    console.error(`[${this.name}] error\n`, ...args)
    if (this.owner.onError instanceof Function) this.owner.onError.call(this, ...args)
  }
  GcskpComponent.prototype.load = function () {
    let stylesState = Promise.resolve(),
      scriptsState = Promise.resolve()
    if (this.style && this.style.length) {
      stylesState = stylesState.then(parallel.bind(this, this.style, includeStyle)).catch(this.error.bind(this))
    }
    if (this.script && this.script.length) {
      scriptsState = scriptsState.then(series.bind(this, this.script, includeScript)).catch(this.error.bind(this))
    }
    if (this.init && this.init instanceof Function) {
      scriptsState = scriptsState.then(this.init.bind(this, this.node)).catch(this.error.bind(this))
    }
    stylesState.then(() => {
      this.node.dispatchEvent(new CustomEvent(`${this.name}:stylesReady`))
    })
    scriptsState.then(() => {
      this.node.dispatchEvent(new CustomEvent(`${this.name}:scriptsReady`))
    })
    Promise.all([stylesState, scriptsState]).then(() => {
      this.state = "ready"
      this.node.dispatchEvent(new CustomEvent(`${this.name}:ready`))
    })
    return {
      scriptsState: scriptsState,
      stylesState: stylesState
    }
  }
  Object.defineProperty(GcskpComponent.prototype, Symbol.toStringTag, {
    get: function () {
      return "GcskpComponent"
    }
  })
  function GcskpCore(params) {
    merge(this, coreDefaults)
    merge(this, params)
    this.ie = ieDetect()
    if (this.ie !== null && this.ie < 12) {
      console.warn("[GcskpCore] detected IE" + this.ie + ", load polyfills")
      let script = document.createElement("script")
      script.src = this.iePolyfill
      document.querySelector("head").appendChild(script)
      script.addEventListener("load", () => {
        if (this.ieHandler instanceof Function) this.ieHandler.call(this, this.ie)
      })
    }
    this.registry = {}
    this.components = []
  }
  GcskpCore.prototype.log = function (...args) {
    if (this.logs) console.log("%c[GcskpCore]", "color: orange; font-weight: 900;", ...args)
  }
  GcskpCore.prototype.register = function (params) {
    let entry = (this.registry[params.name] = params)
    entry.nodes = []
    if (entry.style && !(entry.style instanceof Array)) {
      entry.style = [entry.style]
    }
    if (entry.script && !(entry.script instanceof Array)) {
      entry.script = [entry.script]
    }
  }
  GcskpCore.prototype.prepare = function (root) {
    root = root || this.root
    for (let key in this.registry) {
      let entry = this.registry[key],
        nodes = [this.root]
      if (entry.selector) {
        nodes = Array.from(root.querySelectorAll(entry.selector))
        if (root.nodeType === Node.ELEMENT_NODE && root.matches(entry.selector)) {
          nodes.unshift(root)
        }
      }
      nodes.forEach((node) => {
        if (!node.GcskpComponent || !node.GcskpComponent[entry.name]) {
          let component = new GcskpComponent(
            merge(
              {
                owner: this,
                entry: entry,
                node: node
              },
              entry
            )
          )
          if (inViewport(node)) {
            component.priority = 0
          } else if (nearViewport(node)) {
            component.priority = 1
          } else {
          }
          this.components.push(component)
          entry.nodes.push(node)
        }
      })
    }
  }
  GcskpCore.prototype.queue = function (components, priority, throwEvent) {
    let queue = {
      styles: [],
      scripts: []
    }
    components.forEach((component) => {
      let componentPromises = component.load()
      queue.styles.push(componentPromises.stylesState)
      queue.scripts.push(componentPromises.scriptsState)
    })
    let promise = Promise.all([Promise.all(queue.styles), Promise.all(queue.scripts)])
    if (throwEvent) {
      promise.then(() => {
        if (this.onReady instanceof Function) this.onReady.call(this, priority)
        let event = new CustomEvent("ready")
        event.priority = priority
        this.root.dispatchEvent(event)
      })
    }
    return promise
  }
  GcskpCore.prototype.init = function (throwEvent) {
    let series = [],
      chain = Promise.resolve()
    this.components.forEach(function (component) {
      if (component.state === "pending") {
        component.state = "queue"
        if (!series[component.priority]) series[component.priority] = []
        series[component.priority].push(component)
      }
    })
    series.forEach((set, priority) => {
      chain = chain.then(() => {
        return this.queue(set, priority, throwEvent)
      })
    })
    return chain
  }
  GcskpCore.prototype.observe = function () {
    let tId = null
    const observer = new MutationObserver((mutationsList) => {
      mutationsList.forEach((mutation) => {
        if (mutation.type === "childList" && mutation.addedNodes.length) {
          mutation.addedNodes.forEach((node) => {
            if (node.nodeType === Node.ELEMENT_NODE) {
              if (tId) clearTimeout(tId)
              this.prepare(node)
              tId = setTimeout(() => {
                let tmp = []
                core.components.forEach(function (component) {
                  if (component.state === "pending") {
                    tmp.push(component)
                  }
                })
                this.init()
              }, 100)
            }
          })
        }
      })
    })
    observer.observe(this.root, {
      childList: true,
      subtree: true
    })
  }
  Object.defineProperty(GcskpCore.prototype, Symbol.toStringTag, {
    get: function () {
      return "GcskpCore"
    }
  })
  if (!window.GcskpCore) {
    window.GcskpCore = GcskpCore
  }
})()

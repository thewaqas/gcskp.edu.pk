(function() {
    function merge(source, merged) {
        for (let key in merged) {
            if (merged[key]instanceof Object && merged[key].constructor.name === 'Object') {
                if (typeof (source[key]) !== 'object')
                    source[key] = {};
                source[key] = merge(source[key], merged[key]);
            } else {
                source[key] = merged[key];
            }
        }
        return source;
    }
    function SimpleNotification(params) {
        if (!params || !params.parent || !(params.parent instanceof Element)) {
            throw new Error('SimpleNotification.parent must be an Element');
        }
        merge(this, {
            timeout: 2000,
            transition: 250,
            template: '{text}',
            autoshow: true,
            fields: {
                text: ''
            },
            _tId: {
                show: null,
                hide: null,
                kill: null
            }
        });
        merge(this, params);
        this.node = document.createElement('div');
        this.node.className = 'notification';
        this.node.innerHTML = this.procTemplate();
        this.node.simpleNotification = this;
        if (this.autoshow) {
            this.show();
        }
    }
    SimpleNotification.prototype.procTemplate = function() {
        let markup = this.template;
        Object.keys(this.fields).forEach((key)=>{
            let regexp = new RegExp('\\{' + (key) + '\\}');
            markup = markup.replace(regexp, this.fields[key] || '');
        }
        );
        return markup;
    }
    ;
    SimpleNotification.prototype.show = function() {
        this.parent.appendChild(this.node);
        this._tId.show = setTimeout(()=>{
            this.node.classList.add('notification-active');
        }
        , 0);
        if (this.timeout) {
            this._tId.hide = setTimeout(()=>{
                this.hide();
            }
            , this.timeout);
        }
    }
    ;
    SimpleNotification.prototype.hide = function() {
        this.node.classList.remove('notification-active');
        this._tId.kill = setTimeout(()=>{
            this.remove();
        }
        , this.transition);
    }
    ;
    SimpleNotification.prototype.remove = function() {
        this.node.remove();
        for (let key in this._tId) {
            clearTimeout(this._tId[key]);
        }
    }
    ;
    if (!window.SimpleNotification) {
        window.SimpleNotification = SimpleNotification;
    } else {
        throw new Error('SimpleNotification is already defined or occupied');
    }
}
)();

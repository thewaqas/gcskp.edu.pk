<?php while( have_posts() ) : the_post() ?>

    <?php get_template_part( 'template-parts/page/content' ) ?>

    <?php if ( comments_open() || get_comments_number() ) comments_template(); ?>

<?php endwhile ?>

<?php 

//Setup
define ( '_themename_DEV_MODE', true );

// Includes
require_once( 'lib/enqueue/admin-assets.php' );
require_once( 'lib/enqueue/block-editor.php' );
require_once( 'lib/enqueue/customize-preview.php' );
require_once( 'lib/enqueue/theme-assets.php' );

require_once( 'lib/setup.php' );
require_once( 'lib/theme-support.php' );
require_once( 'lib/helpers.php' );
require_once( 'lib/custom-nav-walker.php' );
require_once( 'lib/widgets.php' );
require_once( 'lib/comment-callback.php' );

require_once( 'lib/customizer.php' );
require_once( 'lib/customizer/misc.php' );
require_once( 'lib/customizer/social.php' );
require_once( 'lib/customizer/footer.php' );
require_once( 'lib/customizer/general.php' );
require_once( 'lib/customizer/blog-single.php' );




// Hooks
add_action( 'wp_enqueue_scripts',       '_themename_assets');
add_action( 'admin_enqueue_scripts',    '_themename_admin_assets');
add_action( 'after_setup_theme',        '_themename_register_menus' );
add_action( 'widgets_init',             '_themename_sidebar_widget' );
add_action( 'customize_register',       '_themename_customize_register' );



// Shortcodes


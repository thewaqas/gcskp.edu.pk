<?php get_header() ?>

<?php get_template_part( 'template-parts/breadcrumb' ) ?>

 <!-- Classic layout with sidebar-->
<section class="section section-lg bg-transparent" data-preset='{"title":"Blog Classic","category":"blog","reload":true,"id":"blog-classic-5"}'>
    <div class="container">
        <div class="row row-40 justify-content-lg-between post-area">
            <div class="col-md-8">

                <!--===== Posts Loop =====-->
                <?php get_template_part( 'loop', 'index' ) ?>


                <hr class="divider divider-sm">
                <!--===== Pagination =====-->
                <div class="pag pag-short justify-content-between">
                    <div class="pag-short-link pag-short-link-prev text-left">
                        <?php previous_posts_link( '<span class="int-arrow-left novi-icon"> </span> <span> Prev Posts Page</span>' ); ?>
                    </div>
                    <div class="pag-short-link pag-short-link-next text-right">
                        <?php next_posts_link( '<span>Next Posts Page </span> <span class="int-arrow-right novi-icon"> </span>' ); ?>
                    </div>
                </div>


            </div>

            
            <?php get_sidebar()  ?>


        </div>
    </div>
</section>



<?php get_footer() ?>
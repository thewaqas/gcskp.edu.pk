            <!-- Footer -->
            <!-- <footer class="section footer footer-lg bg-transparent"> -->
            <footer class="section footer footer-lg bg-800 text-400 context-dark" role="contentinfo">
                <div class="container">

                    <?php get_template_part( 'template-parts/footer/subscribe' ) ?>

                    <?php get_template_part( 'template-parts/footer/widgets' ) ?>

                    <?php get_template_part( 'template-parts/footer/info' ) ?>

                </div>
            </footer><!-- #end Footer -->

        </div><!-- #end Page -->

        <?php get_template_part( 'template-parts/page-loader' ) ?>
        

        <?php wp_footer(  ) ?>

    </body>
</html>

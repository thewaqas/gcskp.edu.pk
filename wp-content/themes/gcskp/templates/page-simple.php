<?php 
/*
    Template Name: Page Breadcrumb
*/ 
?>

<?php get_header() ?>

    <?php get_template_part( 'template-parts/breadcrumb' ) ?>

    <main role="main">
        
        <?php while ( have_posts() ) : the_post(); ?>

            <article <?php post_class(); ?>>

                <?php the_content() ?>

            </article>

        <?php endwhile ?>

    </main>

<?php get_footer() ?>
<?php 
/*
    Template Name: Full-width layout
    Template Post Type: post, page, event
*/
?>


<?php get_header() ?>


    <?php if( have_posts() ) : ?>

        <?php while( have_posts() ) : the_post() ?>

            <?php get_template_part( 'template-parts/posts/single/header' ) ?>

            <?php get_template_part( 'template-parts/posts/single/content', 'full' ) ?>

        <?php endwhile ?>

    <?php else : ?>

        <?php get_template_part( 'template-parts/posts/content', 'none' ) ?>

    <?php endif ?>


<?php get_footer() ?>

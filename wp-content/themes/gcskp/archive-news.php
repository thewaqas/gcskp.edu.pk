<?php get_header() ?>

<?php get_template_part( 'template-parts/breadcrumb' ) ?>




<!-- Modern large layout-->
<section class="section section-lg bg-transparent novi-background" data-preset='{"title":"Blog Modern","category":"blog","reload":false,"id":"blog-modern-3"}'>
    <div class="container">
        <!-- <h1 class="text-center">Modern layout</h1> -->
        <div class="row row-40 justify-content-center">
            <div class="col-md-12 post-area">

                <!--===== Posts Loop =====-->
                <?php get_template_part( 'loop', 'news' ) ?>

                
            
                
            </div>
        </div>
    </div>
</section>






<?php get_footer() ?>
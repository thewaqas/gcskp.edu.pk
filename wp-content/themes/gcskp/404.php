<?php get_header() ?>

    <!-- 404 section-->
    <section class="section service-section service-section-xl bg-transparent text-center" data-preset='{"title":"404","category":"service","reload":false,"id":"404"}'>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8 col-lg-6">
                    <h1><?php _e( 'Error 404 :(', 'themename' ) ?></h1>
                    <h4><?php _e( 'Sorry, but page was not found', 'themename' ) ?></h4>
                    <p><?php _e( 'You may have mistyped the address or the page may have moved.', 'themename' ) ?></p>
                    <div class="button-group">
                        <a class="btn btn-lg btn-primary" href="<?php echo home_url( '/' ) ?>">
                            <span class="btn-icon int-arrow-left novi-icon"></span>
                            <span><?php _e( 'Back to home', 'themename' ) ?></span>
                        </a>
                        <a class="btn btn-lg btn-secondary" href="<?php echo home_url( '/contact' ) ?>"><?php _e( 'Contact us', 'themename' ) ?></a>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php get_footer() ?>
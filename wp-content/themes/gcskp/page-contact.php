<?php get_header() ?>

<!-- Get in touch-->
<section class="section section-lg section-layer pt-5" data-preset='{"title":"Get In Touch","category":"form, contacts","reload":true,"id":"get-in-touch-6"}'>
    <div class="bg-layer bg-image novi-background" style="bottom: 45%; background-image: url(<?php echo get_theme_file_uri('dist/assets/images/image-04-1920x772.jpg') ?>);"></div>
    <div class="container">
        <!-- Breadcrumb-->
        <ul class="breadcrumb context-dark">
            <li class="breadcrumb-item">
                <a class="breadcrumb-link" href="index.html">Home</a>
            </li>
            <li class="breadcrumb-item">
                <a class="breadcrumb-link" href="#">Pages</a>
            </li>
            <li class="breadcrumb-item">
                <span class="breadcrumb-text breadcrumb-active">Get in touch</span>
            </li>
        </ul>
        <div class="row row-30 justify-content-center novi-disabled" style="margin-top: 7.5%">
            <div class="col-md-10 col-lg-8">
                <div class="context-dark text-center">
                    <h1>Get in touch</h1>
                    <h5>You can contact us any way that is convenient for you. We are available 24/7 via fax or email. You can also use a quick contact form on the right or visit our office personally.</h5>
                </div>
                <article class="accent-box">
                    <h4 class="accent-box-title">
                        Email us with any questions or inquiries<br>or use our contact data.
                    </h4>
                    <p class="accent-box-text">Email us with any questions or inquiries or use our contact data.</p>
                    <form class="accent-box-form rd-mailform" data-form-type="contact" method="post" action="components/rd-mailform/rd-mailform.php">
                        <div class="form-row row-15 novi-disabled">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="input-name">Name:</label>
                                    <div class="position-relative">
                                        <input class="form-control" id="input-name" type="text" name="name" placeholder="Your name" data-constraints="@Required">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="input-email">E-mail:</label>
                                    <div class="position-relative">
                                        <input class="form-control" id="input-email" type="email" name="email" placeholder="Your e-mail address" data-constraints="@Email @Required">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="input-tel">Phone:</label>
                                    <div class="position-relative">
                                        <input class="form-control" id="input-tel" type="tel" name="tel" placeholder="X-XXX-XXX-XXXX" data-constraints="@PhoneNumber @Required">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="input-subject">Subject:</label>
                                    <div class="position-relative">
                                        <input class="form-control" id="input-subject" type="text" name="subject" placeholder="Subject" data-constraints="@Required">
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="input-question">Question:</label>
                                    <div class="position-relative">
                                        <textarea class="form-control" id="input-question" name="question" placeholder="Your question" data-constraints="@Required"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="offset-xs group-20 d-flex flex-wrap justify-content-between align-items-center">
                            <div>
                                <button class="btn btn-lg btn-primary accent-box-btn" type="submit">Submit</button>
                            </div>
                            <div class="small">
                                By clicking the button you agree to<br>
                                the 
                                <u class="font-weight-medium">
                                    <a class="link-inherit" href="privacy-policy.html">Privacy Policy</a>
                                </u>
                                and 
                                <u class="font-weight-medium">
                                    <a class="link-inherit" href="#">Terms and Conditions</a>
                                </u>
                            </div>
                        </div>
                    </form>
                </article>
            </div>
        </div>
    </div>
</section>

<!-- How to find us-->
<section class="section section-md bg-transparent novi-background" data-preset='{"title":"How To Find Us","category":"content box","reload":false,"id":"how-to-find-us"}'>
    <div class="container">
        <div class="row row-20">
            <div class="col-md-5 col-lg-6">
                <h3>How to find us</h3>
            </div>
            <div class="col-md-7 col-lg-6">
                <p>You can always visit us in one of our comfortable offices. View their addresses and opening hours or call/email us directly using the information below.</p>
            </div>
        </div>
    </div>
</section>
<!-- Section divider-->
<section class="section novi-background" data-preset='{"title":"Divider Section","category":"content box","reload":false,"id":"divider-section"}'>
    <div class="container">
        <hr class="divider divider-sm">
    </div>
</section>
<!-- Contact us-->
<section class="section section-md bg-transparent novi-background" data-preset='{"title":"Contact Us","category":"contacts","reload":false,"id":"contact-us-1"}'>
    <div class="container">
        <div class="row row-30">
            <div class="col-sm-6">
                <h6 class="text-primary">Intense office #1</h6>
                <div class="offset-sm">
                    <div class="d-inline-flex align-items-center">
                        <span class="icon icon-xs int-phone novi-icon mr-2"></span>
                        <div class="h5">
                            <a href="tel:#">+1 234 567 8901</a>
                        </div>
                    </div>
                </div>
                <div class="offset-xs">
                    <div class="h5">7609 Mckinley Ave, Los Angeles, CA 90001, United States</div>
                </div>
                <ul class="list list-xs small">
                    <li class="list-item">Monday-Friday: 8am - 6pm</li>
                    <li class="list-item">Saturday-Sunday: 8am - 2pm</li>
                    <li class="list-item">Holidays: closed</li>
                </ul>
                <ul class="list">
                    <li class="list-item">
                        <a class="link link-secondary" href="mailto:#">mail@demolink.org</a>
                    </li>
                    <li class="list-item">
                        <a class="link link-secondary" href="mailto:#">info@demolink.org</a>
                    </li>
                </ul>
            </div>
            <div class="col-sm-6">
                <h6 class="text-primary">Intense office #2</h6>
                <div class="offset-sm">
                    <div class="d-inline-flex align-items-center">
                        <span class="icon icon-xs int-phone novi-icon mr-2"></span>
                        <div class="h5">
                            <a href="tel:#">+1 234 567 8901</a>
                        </div>
                    </div>
                </div>
                <div class="offset-xs">
                    <div class="h5">178 West 27th Street, Suite 527, New York, NY 10012, United States</div>
                </div>
                <ul class="list list-xs small">
                    <li class="list-item">Monday-Friday: 8am - 6pm</li>
                    <li class="list-item">Saturday-Sunday: 8am - 2pm</li>
                    <li class="list-item">Holidays: closed</li>
                </ul>
                <ul class="list">
                    <li class="list-item">
                        <a class="link link-secondary" href="mailto:#">mail@demolink.org</a>
                    </li>
                    <li class="list-item">
                        <a class="link link-secondary" href="mailto:#">info@demolink.org</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>



<?php get_footer() ?>
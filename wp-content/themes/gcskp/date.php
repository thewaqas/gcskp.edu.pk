<?php get_header() ?>

<!-- ######### Page Title ######### -->
<section class="section section-md context-dark bg-image text-center" style="background-image: url(<?php echo get_theme_file_uri('dist/assets/images/breadcrumb.jpg') ?>)" data-preset='{"title":"Breadcrumb","category":"breadcrumb","reload":false,"id":"breadcrumb-3"}'>
    <div class="container">
        <h2 class="intro-title"> <?php the_archive_title() ?> </h2>
        <h5 class="intro-subtitle"> 
            <?php  
                if ( is_year() ) {
                    echo "You're viewing a year archive.";
                } elseif ( is_month() ) {
                    echo "You're viewing a month archive.";
                } elseif ( is_day() ) {
                    echo "You're viewing a day archive.";
                }
            ?> 
        </h5>
    </div>
</section>
<!-- ######### END Page Title ######### -->

<!-- Classic layout with sidebar-->
<section class="section section-lg bg-transparent" data-preset='{"title":"Blog Classic","category":"blog","reload":true,"id":"blog-classic-5"}'>
    <div class="container">
        <div class="row row-40 justify-content-lg-between post-area">
            <div class="col-md-8">

                <!--===== Posts Loop =====-->
                <?php get_template_part( 'loop', 'index' ) ?>


                <hr class="divider divider-sm">
                <!--===== Pagination =====-->
                <div class="pag pag-short justify-content-between">
                    <dic class="pag-short-link pag-short-link-prev text-left">
                        <?php previous_posts_link( '<span class="int-arrow-left novi-icon"> </span> <span> Prev Posts Page</span>' ); ?>
                    </dic>
                    <dic class="pag-short-link pag-short-link-next text-right">
                        <?php next_posts_link( '<span>Next Posts Page </span> <span class="int-arrow-right novi-icon"> </span>' ); ?>
                    </dic>
                </div>


            </div>

            
            <?php get_sidebar()  ?>


        </div>
    </div>
</section>



<?php get_footer() ?>
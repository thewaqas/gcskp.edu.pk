<?php if( have_posts() ) : ?>

    <?php while( have_posts() ) : the_post() ?>

        <?php get_template_part( 'template-parts/news/content' ) ?>

    <?php endwhile ?>

    <?php get_template_part( 'template-parts/pagination' ) ?>

<?php else : ?>

    <?php get_template_part( 'template-parts/posts/content', 'none' ) ?>

<?php endif ?> 
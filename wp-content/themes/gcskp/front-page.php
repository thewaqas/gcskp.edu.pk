<?php get_header() ?>




<!-- home slider-->
<section class="section bg-gradient-blue-white position-relative overflow-hidden" data-preset='{"title":"Slider","category":"slider","reload":true,"id":"slider"}'>
    <!-- <div class="section-title-custom text-outline">Green Energy </div> -->
    <div class="swiper-container swiper-vertical-nav" data-swiper="{&quot;simulateTouch&quot;:false,&quot;autoplay&quot;:true}">
        
        <div class="swiper-wrapper">
            <div class="swiper-slide bg-image min-vh-100 section-lg" style="background-image: url( https://gcskp.edu.pk/wp-content/themes/gcskp/dist/assets/images/img.jpg )">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-10 col-md-9 col-lg-7 col-xl-6">
                            <h1>Green Energy for Everyone</h1>
                            <p>Making a positive impact through green energy since 2010.</p>
                            <div class="group-20 offset-md d-flex align-items-center" data-lightgallery="{&quot;selector&quot;:&quot;.btn-play&quot;}">
                                <a class="btn btn-lg btn-primary" href="#">Learn More</a>
                                <a class="btn btn-lg btn-play btn-secondary" href="https://youtu.be/d5k9CILJivQ">
                                    <span class="btn-play-icon mdi mdi-play"></span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide bg-image min-vh-100 section-lg" style="background-image: url( https://gcskp.edu.pk/wp-content/themes/gcskp/dist/assets/images/img.jpg )">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-10 col-md-9 col-lg-7 col-xl-6">
                            <h1>Qualified Green Energy Team</h1>
                            <p>We employ the best local experts in renewable energy.</p>
                            <div class="group-20 offset-md d-flex align-items-center" data-lightgallery="{&quot;selector&quot;:&quot;.btn-play&quot;}">
                                <a class="btn btn-lg btn-primary" href="#">Learn More</a>
                                <a class="btn btn-lg btn-play btn-secondary" href="https://www.youtube.com/watch?v=d5k9CILJivQ">
                                    <span class="btn-play-icon mdi mdi-play"></span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide bg-image min-vh-100 section-lg" style="background-image: url( https://gcskp.edu.pk/wp-content/themes/gcskp/dist/assets/images/img.jpg )">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-10 col-md-9 col-lg-7 col-xl-6">
                            <h1>Durable Solar Panels</h1>
                            <p>We manufacture and provide top-notch solar panel systems.</p>
                            <div class="group-20 offset-md d-flex align-items-center" data-lightgallery="{&quot;selector&quot;:&quot;.btn-play&quot;}">
                                <a class="btn btn-lg btn-primary" href="#">Learn More</a>
                                <a class="btn btn-lg btn-play btn-secondary" href="https://youtu.be/d5k9CILJivQ">
                                    <span class="btn-play-icon mdi mdi-play"></span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="swiper-pagination"></div>
    </div>
</section>


 




<!-- Posts Carousel-->
<section class="section section-lg bg-transparent" data-preset='{"title":"Posts Carousel","category":"blog, carousel","reload":true,"id":"posts-carousel"}'>
    <div class="container">
        <h6 data-animate='{"class":"fadeInUp"}'>Blog</h6>
        <h1 data-animate='{"class":"fadeInUp"}'>Read the latest news</h1>
        <div class="owl-carousel owl-style-2" data-owl="{&quot;nav&quot;:true,&quot;autoplaySpeed&quot;:1000,&quot;smartSpeed&quot;:1000}" data-items="1" data-sm-items="2" data-md-items="1">
            <!-- Post side large 2-->
            <article class="post post-side-large-2">
                <div class="row row-30 justify-content-xl-between align-items-lg-center">
                    <div class="col-md-7">
                        <a class="post-img-link" href="post-intro.html">
                            <img class="pended-img" src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs%3D" data-src="https://gcskp.edu.pk/wp-content/themes/gcskp/dist/assets/images/blog-thumbnail.jpg" alt="" width="730" height="469">
                        </a>
                    </div>
                    <div class="col-md-5 col-xl-4">
                        <div class="post-inner">
                            <div class="post-meta">
                                <div class="post-meta-item">
                                    <span class="post-meta-icon int-clock novi-icon"></span>
                                    <a class="post-meta-link" href="post-intro.html">September 16, 2019</a>
                                </div>
                            </div>
                            <h3 class="post-title">
                                <a href="post-intro.html">Top 10 photography tips to improve the look of your photos</a>
                            </h3>
                            <div class="post-text small">Whether you are a beginner or more experienced with photography, here are some of our favorite tips...</div>
                            <div class="post-meta">
                                <div class="post-meta-item">
                                    <a class="btn btn-lg btn-rect btn-dark post-btn" href="post-intro.html">
                                        Learn more<span class="link-icon int-arrow-right novi-icon"></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
            <!-- Post side large 2-->
            <article class="post post-side-large-2">
                <div class="row row-30 justify-content-xl-between align-items-lg-center">
                    <div class="col-md-7">
                        <a class="post-img-link" href="post-intro.html">
                            <img class="pended-img" src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs%3D" data-src="https://gcskp.edu.pk/wp-content/themes/gcskp/dist/assets/images/blog-thumbnail.jpg" alt="" width="730" height="469">
                        </a>
                    </div>
                    <div class="col-md-5 col-xl-4">
                        <div class="post-inner">
                            <div class="post-meta">
                                <div class="post-meta-item">
                                    <span class="post-meta-icon int-clock novi-icon"></span>
                                    <a class="post-meta-link" href="post-intro.html">September 16, 2019</a>
                                </div>
                            </div>
                            <h3 class="post-title">
                                <a href="post-intro.html">How to Take Your Everyday Photography to the Next Level</a>
                            </h3>
                            <div class="post-text small">As with many hobbies, the great thing about photography is you can reignite your passion. So...</div>
                            <div class="post-meta">
                                <div class="post-meta-item">
                                    <a class="btn btn-lg btn-rect btn-dark post-btn" href="post-intro.html">
                                        Learn more<span class="link-icon int-arrow-right novi-icon"></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
            <!-- Post side large 2-->
            <article class="post post-side-large-2">
                <div class="row row-30 justify-content-xl-between align-items-lg-center">
                    <div class="col-md-7">
                        <a class="post-img-link" href="post-intro.html">
                            <img class="pended-img" src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs%3D" data-src="https://gcskp.edu.pk/wp-content/themes/gcskp/dist/assets/images/blog-thumbnail.jpg" alt="" width="730" height="469">
                        </a>
                    </div>
                    <div class="col-md-5 col-xl-4">
                        <div class="post-inner">
                            <div class="post-meta">
                                <div class="post-meta-item">
                                    <span class="post-meta-icon int-clock novi-icon"></span>
                                    <a class="post-meta-link" href="post-intro.html">September 16, 2019</a>
                                </div>
                            </div>
                            <h3 class="post-title">
                                <a href="post-intro.html">12 Common Photography Mistakes You Should Avoid</a>
                            </h3>
                            <div class="post-text small">None of us are perfect and we all make mistakes. While some photographers might be naturally...</div>
                            <div class="post-meta">
                                <div class="post-meta-item">
                                    <a class="btn btn-lg btn-rect btn-dark post-btn" href="post-intro.html">
                                        Learn more<span class="link-icon int-arrow-right novi-icon"></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        </div>
    </div>
</section>


<!-- Features-->
<section class="section bg-transparent" data-preset='{"title":"Offer","category":"carousel","reload":false,"id":"offer"}'>
    <div class="container">
        <div class="row align-items-center slick-creative novi-disabled">
            <div class="col-md-7 col-xl-6 d-md-flex flex-md-row-reverse">
                <div class="slick-slider slider-for slider-images" data-slick='{"arrows":false,"asNavFor":".slider-nav","adaptiveHeight":true}'>
                    <div class="text-right">
                        <img src="https://gcskp.edu.pk/wp-content/themes/gcskp/dist/assets/images/image-09-620x398.jpg" alt="" width="930" height="779">
                    </div>
                    <div class="text-right">
                        <img src="https://gcskp.edu.pk/wp-content/themes/gcskp/dist/assets/images/image-09-620x398.jpg" alt="" width="930" height="779">
                    </div>
                    <div class="text-right">
                        <img src="https://gcskp.edu.pk/wp-content/themes/gcskp/dist/assets/images/image-09-620x398.jpg" alt="" width="930" height="779">
                    </div>
                </div>
            </div>
            <div class="col-md-5 col-xl-6 pt-5" style="padding-bottom: 14.2%">
                <div class="pl-4">
                    <h5 class="text-primary">What We Offer</h5>
                    <div class="slick-slider slider-nav" data-slick='{"arrows":false,"asNavFor":".slider-for","autoplay":true,"autoplaySpeed":3000,"focusOnSelect":true,"variableWidth":true}'>
                        <div class="slick-dot">Solar Panels</div>
                        <div class="slick-dot">Wind Turbines</div>
                        <div class="slick-dot">Hybrid Energy</div>
                    </div>
                </div>
                <div class="slick-slider slider-for" data-slick='{"arrows":false,"asNavFor":".slider-nav"}'>
                    <div class="slick-custom-slide">
                        <p>We only use the most advanced solar technologies made by trusted names like LG. These solar panels are one of the best on the market.</p>
                        <a class="btn btn-lg btn-primary" href="#">Learn More</a>
                    </div>
                    <div class="slick-custom-slide">
                        <p>Our wind turbines are designed and built to provide the most cost-effective long term power even if you are repowering an existing energy site. </p>
                        <a class="btn btn-lg btn-primary" href="#">Learn More</a>
                    </div>
                    <div class="slick-custom-slide">
                        <p>Our team provides cutting-edge hybrid energy solutions for corporate and individual customers throughout the world.</p>
                        <a class="btn btn-lg btn-primary" href="#">Learn More</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

 <!-- Services-->
 <section class="section bg-gradient-blue-white position-relative overflow-hidden" data-preset='{"title":"Services","category":"slider","reload":true,"id":"services"}'>
    <div class="container">
        <h6 data-animate='{"class":"fadeInUp"}'>Best photography solutions</h6>
    </div>
    <!-- Swiper slider-->
    <div class="swiper-container mt-5" style="min-height: 75vh">
        <!-- Additional required wrapper-->
        <div class="swiper-wrapper">
            <!-- Slides-->
            <div class="swiper-slide section-lg px-xl-5 align-items-lg-end context-dark overlay-dark-02" style="background-image: url( https://gcskp.edu.pk/wp-content/themes/gcskp/dist/assets/images/img.jpg )">
                <div class="container">
                    <div class="row row-20 justify-content-between align-items-end">
                        <div class="col-sm-8 col-md-6">
                            <h2>Photos that are a perfect reflection of your lifestyle</h2>
                            <p class="small">
                                Lifestyle photography services capture the precious moments of<br class="d-none d-lg-block">your life that you value instead of capturing staged photos.
                            </p>
                        </div>
                        <div class="col-auto">
                            <a class="link small" href="#">
                                Learn more<span class="link-icon int-arrow-right novi-icon"></span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide section-lg px-xl-5 align-items-lg-end context-dark" style="background-image: url( https://gcskp.edu.pk/wp-content/themes/gcskp/dist/assets/images/img.jpg )">
                <div class="container">
                    <div class="row row-20 justify-content-between align-items-end">
                        <div class="col-sm-8 col-md-6">
                            <h2>Corporate photography for all your business needs</h2>
                            <p class="small">
                                Corporate photography helps businesses of all kinds to build trust<br class="d-none d-lg-block">online and individuals to connect with their audience.
                            </p>
                        </div>
                        <div class="col-auto">
                            <a class="link small" href="#">
                                Learn more<span class="link-icon int-arrow-right novi-icon"></span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide section-lg px-xl-5 align-items-lg-end context-dark overlay-dark" style="background-image: url( https://gcskp.edu.pk/wp-content/themes/gcskp/dist/assets/images/img.jpg )">
                <div class="container">
                    <div class="row row-20 justify-content-between align-items-end">
                        <div class="col-sm-8 col-md-6">
                            <h2>Architectural &commercial photography services</h2>
                            <p class="small">
                                Through architectural and commercial photography services, we<br class="d-none d-lg-block">help express companies’ stories to fuel the brand images.
                            </p>
                        </div>
                        <div class="col-auto">
                            <a class="link small" href="#">
                                Learn more<span class="link-icon int-arrow-right novi-icon"></span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Pagination-->
        <div class="swiper-pagination d-xl-none"></div>
        <!-- Navigation buttons-->
        <div class="swiper-button-prev d-none d-xl-flex"></div>
        <div class="swiper-button-next d-none d-xl-flex"></div>
    </div>
</section>

<!-- Skills and counters-->
<section class="section section-lg bg-200 bg-image-special" data-preset='{"title":"Skills","category":"content","reload":true,"id":"skills"}'>
    <div class="bg-image-special-image bg-image" style="background-image: url( https://gcskp.edu.pk/wp-content/themes/gcskp/dist/assets/images/blog-thumbnail.jpg)"></div>
    <div class="container">
        <div class="row row-50 row-lg-80 row-xl-100 row-xxl-150 novi-disabled">
            <div class="col-12">
                <div class="row row-40 justify-content-lg-between novi-disabled">
                    <div class="col-md-5" data-animate='{"class":"fadeInUp"}'>
                        <h3>Leading experts in renewable energy</h3>
                        <p class="block block-sm">Our team specializes in various kinds of green energy sources for everyday use.</p>
                    </div>
                    <div class="col-md-7 col-lg-6 progress-linear-area">
                        <!-- Linear progress bar-->
                        <div class="progress-linear progress-linear-lg progress-linear-primary">
                            <div class="progress-linear-inner">
                                <div class="progress-linear-title">
                                    Solar energy
                                    <span class="progress-linear-counter-wrap">
                                        — <span class="progress-linear-counter">75</span>
                                        %
                                    </span>
                                </div>
                                <div class="progress-linear-body">
                                    <div class="progress-linear-bar"></div>
                                </div>
                                <div class="progress-linear-text">Solar energy is derived by capturing radiant energy from sunlight.</div>
                            </div>
                        </div>
                        <!-- Linear progress bar-->
                        <div class="progress-linear progress-linear-lg progress-linear-secondary">
                            <div class="progress-linear-inner">
                                <div class="progress-linear-title">
                                    Wind energy
                                    <span class="progress-linear-counter-wrap">
                                        — <span class="progress-linear-counter">99</span>
                                        %
                                    </span>
                                </div>
                                <div class="progress-linear-body">
                                    <div class="progress-linear-bar"></div>
                                </div>
                                <div class="progress-linear-text">Wind energy is a clean energy source, meaning it doesn’t pollute the air.</div>
                            </div>
                        </div>
                        <!-- Linear progress bar-->
                        <div class="progress-linear progress-linear-lg progress-linear-gray-400">
                            <div class="progress-linear-inner">
                                <div class="progress-linear-title">
                                    Hydroelectric energy
                                    <span class="progress-linear-counter-wrap">
                                        — <span class="progress-linear-counter">45</span>
                                        %
                                    </span>
                                </div>
                                <div class="progress-linear-body">
                                    <div class="progress-linear-bar"></div>
                                </div>
                                <div class="progress-linear-text">Hydroelectric power is very versatile and can be generated worldwide.</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="row row-40 justify-content-lg-between novi-disabled">
                    <div class="col-md-5" data-animate='{"class":"fadeInUp"}'>
                        <h3>We've helped businesses increase their revenue on average by 90% in their first year with us!</h3>
                        <p class="block block-sm">Get your source of green energy to become more energy efficient today.</p>
                    </div>
                    <div class="col-md-7 col-lg-6">
                        <div class="row row-40">
                            <div class="col-auto col-xs-6">
                                <!-- Counter-->
                                <div class="counter">
                                    <div class="counter-body">
                                        <div class="counter-value h2">
                                            <span data-counter="">2</span>
                                            <span class="counter-postfix">k+</span>
                                        </div>
                                        <p class="counter-title">Customers Annually</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-auto col-xs-6">
                                <!-- Counter-->
                                <div class="counter">
                                    <div class="counter-body">
                                        <div class="counter-value h2">
                                            <span data-counter="">2478</span>
                                        </div>
                                        <p class="counter-title">Positive Reviews</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-auto col-xs-6">
                                <!-- Counter-->
                                <div class="counter">
                                    <div class="counter-body">
                                        <div class="counter-value h2">
                                            <span class="counter-prefix">+</span>
                                            <span data-counter="">83</span>
                                            <span class="counter-postfix">%</span>
                                        </div>
                                        <p class="counter-title">Usage increase</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- Classic layout 3 columns-->
<section class="section section-lg bg-transparent" data-preset='{"title":"Blog Classic","category":"blog","reload":false,"id":"blog-classic-3"}'>
    <div class="container">
        <h1 class="text-center">Classic layout</h1>
        <div class="row row-40 row-xl-70 row-xxl-100">

            <div class="col-sm-6 col-lg-4">
                <!-- Post classic-->
                <div class="post post-sm">
                    <a class="post-img-link" href="post.html">
                        <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs%3D" data-src="https://gcskp.edu.pk/wp-content/themes/gcskp/dist/assets/images/blog-thumbnail.jpg" alt="" width="400" height="257">
                    </a>
                    <div class="post-meta">
                        <div class="post-meta-item">
                            <span class="post-meta-icon int-user novi-icon"></span>
                            <a class="post-meta-link" href="post.html">Mark Rogers</a>
                        </div>
                        <div class="post-meta-item">
                            <span class="post-meta-icon int-clock novi-icon"></span>
                            <a class="post-meta-link" href="post.html">September 16, 2019</a>
                        </div>
                    </div>
                    <h5 class="post-title">
                        <a href="post.html">Top 10 essential design trends that can revolutionize modern web design</a>
                    </h5>
                </div>
            </div>

            <div class="col-sm-6 col-lg-4">
                <!-- Post classic-->
                <div class="post post-sm">
                    <a class="post-img-link" href="post.html">
                        <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs%3D" data-src="https://gcskp.edu.pk/wp-content/themes/gcskp/dist/assets/images/blog-thumbnail.jpg" alt="" width="400" height="257">
                    </a>
                    <div class="post-meta">
                        <div class="post-meta-item">
                            <span class="post-meta-icon int-user novi-icon"></span>
                            <a class="post-meta-link" href="post.html">Mark Rogers</a>
                        </div>
                        <div class="post-meta-item">
                            <span class="post-meta-icon int-clock novi-icon"></span>
                            <a class="post-meta-link" href="post.html">September 16, 2019</a>
                        </div>
                    </div>
                    <h5 class="post-title">
                        <a href="post.html">Top 10 essential design trends that can revolutionize modern web design</a>
                    </h5>
                </div>
            </div>

            <div class="col-sm-6 col-lg-4">
                <!-- Post classic-->
                <div class="post post-sm">
                    <a class="post-img-link" href="post.html">
                        <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs%3D" data-src="https://gcskp.edu.pk/wp-content/themes/gcskp/dist/assets/images/blog-thumbnail.jpg" alt="" width="400" height="257">
                    </a>
                    <div class="post-meta">
                        <div class="post-meta-item">
                            <span class="post-meta-icon int-user novi-icon"></span>
                            <a class="post-meta-link" href="post.html">Mark Rogers</a>
                        </div>
                        <div class="post-meta-item">
                            <span class="post-meta-icon int-clock novi-icon"></span>
                            <a class="post-meta-link" href="post.html">September 16, 2019</a>
                        </div>
                    </div>
                    <h5 class="post-title">
                        <a href="post.html">Top 10 essential design trends that can revolutionize modern web design</a>
                    </h5>
                </div>
            </div>
          
           
          
        </div>
    </div>
</section>




<?php get_footer() ?>
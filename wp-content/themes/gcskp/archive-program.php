<?php get_header() ?>

<?php get_template_part( 'template-parts/breadcrumb' ) ?>


<!-- Portfolio grid vertical-->
<section class="section section-lg bg-transparent novi-background" data-preset='{"title":"Portfolio Grid Vertical","category":"thumbnail, gallery, isotope","reload":true,"id":"portfolio-grid-vertical"}'>
    <div class="container">
        <div class="isotope-wrap isotope-scale-vertical">
            <div class="isotope-filters">
                <div class="isotope-filter active" data-isotope-filter="*">All</div>
                <div class="isotope-filter" data-isotope-filter=".design">Design</div>
                <div class="isotope-filter" data-isotope-filter=".development">Development</div>
                <div class="isotope-filter" data-isotope-filter=".marketing">Marketing</div>
            </div>
            <div class="isotope row row-30 row-md-40" data-lightgallery>
                <div class="col-6 isotope-item development">
                    <a class="thumbnail-up-shadow lightgallery-item" href="https://gcskp.edu.pk/wp-content/themes/gcskp/dist/assets/images/banner-01.jpg">
                        <img class="lazy-img" src="https://gcskp.edu.pk/wp-content/themes/gcskp/dist/assets/images/banner-01.jpg" data-src="https://gcskp.edu.pk/wp-content/themes/gcskp/dist/assets/images/banner-01.jpg" alt="" width="400" height="400">
                    </a>
                </div>
                <div class="col-6 isotope-item development">
                    <a class="thumbnail-up-shadow lightgallery-item" href="https://gcskp.edu.pk/wp-content/themes/gcskp/dist/assets/images/banner-01.jpg">
                        <img class="lazy-img" src="https://gcskp.edu.pk/wp-content/themes/gcskp/dist/assets/images/banner-01.jpg" data-src="https://gcskp.edu.pk/wp-content/themes/gcskp/dist/assets/images/banner-01.jpg" alt="" width="400" height="400">
                    </a>
                </div>
                <div class="col-6 isotope-item design">
                    <a class="thumbnail-up-shadow lightgallery-item" href="https://gcskp.edu.pk/wp-content/themes/gcskp/dist/assets/images/banner-01.jpg">
                        <img class="lazy-img" src="https://gcskp.edu.pk/wp-content/themes/gcskp/dist/assets/images/banner-01.jpg" data-src="https://gcskp.edu.pk/wp-content/themes/gcskp/dist/assets/images/banner-01.jpg" alt="" width="400" height="400">
                    </a>
                </div>
                <div class="col-6 isotope-item design">
                    <a class="thumbnail-up-shadow lightgallery-item" href="https://gcskp.edu.pk/wp-content/themes/gcskp/dist/assets/images/banner-01.jpg">
                        <img class="lazy-img" src="https://gcskp.edu.pk/wp-content/themes/gcskp/dist/assets/images/banner-01.jpg" data-src="https://gcskp.edu.pk/wp-content/themes/gcskp/dist/assets/images/banner-01.jpg" alt="" width="400" height="400">
                    </a>
                </div>
                <div class="col-6 isotope-item marketing">
                    <a class="thumbnail-up-shadow lightgallery-item" href="https://gcskp.edu.pk/wp-content/themes/gcskp/dist/assets/images/banner-01.jpg">
                        <img class="lazy-img" src="https://gcskp.edu.pk/wp-content/themes/gcskp/dist/assets/images/banner-01.jpg" data-src="https://gcskp.edu.pk/wp-content/themes/gcskp/dist/assets/images/banner-01.jpg" alt="" width="400" height="400">
                    </a>
                </div>
                <div class="col-6 isotope-item marketing">
                    <a class="thumbnail-up-shadow lightgallery-item" href="https://gcskp.edu.pk/wp-content/themes/gcskp/dist/assets/images/banner-01.jpg">
                        <img class="lazy-img" src="https://gcskp.edu.pk/wp-content/themes/gcskp/dist/assets/images/banner-01.jpg" data-src="https://gcskp.edu.pk/wp-content/themes/gcskp/dist/assets/images/banner-01.jpg" alt="" width="400" height="400">
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>




<?php get_footer() ?>
<?php get_header() ?>

<!-- Breadcrumb dark centered-->
<section class="section section-md context-dark bg-image text-center" style="background-image: url(<?php echo get_theme_file_uri('dist/assets/images/breadcrumb.jpg') ?>)" data-preset='{"title":"Breadcrumb","category":"breadcrumb","reload":false,"id":"breadcrumb-3"}'>
    <div class="container">
    
        <ul class="breadcrumb d-inline-flex justify-content-center">
            <li class="breadcrumb-item">
                <a class="breadcrumb-link" href="<?php home_url('/') ?>">Home</a>
            </li>
            <li class="breadcrumb-item">
                <a class="breadcrumb-link" href="<?php home_url('/search') ?>">Search</a>
            </li>
            <li class="breadcrumb-item">
                <span class="breadcrumb-text breadcrumb-active"> <?php the_search_query()  ?> </span>
            </li>
        </ul>
        <h2><?php _e( 'Search Results for: ', '_themename' ); the_search_query() ?></h2>

        <?php get_search_form() ?>

    </div>
</section>


<!-- Search results-->
<section class="section section-sm bg-transparent" data-preset='{"title":"Search Results","category":"service","reload":true,"id":"search-results"}'>
    <div class="container">
        <div class="rd-search-results">

            <ol class="search-list">

                <?php if( have_posts() ) : ?>

                    <?php while( have_posts() ) : the_post() ?>

                        <li class="search-list-item">
                            <h3 class="search-title">
                                <a target="_top" href="<?php the_permalink() ?>" class="search-link"> <?php the_title() ?> </a>
                            </h3>
                            <p>
                            <?php 
                                if( has_excerpt() ) {
                                    echo wp_trim_words( get_the_excerpt(), 100, '...' );
                                } else {
                                    echo wp_trim_words( get_the_content(), 100, '...' );
                                }
                            ?>
                            </p>
                            <br/>
                            <a class="btn btn-sm btn-primary" href="<?php the_permalink() ?>">Read more</a>           
                        </li>

                    <?php endwhile ?>

                    <?php get_template_part( 'template-parts/pagination' ) ?>
                    

                <?php else : ?>

                    <?php get_template_part( 'template-parts/posts/content', 'none' ) ?>

                <?php endif ?> 

                
                
                          
                            
                            
            </ol>

        </div>
    </div>
</section>

<?php get_footer() ?>
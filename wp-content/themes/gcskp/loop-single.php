<?php if( have_posts() ) : ?>

    <?php while( have_posts() ) : the_post() ?>

        <?php get_template_part( 'template-parts/posts/content', 'single' ) ?>

        <?php get_template_part( 'template-parts/single/navigation' ) ?>
        
        <?php get_template_part( 'template-parts/single/author' ) ?>

        <?php get_template_part( 'template-parts/single/related-post' ) ?>

        <?php 
            if ( comments_open() || get_comments_number() ) {
                
                comments_template(); 
            }
        ?>
    
    <?php endwhile ?>

<?php else : ?>

    <?php get_template_part( 'template-parts/posts/content', 'none' ) ?>

<?php endif ?>
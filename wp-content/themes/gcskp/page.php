<?php get_header() ?>

<?php get_template_part( 'template-parts/breadcrumb' ) ?>

<!-- page -->
<section class="section section-lg bg-transparent" data-preset='{"title":"Post Single With Sidebar","category":"blog","reload":true,"id":"post-single-with-sidebar"}'>
    <div class="container">
        <div class="row row-40 justify-content-xxl-between">
            <div class="col-md-12">

                <!-- <h1 class="text-center">page.php</h1> -->

                <?php get_template_part( 'loop', 'page' ) ?>

            </div>    
        </div>
    </div>
</section>

<?php get_footer() ?>
import $ from 'jquery';

import strip_tags from './helpers/strip-tags';


wp.customize( 'blogname', (value) => {
    value.bind( (to) => {
        $('.navbar-logo-link').html(strip_tags(to));
    } )
})



wp.customize( '_themename_header_show_search', (value) => {
    value.bind( (to) => {
        if (to) {
            $('#header_search').show();    
        }
        else {
            $('#header_search').hide();    
        }
    } )
})


wp.customize( '_themename_header_show_user', (value) => {
    value.bind( (to) => {
        if (to) {
            $('#header_user').show();    
        }
        else {
            $('#header_user').hide();    
        }
    } )
})











wp.customize( '_themename_primary_color', (value) => {
    value.bind( (to) => {

        let inline_css = ``;
        let inline_css_obj = _themename['inline-css'];

        for(let selector in inline_css_obj) {

            inline_css += `${selector} {`;

                for( let prop in inline_css_obj[selector] ) {

                    let val = inline_css_obj[selector][prop];
                    inline_css += ` ${prop}: ${wp.customize(val).get() }`;
                }

            inline_css += `}`;
        }
        $('#_themename-stylesheet-inline-css').html( inline_css );
    } )
})


wp.customize( '_themename_site_info', (value) => {
    value.bind( (to) => {
        $('.c-site-info__text').html(strip_tags(to, '<a>'));
    } )
})

wp.customize( '_themename_display_author_info', (value) => {
    value.bind( (to) => {
        if (to) {
            $('.c-post-author').show();    
        }
        else {
            $('.c-post-author').hide();    
        }
        
    } )
})
const staticCache = 'static-cache';
const dynamicCache = 'dynamic-cache';

const assets = [
    '/',
	'/wp-content/themes/gcskp/components/base/base.css',
	'/wp-content/themes/gcskp/components/base/core.js',
	'/wp-content/themes/gcskp/components/base/script.js',

]

const limitNumCache = (cacheName, num) => {
    caches.open(cacheName).then(cache => {
        cache.keys().then(keys=>{
            if (keys.length > num) {
                cache.delete(keys[0]).then(limitNumCache(cacheName, num))
            }
        })
    })
}

//install process
self.addEventListener('install', e => {
    e.waitUntil(
        caches.open(staticCache).then(cache => {
            cache.addAll(assets)
        })
    )
})

//activate.
self.addEventListener('activate', e => {
    console.log('avtivate')
})

self.addEventListener('fetch', e => {
    if (e.request.url.indexOf('gcskp.edu.pk') === -1) {
        e.respondWith(
            caches.match(e.request).then(staticRes => {
                return staticRes || fetch(e.request).then(dynamicRes => {
                    return caches.open(dynamicCache).then(cache => {
                        cache.put(e.request.url, dynamicRes.clone())
                        limitNumCache(dynamicCache, 100)
                        return dynamicRes
                    })
                })
            }).catch( ()=>caches.match('/404'))
        )
    }
})

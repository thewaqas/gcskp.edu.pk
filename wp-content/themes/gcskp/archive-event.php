<?php get_header() ?>

<?php get_template_part( 'template-parts/breadcrumb' ) ?>

 <!-- Dated layout-->
 <section class="section section-lg bg-transparent novi-background" data-preset='{"title":"Blog Numbered","category":"blog","reload":false,"id":"blog-numbered"}'>
    <div class="container">
        <!-- <h1 class="text-center">Modern layout</h1> -->
        <div class="row row-40 row-md-60 justify-content-between">
            
            <?php get_sidebar( 'event' )  ?>
            
            <div class="col-sm-12 col-md-8 col-xxl-9">
                <?php get_template_part( 'loop', 'event' ) ?>
            </div>

        </div>
    </div>
</section>


<?php get_footer() ?>
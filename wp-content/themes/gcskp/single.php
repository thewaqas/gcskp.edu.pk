<?php get_header() ?>


    <?php if( have_posts() ) : ?>

        <?php while( have_posts() ) : the_post() ?>

            <h1 class="text-center">single.php</h1>

            <div id="post-<?php the_ID() ?>" <?php post_class() ?> >

                <?php get_template_part( 'template-parts/posts/single/header' ) ?>

                <?php get_template_part( 'template-parts/posts/single/content' ) ?>

            </div>

        <?php endwhile ?>

    <?php else : ?>

        <?php get_template_part( 'template-parts/posts/content', 'none' ) ?>

    <?php endif ?>


<?php get_footer() ?>
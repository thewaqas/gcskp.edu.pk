<?php

    if ( post_password_required() ) {
        return;
    }

?>

<?php if ( have_comments() ) : ?>

    <hr class="divider divider-sm post-single-divider">

    <h6 class="blog-article-subtitle">
        <?php 
            /* translators: 1 is number of comments and 2 is post title */  
            printf( 
                esc_html( _n( 
                        '%1$s Reply to "%2$s"',
                        '%1$s Replies to "%2$s"',
                        get_comments_number(),
                        '_themename'
                    ) ),
                number_format_i18n( get_comments_number() ),
                get_the_title()
            )
        ?>
    </h6>
    
    
    <ul class="section section-sm">
        <?php
            /*wp_list_comments( 
                array( 
                    'style' => 'div',
                    'short_ping' => false,
                    'avatar_size' => 100,
                    'reply_text' => 'Reply',
                    // 'callback' => '_themename_comment_callback'
                ) 
            )*/

            foreach ($comments as $comment ) {
            ?>
            <!-- Comments bordered-->
            <li <?php comment_class( [ 'comment comment-cloud' ] ) ?>>
                
                <!-- <div class="comment comment-bordered"> -->
                    <div class="media media-md flex-column flex-sm-row">
                        <div class="media-left">
                            <div class="mt-1 d-none d-sm-inline-block">
                            <?php  echo get_avatar( $comment, 60, '', '', [ 'class'=> 'comment-image' ] ); ?>
                            <!-- <img class="lazy-img comment-image" src="<?php //echo $avatar ?>" data-src="<?php //echo $avatar ?>" alt="" width="60" height="60"> -->
                            </div>
                        </div>
                        <div class="media-body">
                            <div class="comment-panel">
                                <div>
                                    <div class="media media-xs align-items-center">
                                        <div class="media-left d-sm-none">
                                            <!-- <div class="comment-icon int-user novi-icon"></div> -->
                                            <?php  echo get_avatar( $comment, 60, '', '', [ 'class'=> 'comment-image' ] ); ?>
                                        </div>
                                        <div class="media-body">
                                            <ul class="comment-meta">
                                                <li class="comment-meta-item">
                                                    By <span class="font-weight-medium"> <?php echo get_comment_author_link( $comment ) ?> </span>
                                                </li>
                                                <li class="comment-meta-item">
                                                    Posted 
                                                    <time class="font-weight-medium"  datetime="<?php comment_time( 'c' )?>">
                                                        <?php 
                                                            printf ( 
                                                                esc_html__( '%s ago', '_themename' ), 
                                                                human_time_diff( get_comment_time( 'U' ), 
                                                                current_time( 'U' ) ) 
                                                            );  
                                                        ?>
                                                    </time>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!-- <a class="comment-link" href="#">Reply</a> -->
                            </div>
                            <div class="comment-text"><?php comment_text()  ?></div>
                        </div>
                    </div>
                <!-- </div> -->
                
            </li>
        
                
            <?php
            }
        ?>
    </ul>               

    <?php the_comments_pagination() ?>

<?php endif ?>

<?php if ( !comments_open() && get_comments_number() ) : ?>

    <p class="c-comments__closed">
        <?php esc_html_e( 'Comments are close for this post', '_themename' )  ?>
    </p>

<?php endif ?>


<div id="responce" class="clearfix">

    <?php comment_form( [
        'comment_field' => '<div class="form-group">
                <label for="input-comment">Comment *</label>
                <div class="position-relative">
                    <textarea class="form-control" id="input-comment" name="comment" placeholder="Say something nice..." data-constraints="@Required"></textarea>
                </div>
            </div>',
        'fields' => [
            'author'  => 
                '<div class="form-group">
                    <label for="input-name">'.__( 'Name:', '_themename' ).'</label>
                    <div class="position-relative">
                        <input class="form-control" id="input-name" type="text" name="author" placeholder="Full Name" data-constraints="@Required">
                    </div>
                </div>',
            'email'  => 
                '<div class="form-group">
                    <label for="input-email">'.__( 'E-mail:', '_themename' ).'</label>
                    <div class="position-relative">
                        <input class="form-control" id="input-email" type="email" name="email" placeholder="Your E-mail address" data-constraints="@Email @Required">
                    </div>
                </div>',
            'url'  => 
                '<div class="form-group">
                    <label for="input-name">'.__( 'Website:', '_themename' ).'</label>
                    <div class="position-relative">
                        <input class="form-control" id="input-url" type="text" name="url" placeholder="Website" data-constraints="@Required">
                    </div>
                </div>',
        ],
        'class_submit' => 'btn btn-lg btn-secondary',
        'lable_submit' => __( 'Submit Comment', '_themename' ),
        'title_reply' => __( '<h4 class="blog-article-subtitle">Leave a <span><b> Comment </b><span></h4>', '_themename' ),
    ] ) ?>
    
</div>

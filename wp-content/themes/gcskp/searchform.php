<?php $unique_id = esc_attr( uniqid( 'search-form-' ) ) ?>

<form class="widget-search-form search-form" role="search" method="GET" action="<?php echo esc_url( home_url() )?>">
    <div class="input-group">
        <input class="form-control" type="search" autocomplete="off" name="s"
                placeholder="<?php esc_attr_e( 'Search...', 'label', '_themename' )?>"
                value="<?php get_search_query() ?>">
        <div class="input-group-append">
            <button type="submit" class="widget-search-btn input-group-text input-group-text-nodivider int-search novi-icon"></button>
        </div>
    </div>
</form>
<div class="widget-search-results"></div> 
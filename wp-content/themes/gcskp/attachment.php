<?php get_header() ?>

    <!-- Breadcrumb dark centered-->
    <section class="section section-md context-dark bg-image text-center" style="background-image: url(<?php echo get_theme_file_uri('dist/assets/images/breadcrumb.jpg') ?>)" data-preset='{"title":"Breadcrumb","category":"breadcrumb","reload":false,"id":"breadcrumb-3"}'>
        <div class="container">
            <h2 class="intro-title"> <?php the_title() ?> </h2>
        </div>
    </section>

    <?php if( have_posts() ) : ?>

        <?php while( have_posts() ) : the_post() ?>

               <!-- Anchor buttons-->
               <section class="section section-lg bg-transparent" data-preset='{"title":"Button Anchor Sample","category":"sample","reload":false,"id":"button-anchor-sample"}'>
                <div class="container text-center">
                    <div class="mt-5">
                        <div class="d-inline-flex justify-content-center align-items-center flex-wrap group-20">
                            <a class="btn btn-outline btn-primary btn-lg btn-block" href="<?php echo $post->guid ?>">Direct Download</a>
                        </div>
                    </div>
                    <?php the_content() ?>
                </div>
            </section>
        

        <?php endwhile ?>

    <?php else : ?>

        <?php get_template_part( 'template-parts/posts/content', 'none' ) ?>

    <?php endif ?>


<?php get_footer() ?>
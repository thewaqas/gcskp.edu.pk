<article <?php post_class() ?>>
    
    <div class="c-page__content">
        <?php the_content() ?>
    </div>
    
</article>
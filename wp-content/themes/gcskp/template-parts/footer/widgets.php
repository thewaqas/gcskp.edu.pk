<hr class="divider footer-divider">

<div class="row row-40 row-md-50 justify-content-md-between justify-content-xl-end">
    
    <div class="col-lg-4 col-xl-3">
        <!-- Logo-->
        <div class="logo">
            <a class="logo-link" href="index.html">
                <img class="lazy-img logo-image-default" src="https://gcskp.edu.pk/wp-content/uploads/logo-gc-1.png'" data-src="https://gcskp.edu.pk/wp-content/uploads/logo-gc-1.png'" alt="Govt. College Sheikhupura" width="114" height="27">
                <img class="lazy-img logo-image-inverse" src="https://gcskp.edu.pk/wp-content/uploads/logo-gc-1.png'" data-src="https://gcskp.edu.pk/wp-content/uploads/logo-gc-1.png'" alt="Govt. College Sheikhupura" width="114" height="27">
            </a>
        </div>
        <p class="small">Govermemt Post Graduate College Sheikhupura</p>

    </div>
    <div class="col-auto col-xs-4 col-lg-auto col-xl-3">
        <h6>Features</h6>
        <ul class="list list-xl small font-weight-normal">
            <li class="list-item">
                <a class="list-link" href="#">Messaging</a>
            </li>
            <li class="list-item">
                <a class="list-link" href="#">Spam filtering</a>
            </li>
            <li class="list-item">
                <a class="list-link" href="#">Tweeting</a>
            </li>
            <li class="list-item">
                <a class="list-link" href="#">Picture editing</a>
            </li>
            <li class="list-item">
                <a class="list-link" href="#">Account security</a>
            </li>
        </ul>
    </div>
    <div class="col-auto col-xs-4 col-lg-auto col-xl-3">
        <h6>Navigation</h6>
        <ul class="list list-xl small font-weight-normal">
            <li class="list-item">
                <a class="list-link" href="#">About us</a>
            </li>
            <li class="list-item">
                <a class="list-link" href="#">Our team</a>
            </li>
            <li class="list-item">
                <a class="list-link" href="#">Our history</a>
            </li>
            <li class="list-item">
                <a class="list-link" href="#">Portfolio</a>
            </li>
            <li class="list-item">
                <a class="list-link" href="#">Careers</a>
            </li>
        </ul>
    </div>
    <div class="col-auto col-xs-4 col-lg-auto col-xl-3">
        <h6>Quick links</h6>
        <ul class="list list-xl small font-weight-normal">
            <li class="list-item">
                <a class="list-link" href="#">Reviews</a>
            </li>
            <li class="list-item">
                <a class="list-link" href="#">Troubleshooting</a>
            </li>
            <li class="list-item">
                <a class="list-link" href="#">App installation</a>
            </li>
            <li class="list-item">
                <a class="list-link" href="#">Configuration</a>
            </li>
            <li class="list-item">
                <a class="list-link" href="#">App changelog</a>
            </li>
        </ul>
    </div>

</div>
<hr class="divider footer-divider">
<div class="row row-15 align-items-center justify-content-between footer-panel">
    <div class="col-auto">
        <!-- Copyright-->
        <p class="rights">

            <?php echo get_theme_mod( '_themename_footer_copyright_text' ) ?>

            <?php if( get_theme_mod( '_themename_footer_tos_page' ) ) : ?>    
                <a class="rights-link" href="<?php the_permalink( get_theme_mod( '_themename_footer_tos_page' ) ) ?>">- Terms of Services </a>
            <?php endif ?>

            <?php if( get_theme_mod( '_themename_footer_privacy_page' ) ) : ?>    
                <a class="rights-link" href="<?php the_permalink( get_theme_mod( '_themename_footer_privacy_page' ) ) ?>">- Privacy Policy</a>
            <?php endif ?>

            <?php if( get_theme_mod( '_themename_report_file' ) ) : ?>    
                <a class="rights-link" href="<?php echo get_theme_mod( '_themename_report_file' )?>">- Download Report</a>
            <?php endif ?>
            <?php if( get_theme_mod( '_themename_to_top_color' ) ) : ?>    
                <a class="rights-link" href="<?php the_permalink( get_theme_mod( '_themename_to_top_color' ) ) ?>">- Color Value = <?php echo get_theme_mod( '_themename_to_top_color' )?></a>
            <?php endif ?>

        </p>
    </div>
    <div class="col-auto">
        <div class="group-30 d-flex text-white">
            
            <?php if ( get_theme_mod( '_themename_facebook_handle' ) ) : ?>
                <a class="navbar-info-icon icon-social int-facebook novi-icon" href="https://facebook.com/<?php echo get_theme_mod( '_themename_facebook_handle' ) ?>" target="_blank"></a>
            <?php endif ?>

            <?php if ( get_theme_mod( '_themename_twitter_handle' ) ) : ?>
                <a class="navbar-info-icon icon-social int-twitter novi-icon" href="https://twitter.com/<?php echo get_theme_mod( '_themename_twitter_handle' ) ?>" target="_blank"></a>
            <?php endif ?>

            <?php if ( get_theme_mod( '_themename_instagram_handle' ) ) : ?>
                <a class="navbar-info-icon icon-social int-instagram novi-icon" href="https://instagram.com/<?php echo get_theme_mod( '_themename_instagram_handle' ) ?>" target="_blank"></a>
            <?php endif ?>

            <?php if ( get_theme_mod( '_themename_linkedin_handle' ) ) : ?>
                <a class="navbar-info-icon icon-social int-linkedin novi-icon" href="https://linkedin.com/in/<?php echo get_theme_mod( '_themename_linkedin_handle' ) ?>" target="_blank"></a>
            <?php endif ?>

            <?php if ( get_theme_mod( '_themename_youtube_handle' ) ) : ?>
                <a class="navbar-info-icon icon-social int-youtube novi-icon" href="https://youtube.com/<?php echo get_theme_mod( '_themename_youtube_handle' ) ?>" target="_blank"></a>
            <?php endif ?>
        
        </div>
    </div>
</div>
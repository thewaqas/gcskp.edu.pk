


<div class="row row-30 justify-content-xl-between">
    <div class="col-md-10 col-lg-6">
        <h4>Subscribe to our newsletter</h4>
        <p> Trying to stay on top of it all? Get the best tools, resources and inspiration sent to your inbox every Wednesday.</p>
    </div>
    <div class="col-md-10 col-lg-6 col-xl-5">
        <form class="rd-mailform form-inline group-20 mt-lg-3" data-form-output="footer-consulting-form-output" data-form-type="subscribe" method="post" action="components/rd-mailform/rd-mailform.php" novalidate="novalidate">
        <div class="form-inline-group has-error">
        <input class="form-control form-control-has-validation" type="email" name="email" placeholder="Enter your email address" data-constraints="@Email @Required" id="">
        </div>
        <button class="btn btn-primary" type="submit">Sign in</button>
        </form>
        <div class="form-output snackbar snackbar-secondary" id="footer-consulting-form-output"></div>
    </div>
</div>
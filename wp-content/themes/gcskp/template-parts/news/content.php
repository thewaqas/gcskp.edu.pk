<!-- Post side large-->
<div class="post post-side post-side-large <?php echo ( $wp_query->current_post%2 == 1 ) ? '' : 'post-side-reverse' ?>">

    <a class="post-img-link" href="<?php the_permalink() ?>">
        <?php if( has_post_thumbnail() ) : ?>
            <img class="lazy-img" src="<?php the_post_thumbnail_url( 'large' ) ?>" data-src="<?php the_post_thumbnail_url( 'large' ) ?>" alt="<?php the_title() ?>" width="620" height="398">
        <?php else : ?>
            <img class="lazy-img" src="https://gcskp.edu.pk/wp-content/themes/gcskp/dist/assets/images/news-thumbnail.jpg" data-src="https://gcskp.edu.pk/wp-content/themes/gcskp/dist/assets/images/news-thumbnail.jpg" alt="<?php the_title() ?>" width="620" height="398">
        <?php endif ?>
    </a>

    <div class="post-inner">
        <h3 class="post-title">
            <a href="<?php the_permalink() ?>"><?php the_title() ?></a>
        </h3>
        <div class="post-meta">

            <?php if( has_category() ) : ?> 
                <div class="post-meta-item">
                    <div class="category"> <?php the_category( ' ' ); ?> </div>
                </div>
            <?php endif ?>

            <div class="post-meta-item">
                <span class="post-meta-icon int-user novi-icon"></span>
                <a class="post-meta-link" href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ) ?>"> <?php the_author() ?> </a>
            </div>
            <div class="post-meta-item">
                <span class="post-meta-icon int-clock novi-icon"></span>
                <a class="post-meta-link" href="<?php the_permalink() ?>"><?php echo esc_html( get_the_date('F j, Y' ) )  ?></a>
            </div>
        </div>
        <div class="post-text">
        <?php 
            if( has_excerpt() ){
                echo wp_trim_words( get_the_excerpt(), 25, '...' );
            }else{
                echo wp_trim_words( get_the_content(), 25, '...' );
            }
        ?>
    
        </div>
        <div class="post-meta">
            <div class="post-meta-item">
                <a class="btn btn-primary post-btn" href="<?php the_permalink() ?>">
                    Read more<span class="link-icon int-arrow-right novi-icon"></span>
                </a>
            </div>
            <div class="post-meta-item">
                <span class="post-meta-icon int-chat novi-icon"></span>
                <a class="post-meta-link" href="<?php the_permalink() ?>"><?php echo get_comments_number() ?></a>
            </div>
        </div>
    </div>
</div>


<!-- Post single with sidebar-->
<section class="section section-lg bg-transparent" data-preset='{"title":"Post Single With Sidebar","category":"blog","reload":true,"id":"post-single-with-sidebar"}'>
    <div class="container">
        <div class="row row-40 justify-content-xxl-between">


            <div class="col-md-12">
                <div class="blog-article clearfix">

                    <?php if( has_post_thumbnail() ) : ?>
                    <a class="post-img-link" href="<?php the_permalink() ?>">
                        <?php 
                        /*
                        the_post_thumbnail( 'large', [ 
                            'class' => 'lazy-img' 
                        ] ); 
                        */
                        ?>       
                        <img class="lazy-img" src="<?php the_post_thumbnail_url( 'large' ) ?>" data-src="<?php the_post_thumbnail_url( 'large' ) ?>" alt="" width="840" height="540">
                    </a>
                    <?php endif ?> 

                    <div class="post-text">
                        <?php 
                            the_content();
                            wp_link_pages();
                        ?> 
                    </div>

                    <div class="tag-container">
                        <?php the_tags( '<div class="single-post-tag">', ' ', '</div>' )?>
                    </div>

                    
                    <?php get_template_part( 'template-parts/posts/single/parts/share' ) ?>

                    <?php get_template_part( 'template-parts/posts/single/parts/navigation' ) ?>
            
                    <?php get_template_part( 'template-parts/posts/single/parts/author' ) ?>

                    <?php get_template_part( 'template-parts/posts/single/parts/related-post' ) ?>

                    <?php if ( comments_open() || get_comments_number() )  comments_template() ?>

                </div>
            </div>
            
        </div>
    </div>
</section>


 
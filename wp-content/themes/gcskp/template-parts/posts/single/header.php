 <!-- Intro project-->
 <section class="section intro intro-project bg-image context-dark" style="background-image: url( <?php echo get_theme_file_uri('dist/assets/images/breadcrumb.jpg') ?>)" data-preset='{"title":"Intro Project","category":"intro","reload":false,"id":"intro-project"}'>
    <div class="container">
    
        <div class="text-center">
            <!-- Breadcrumb-->
            <ul class="breadcrumb d-inline-flex justify-content-center">
                <li class="breadcrumb-item">
                    <a class="breadcrumb-link" href="<?php home_url() ?>">Home</a>
                </li>
                <li class="breadcrumb-item">
                    <a class="breadcrumb-link" href="<?php site_url( './blog' ) ?>">Blog</a>
                </li>
                <li class="breadcrumb-item">
                    <span class="breadcrumb-text breadcrumb-active">Blog Post</span>
                </li>
            </ul>
        </div>

        <div class="post-meta post-meta-between">

            <div class="post-meta-item">
                <div class="category-single">
                    <?php the_category( ' ' ) ?>
                </div>
            </div>
            
            <div class="post-meta-item">
                <span class="post-icon int-clock novi-icon text-white"></span>
                <span class="post-meta-text"><?php echo esc_html( get_the_date('F j, Y' ) )  ?></span>
            </div>
        </div>

        <!-- <h4 class="intro-subtitle">Blog Post</h4> -->
        <h1 class="intro-title"> <?php the_title() ?> </h1>

        <!-- Person side-->
        <div class="person person-side person-side-sm">
            <div class="person-side-left">
                <img class="lazy-img person-img" src="<?php echo get_avatar_url( get_the_author_meta( 'ID' ), 96 )?>" data-src="<?php echo get_avatar_url( get_the_author_meta( 'ID' ), 128 )?>" alt="" width="100" height="100">
            </div>
            <div class="person-side-body">
                <div class="person-title">
                    <a class="post-meta-link" href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ) ?>"> <?php the_author() ?> </a>
                </div>
                <div class="person-subtitle toUppercase">
                    <?php echo get_user_role(get_the_author_meta('ID')) ?>
                </div>
            </div>
        </div>

    </div>
</section>
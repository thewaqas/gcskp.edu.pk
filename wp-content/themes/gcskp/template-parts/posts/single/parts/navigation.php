<?php 

    $prev = get_previous_post();
    $next = get_next_post();

?>

<?php if(  $prev || $next ) : ?>

    <div class="pag pag-extended justify-content-between">

        <h2 class="u-screen-reader-text">
            <?php esc_attr_e( 'Post Navigation', '_themename' ) ?>
        </h2>


        <?php if( $prev ) : ?>
            <a class="pag-extended-item" href="<?php the_permalink( $prev->ID ) ?>">
                <h5 class="pag-extended-title"><?php echo esc_html( get_the_title( $prev->ID ) )?></h5>
                <div class="pag-extended-link pag-extended-link-prev">
                    <span class="pag-extended-arrow int-arrow-left novi-icon"></span>
                    <span><?php echo esc_html__( 'Prev post', '_themename' ) ?></span>
                </div>
            </a>

        <?php endif ?>
            
        <?php if( $next ) : ?>
            <a class="pag-extended-item" href="<?php the_permalink( $next->ID ) ?>">
                <h5 class="pag-extended-title"><?php echo esc_html( get_the_title( $next->ID ) )?></h5>
                <div class="pag-extended-link pag-extended-link-next">
                    <span><?php echo esc_html__( 'Next post', '_themename' ) ?></span>
                    <span class="pag-extended-arrow int-arrow-right novi-icon"></span>
                </div>
            </a>
        <?php endif ?>

    </div>

<?php endif ?>
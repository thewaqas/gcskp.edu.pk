 <!-- Post hover 2 column-->
 <section class="section section-md bg-transparent" data-preset='{"title":"Post Hover 2 Column","category":"blog","reload":true,"id":"post-hover-2-column"}'>
    <div class="container">
        <hr class="divider divider-sm post-single-divider">
        <h4 class="blog-article-subtitle">Related posts</h4>
        <div class="row row-40 justify-content-center">
            

    <?php 
    
    $categories = get_the_category();

    $relatedPosts = new WP_Query([
        'posts_per_page' => 2,
        'post__not_in'  => [ $post->ID ],
        'cat'            => !empty($categories) ? $categories[0]->term_id : ''
        
    ]);
    
    if ( $relatedPosts->have_posts() ) {
        
        while ( $relatedPosts->have_posts() ) {
            
            $relatedPosts->the_post();

            ?>

            <div class="col-xs-10 col-md-6">
                <!-- Post hover-->
                <div class="post post-hover post-sm">
                    <div class="post-img-link">
                        
                        <a href="<?php the_permalink() ?>">
                            <?php if( has_post_thumbnail() && false ) : ?>
                                <img style="width:100%;" class="lazy-img" src="<?php the_post_thumbnail_url( 'large' ) ?>" data-src="<?php the_post_thumbnail_url( 'large' ) ?>" alt="" width="620" height="398">
                            <?php else : ?>
                                <img class="lazy-img" src="https://gcskp.edu.pk/wp-content/themes/gcskp/dist/assets/images/blog-thumbnail.jpg" data-src="https://gcskp.edu.pk/wp-content/themes/gcskp/dist/assets/images/blog-thumbnail.jpg" alt="" width="620" height="398">
                            <?php endif ?>
                        </a>

                        <a class="post-meta-link post-meta-linkbox post-meta-linkbox-primary" href="<?php the_permalink() ?>">HTML</a>
                        <?php if( has_category() ) : ?> 
                            <!-- <div class="post-meta-item">
                                <div class="category"> <?php //the_category( ' ' ); ?> </div>
                            </div> -->
                        <?php endif ?>

                    </div>
                    <div class="post-inner">
                        <h5 class="post-title">
                            <a href="<?php the_permalink() ?>"><?php the_title( ) ?></a>
                        </h5>
                        <div class="post-meta">
                            <div class="post-meta-item">
                                <span class="post-meta-icon int-user novi-icon"></span>
                                <a class="post-meta-link" href="<?php the_permalink() ?>">Mark Rogers</a>
                            </div>
                            <div class="post-meta-item">
                                <span class="post-meta-icon int-clock novi-icon"></span>
                                <a class="post-meta-link" href="<?php the_permalink() ?>">September 16, 2019</a>
                            </div>
                        </div>
                        <div class="post-text">
                        <?php 
                            if(has_excerpt()){
                                mb_strimwidth(get_the_excerpt(), 0, 500, '...');
                            }else{
                                mb_strimwidth(get_the_content(), 0, 500, '...');
                            }
                        ?>
                        </div>
                        <div class="post-meta post-meta-between">
                            <div class="post-meta-item">
                                <a class="btn btn-sm btn-secondary" href="<?php the_permalink() ?>">Read more</a>
                            </div>
                            <div class="post-meta-item">
                                <a class="post-meta-link post-meta-linkbox" href="<?php the_permalink() ?>">
                                    <span class="post-meta-icon int-chat novi-icon"></span>
                                    <span><?php echo get_comments_number() ?></span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            

            <?php
        }
        wp_reset_postdata();
    }
    
    ?>

        </div>
    </div>
</section>

<hr class="divider divider-sm post-single-divider">
<!-- Person side-->
<div class="person person-side person-side-lg align-items-start">
    <h2 class="u-screen-reader-text">
        <?php esc_attr_e( 'About The Author',   '_themename' ) ?>
    </h2>
    <?php 
        $author_id          = get_the_author_meta( 'ID' );
        $author_posts       = get_the_author_posts();
        $author_display     = get_the_author();
        $author_posts_url   = get_author_posts_url( $author_id );
        $author_description = get_the_author_meta( 'user_description' );
        $author_website     = get_the_author_meta( 'user_url' );
        $author_role        = get_the_author_meta( 'status', $author_id )
    ?>

    <div class="person-side-left">
        <?php //echo get_avatar( $author_id, 128, [ 'class' => 'lazy-img person-img' ]); ?>
        <img class="lazy-img person-img" src="<?php echo get_avatar_url( get_the_author_meta( 'ID' ), 175 )?>" data-src="<?php echo get_avatar_url( get_the_author_meta( 'ID' ), 175 )?>" alt="" width="176" height="176">
    </div>
    <div class="person-side-body">
        <div class="person-title">
            <?php if( $author_website ) : ?>
                <a href="<?php echo esc_html( $author_website ) ?>" target="_blank">
            <?php endif ?>

            <?php echo esc_html( $author_display ) ?>
            
            <?php if( $author_website ) : ?> </a> <?php endif ?>
            <!-- <a href="team-member.html">Mark Rogers</a> -->
        </div>
        <div class="person-subtitle toUppercase">
            <?php echo get_user_role(get_the_author_meta('ID')) ?>

            <a class="tag tag-light" href="<?php echo esc_html( $author_posts_url ) ?>">
                <?php 
                    /* translators: 5s is the number of posts */
                    printf( esc_html( _n( '%s post', '%s posts', $author_posts, '_themename' )), number_format_i18n( $author_posts ) );
                ?>
            </a>
        </div>
        <div class="person-text"> <?php echo esc_html( nl2br( $author_description ) ) ?> </div>
        <div class="person-social">
            <a class="person-icon novi-icon int-youtube" href="#"></a>
            <a class="person-icon novi-icon int-facebook" href="#"></a>
            <a class="person-icon novi-icon int-instagram" href="#"></a>
            <a class="person-icon novi-icon int-twitter" href="#"></a>
            <a class="person-icon novi-icon int-linkedin" href="#"></a>
        </div>
    </div>
</div>
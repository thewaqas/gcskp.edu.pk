

    <h3 class="post-title">
        <a href="<?php the_permalink() ?>" title="<?php the_title_attribute() ?>"> <?php the_title() ?> </a>
    </h3>

<div class="post-meta">

    <?php if( has_category() ) : ?> 

        <div class="post-meta-item">
            <div class="category">
                <?php 
                    the_category( ' ' ); 
                ?>
            </div>
        </div>

    <?php endif ?>

    <div class="post-meta-item">
        <span class="post-meta-icon int-user novi-icon"></span>
        <a class="post-meta-link" href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ) ?>"> <?php the_author() ?> </a>
    </div>
    <div class="post-meta-item">
        <span class="post-meta-icon int-clock novi-icon"></span>
        <a class="post-meta-link" href="<?php the_permalink() ?>"><?php echo esc_html( get_the_date('F j, Y' ) )  ?></a>
    </div>
</div>


<!-- <header class="c-post__header">
            

    <div  class="c-post__meta"> <?php //_themename_post_meta() ?> </div>

</header> -->
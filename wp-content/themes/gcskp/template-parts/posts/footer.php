<?php if( is_single() ) : ?>  
    
    <div class="post-meta">
        <div class="post-meta-item">
            <span class="post-meta-icon int-chat novi-icon"></span>
            <a class="post-meta-link" href="<?php the_permalink() ?>"> <?php comments_number( '0' ) ?> </a>
        </div>
        <?php if( has_tag() ) : ?> 
            <div class="post-meta-item">
                <span class="post-meta-icon int-tag novi-icon"></span>
                <?php 
                    $tags_list = get_the_tag_list( '<div class="post-tags">', ', ', '</div>' );
                    /* translator: %s is categories list */
                    printf( esc_attr__( 'Tags :   %s', '_themename' ), $tags_list);
                ?>
            </div>
        <?php endif ?>
    </div>

<?php else : ?>

    <div class="post-meta">
        <div class="post-meta-item">
            <span class="post-meta-icon int-chat novi-icon"></span>
            <a class="post-meta-link" href="<?php the_permalink() ?>"> <?php comments_number( '0' ) ?> </a>
        </div>
        <?php if( has_tag() ) : ?> 
            <div class="post-meta-item">
                <span class="post-meta-icon int-tag novi-icon"></span>
                <?php 
                    /* translator: used between categories */ 
                    $tags_list = get_the_tag_list( '<div class="post-tags">', ', ', '</div>' );
                    /* translator: %s is categories list */
                    printf( esc_attr__( 'Tags: %s', '_themename' ), $tags_list);
                ?>
            </div>
        <?php endif ?>
    </div>

<?php endif ?>
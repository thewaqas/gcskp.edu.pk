<!-- Post classic-->
<div <?php post_class('post divider-layout')?>>

    <?php get_template_part( '/template-parts/posts/header' ) ?>

    <?php if( has_post_thumbnail() ) : ?>
        <a class="post-img-link" href="<?php the_permalink() ?>">
            <?php 
            /*
            the_post_thumbnail( 'large', [ 
                'class' => 'lazy-img' 
            ] ); 
            */
            ?>       
            <img class="lazy-img" src="<?php the_post_thumbnail_url( 'large' ) ?>" data-src="<?php the_post_thumbnail_url( 'large' ) ?>" alt="" width="840" height="540">
        </a>
    <?php endif ?> 

    
    <div class="post-text">
        <?php 
            if( has_excerpt() ) {
                echo wp_trim_words( get_the_excerpt(), 100, '...' );
            } else {
                echo wp_trim_words( get_the_content(), 100, '...' );
            }
        ?>
    </div>
    
    
    <?php get_template_part( '/template-parts/posts/footer' ) ?>

    <?php if( !is_single() ) _themename_readmore_link() ?>

    <?php //echo _themename_delete_post() ?>
    
    
</div>

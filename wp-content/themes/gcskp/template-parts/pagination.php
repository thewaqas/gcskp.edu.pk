<!-- Pagination-->
<section class="section section-md bg-transparent" data-preset='{"title":"Pagination","category":"navigation, sample","reload":false,"id":"pagination"}'>
    <div class="container text-center">
        <?php the_posts_pagination() ?>
    </div>
</section>
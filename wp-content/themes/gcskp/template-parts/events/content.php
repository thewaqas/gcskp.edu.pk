<!-- Post number-->
<div class="post post-number post-sm divider-layout">
    
    <?php $eventDate = new DateTime(get_field('event_date')); ?>
    <div class="post-date">
        <div class="post-date-number"> <?php echo $eventDate->format('d') ?> </div>
        <div class="post-date-month"> <?php echo $eventDate->format('M') ?>, <?php echo $eventDate->format('Y') ?>  </div>
    </div>

    <div class="post-inner">

        <h3 class="post-title">
            <a href="<?php the_permalink() ?>"> <?php the_title() ?> </a>
        </h3>
        <div class="post-meta">
            <?php if( has_category() ) : ?> 
                <div class="post-meta-item">
                    <div class="category">
                        <?php the_category( ' ' ); ?>
                    </div>
                </div>
            <?php endif ?>
            <div class="post-meta-item">
                <span class="post-meta-icon int-user novi-icon"></span>
                <a class="post-meta-link" href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ) ?>"> <?php the_author() ?> </a>
            </div>
            <div class="post-meta-item">
                <span class="post-meta-icon int-chat novi-icon"></span>
                <a class="post-meta-link" href="<?php the_permalink() ?>"> <?php comments_number( '0' ) ?> </a>
            </div>
        </div>
        <a class="post-img-link" href="<?php the_permalink() ?>">
            <?php if( has_post_thumbnail() ) : ?>
                <img class="lazy-img" src="<?php the_post_thumbnail_url( 'large' ) ?>" data-src="<?php the_post_thumbnail_url( 'large' ) ?>" alt="<?php the_title() ?>" width="840" height="540">
            <?php else : ?>
                <img class="lazy-img" src="https://gcskp.edu.pk/wp-content/themes/gcskp/dist/assets/images/news-thumbnail.jpg" data-src="https://gcskp.edu.pk/wp-content/themes/gcskp/dist/assets/images/news-thumbnail.jpg" alt="<?php the_title() ?>" width="840" height="540">
            <?php endif ?>
        </a>
        <div class="post-text">
            <?php 
                if( has_excerpt() ){
                    echo wp_trim_words( get_the_excerpt(), 50, '...' );
                }else{
                    echo wp_trim_words( get_the_content(), 50, '...' );
                }
            ?>    
        </div>
        <a class="btn btn-primary post-btn" href="<?php the_permalink() ?>">Read more</a>
      
    </div>
</div>

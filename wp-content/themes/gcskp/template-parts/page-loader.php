<!-- Page Loader-->
<div class="page-loader bg-gradient-animated">
    <svg class="page-loader-progress" x="0px" y="0px" width="180" height="180" viewBox="0 0 180 180">
        <circle class="page-loader-circle" cx="90" cy="90" r="88"></circle>
    </svg>
    <div class="page-loader-inner">
        <div class="page-loader-inner-side page-loader-inner-side-1">
            <img class="pended-img page-loader-logo" src="<?php echo get_theme_file_uri( 'dist/assets/images/logo-default-114x27.svg' ) ?>" data-src="<?php echo get_theme_file_uri( 'dist/assets/images/logo-default-114x27.svg' ) ?>" alt="" width="114" height="27" data-target=".page-loader-inner">
        </div>
        <div class="page-loader-inner-side page-loader-inner-side-2">Loading...</div>
    </div>
</div>
<div class="notifications-area" id="notifications-area"></div>
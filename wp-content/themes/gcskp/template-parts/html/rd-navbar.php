<div class="navbar-cell navbar-sidebar">
    <ul class="navbar-navigation rd-navbar-nav">
        <li class="navbar-navigation-root-item active">
            <a class="navbar-navigation-root-link" href="index.html">Home</a>
            <div class="navbar-navigation-megamenu rd-navbar-megamenu">
                <div class="navbar-navigation-megamenu-container">
                    <div class="navbar-navigation-back">
                        <button class="navbar-navigation-back-btn">Back</button>
                    </div>
                    <div class="navbar-navigation-megamenu-row">
                        <ul class="navbar-navigation-megamenu-column">
                            <li class="navbar-navigation-megamenu-heading">Home Types</li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="index.html">Default</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="home-consulting.html">Consulting</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="home-business.html">Business</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="home-studio.html">Studio</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="home-application.html">Application</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="home-event.html">Event</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item active">
                                <a class="navbar-navigation-megamenu-link" href="home-blog.html">Blog</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="home-store.html">Store</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="home-minimal.html">Minimal</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="home-portfolio.html">Portfolio</a>
                            </li>
                        </ul>
                        <ul class="navbar-navigation-megamenu-column">
                            <li class="navbar-navigation-megamenu-heading">Childs</li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="../green-desk/index.html">GreenDesk</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="../seabay/index.html">Seabay</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="../construction/index.html">Construction</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="../ingreen/index.html">Ingreen</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="../coronavirus/index.html">Coronavirus</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="../charity/index.html">Charity</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="../startup/index.html">Startup</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="../event/index.html">Event</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="../travel-agency/index.html">Travel Agency</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="../seo/index.html">
                                    SEO <span class="badge badge-info">new</span>
                                </a>
                            </li>
                        </ul>
                        <ul class="navbar-navigation-megamenu-column" style="flex-basis: 50%;">
                            <li class="navbar-navigation-banner">
                                <a href="#" target="_blank">
                                    <img class="lazy-img" src="<?php echo get_theme_file_uri( 'dist/assets/images/banner-01.jpg' ) ?>" data-src="<?php echo get_theme_file_uri( 'dist/assets/images/banner-01.jpg' ) ?>" alt="" width="620" height="360">
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </li>
        <li class="navbar-navigation-root-item">
            <a class="navbar-navigation-root-link" href="#">Features</a>
            <ul class="navbar-navigation-dropdown rd-navbar-dropdown">
                <li class="navbar-navigation-back">
                    <button class="navbar-navigation-back-btn">Back</button>
                </li>
                <li class="navbar-navigation-dropdown-item">
                    <a class="navbar-navigation-dropdown-link" href="#">Revolution Slider</a>
                    <ul class="navbar-navigation-dropdown rd-navbar-dropdown">
                        <li class="navbar-navigation-back">
                            <button class="navbar-navigation-back-btn">Back</button>
                        </li>
                        <li class="navbar-navigation-dropdown-item">
                            <a class="navbar-navigation-dropdown-link" href="revolution-parallax-zoom-slices.html">Parallax Zoom Slices</a>
                        </li>
                        <li class="navbar-navigation-dropdown-item">
                            <a class="navbar-navigation-dropdown-link" href="revolution-crossfade.html">Crossfade</a>
                        </li>
                        <li class="navbar-navigation-dropdown-item">
                            <a class="navbar-navigation-dropdown-link" href="revolution-fade-through.html">Fade through</a>
                        </li>
                        <li class="navbar-navigation-dropdown-item">
                            <a class="navbar-navigation-dropdown-link" href="revolution-slide.html">Slide</a>
                        </li>
                        <li class="navbar-navigation-dropdown-item">
                            <a class="navbar-navigation-dropdown-link" href="revolution-overlay.html">Overlay</a>
                        </li>
                        <li class="navbar-navigation-dropdown-item">
                            <a class="navbar-navigation-dropdown-link" href="revolution-slots-zoom.html">Slots Zoom</a>
                        </li>
                    </ul>
                </li>
                <li class="navbar-navigation-dropdown-item">
                    <a class="navbar-navigation-dropdown-link" href="#">Plugins</a>
                    <ul class="navbar-navigation-dropdown rd-navbar-dropdown">
                        <li class="navbar-navigation-back">
                            <button class="navbar-navigation-back-btn">Back</button>
                        </li>
                        <li class="navbar-navigation-dropdown-item">
                            <a class="navbar-navigation-dropdown-link" href="fullcalendar.html">Fullcalendar</a>
                        </li>
                        <li class="navbar-navigation-dropdown-item">
                            <a class="navbar-navigation-dropdown-link" href="parallax.html">Parallax</a>
                        </li>
                        <li class="navbar-navigation-dropdown-item">
                            <a class="navbar-navigation-dropdown-link" href="vide.html">Vide</a>
                        </li>
                        <li class="navbar-navigation-dropdown-item">
                            <a class="navbar-navigation-dropdown-link" href="rd-navbar.html">RD Navbar</a>
                        </li>
                        <li class="navbar-navigation-dropdown-item navbar-navigation-item-disabled">
                            <a class="navbar-navigation-dropdown-link" href="rd-twitter-feed.html">RD Twitter Feed</a>
                        </li>
                        <li class="navbar-navigation-dropdown-item navbar-navigation-item-disabled">
                            <a class="navbar-navigation-dropdown-link" href="rd-flickr-gallery.html">RD Flickr Gallery</a>
                        </li>
                        <li class="navbar-navigation-dropdown-item">
                            <a class="navbar-navigation-dropdown-link" href="rd-mailform.html">RD Mailform</a>
                        </li>
                        <li class="navbar-navigation-dropdown-item">
                            <a class="navbar-navigation-dropdown-link" href="google-recaptcha.html">Google ReCaptcha</a>
                        </li>
                        <li class="navbar-navigation-dropdown-item">
                            <a class="navbar-navigation-dropdown-link" href="subscribe-2.html">Mailchimp &Campaign Monitor</a>
                        </li>
                        <li class="navbar-navigation-dropdown-item">
                            <a class="navbar-navigation-dropdown-link" href="charts-&amp;-graphs.html">Charts &Graphs</a>
                        </li>
                    </ul>
                </li>
                <li class="navbar-navigation-dropdown-item">
                    <a class="navbar-navigation-dropdown-link" href="#">Headers</a>
                    <ul class="navbar-navigation-dropdown rd-navbar-dropdown">
                        <li class="navbar-navigation-back">
                            <button class="navbar-navigation-back-btn">Back</button>
                        </li>
                        <li class="navbar-navigation-dropdown-item">
                            <a class="navbar-navigation-dropdown-link" href="header-default.html">Default</a>
                        </li>
                        <li class="navbar-navigation-dropdown-item">
                            <a class="navbar-navigation-dropdown-link" href="header-consulting.html">Consulting</a>
                        </li>
                        <li class="navbar-navigation-dropdown-item">
                            <a class="navbar-navigation-dropdown-link" href="header-business.html">Business</a>
                        </li>
                        <li class="navbar-navigation-dropdown-item">
                            <a class="navbar-navigation-dropdown-link" href="header-studio.html">Studio</a>
                        </li>
                        <li class="navbar-navigation-dropdown-item">
                            <a class="navbar-navigation-dropdown-link" href="header-app.html">Application</a>
                        </li>
                        <li class="navbar-navigation-dropdown-item">
                            <a class="navbar-navigation-dropdown-link" href="header-event.html">Event</a>
                        </li>
                        <li class="navbar-navigation-dropdown-item">
                            <a class="navbar-navigation-dropdown-link" href="header-blog.html">Blog</a>
                        </li>
                        <li class="navbar-navigation-dropdown-item">
                            <a class="navbar-navigation-dropdown-link" href="header-store.html">Store</a>
                        </li>
                        <li class="navbar-navigation-dropdown-item">
                            <a class="navbar-navigation-dropdown-link" href="header-minimal.html">Minimal</a>
                        </li>
                        <li class="navbar-navigation-dropdown-item navbar-navigation-item-disabled">
                            <a class="navbar-navigation-dropdown-link" href="header-portfolio.html">
                                Portfolio <span class="badge badge-warning">soon</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="navbar-navigation-dropdown-item">
                    <a class="navbar-navigation-dropdown-link" href="footers.html">Footers</a>
                </li>
                <li class="navbar-navigation-dropdown-item">
                    <a class="navbar-navigation-dropdown-link" href="ui-kit.html">Advanced UI Kit</a>
                </li>
            </ul>
        </li>
        <li class="navbar-navigation-root-item">
            <a class="navbar-navigation-root-link" href="#">Pages</a>
            <div class="navbar-navigation-megamenu rd-navbar-megamenu navbar-dark">
                <div class="navbar-navigation-megamenu-container">
                    <div class="navbar-navigation-back">
                        <button class="navbar-navigation-back-btn">Back</button>
                    </div>
                    <h5 class="navbar-navigation-megamenu-heading">Pages</h5>
                    <div class="navbar-navigation-megamenu-row">
                        <ul class="navbar-navigation-megamenu-column">
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="about-us.html">About us</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="about-us-2.html">About us 2</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="about-me.html">About me</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="about-me-2.html">About me 2</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="services.html">Services</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="our-team.html">Our team</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="our-team-2.html">Our team 2</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="team-member.html">Team member</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="team-member-2.html">Team member 2</a>
                            </li>
                        </ul>
                        <ul class="navbar-navigation-megamenu-column">
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="careers.html">Careers</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="faq.html">FAQ</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="faq-2.html">FAQ 2</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="faq-3.html">FAQ 3</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="faq-4.html">FAQ 4</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="contact-me.html">Contact me</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="contact-us.html">Contact us</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="contact-us-2.html">Contact us 2</a>
                            </li>
                        </ul>
                        <ul class="navbar-navigation-megamenu-column">
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="get-in-touch.html">Get in touch</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="contact-us-3.html">Contact us 3</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="subscribe.html">Subscribe</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="sitemap.html">Sitemap</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="coming-soon.html">Coming soon</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="search-results.html">Search results</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="login.html">Login</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="register.html">Register</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="login-register.html">Login / Register</a>
                            </li>
                        </ul>
                        <ul class="navbar-navigation-megamenu-column">
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="pricing.html">Pricing</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="appointment.html">Appointment</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="appointment-calendar.html">Appointment calendar</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="maintenance.html">Maintenance</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="clients.html">Clients</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="under-construction.html">Under construction</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="404-page.html">404 page</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="503-page.html">503 page</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="privacy-policy.html">Privacy policy</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </li>
        <li class="navbar-navigation-root-item">
            <a class="navbar-navigation-root-link" href="#">Portfolio</a>
            <ul class="navbar-navigation-dropdown rd-navbar-dropdown">
                <li class="navbar-navigation-back">
                    <button class="navbar-navigation-back-btn">Back</button>
                </li>
                <li class="navbar-navigation-dropdown-item">
                    <a class="navbar-navigation-dropdown-link" href="#">Grid layout</a>
                    <ul class="navbar-navigation-dropdown rd-navbar-dropdown">
                        <li class="navbar-navigation-back">
                            <button class="navbar-navigation-back-btn">Back</button>
                        </li>
                        <li class="navbar-navigation-dropdown-item">
                            <a class="navbar-navigation-dropdown-link" href="portfolio-grid-horizontal.html">Horizontal</a>
                        </li>
                        <li class="navbar-navigation-dropdown-item">
                            <a class="navbar-navigation-dropdown-link" href="portfolio-grid-vertical.html">Vertical</a>
                        </li>
                    </ul>
                </li>
                <li class="navbar-navigation-dropdown-item">
                    <a class="navbar-navigation-dropdown-link" href="#">Masonry layout</a>
                    <ul class="navbar-navigation-dropdown rd-navbar-dropdown">
                        <li class="navbar-navigation-back">
                            <button class="navbar-navigation-back-btn">Back</button>
                        </li>
                        <li class="navbar-navigation-dropdown-item">
                            <a class="navbar-navigation-dropdown-link" href="portfolio-masonry-horizontal.html">Horizontal</a>
                        </li>
                        <li class="navbar-navigation-dropdown-item">
                            <a class="navbar-navigation-dropdown-link" href="portfolio-masonry-vertical.html">Vertical</a>
                        </li>
                    </ul>
                </li>
                <li class="navbar-navigation-dropdown-item">
                    <a class="navbar-navigation-dropdown-link" href="#">Wide layout</a>
                    <ul class="navbar-navigation-dropdown rd-navbar-dropdown">
                        <li class="navbar-navigation-back">
                            <button class="navbar-navigation-back-btn">Back</button>
                        </li>
                        <li class="navbar-navigation-dropdown-item">
                            <a class="navbar-navigation-dropdown-link" href="portfolio-wide-horizontal.html">Horizontal</a>
                        </li>
                        <li class="navbar-navigation-dropdown-item">
                            <a class="navbar-navigation-dropdown-link" href="portfolio-wide-vertical.html">Vertical</a>
                        </li>
                    </ul>
                </li>
                <li class="navbar-navigation-dropdown-item">
                    <a class="navbar-navigation-dropdown-link" href="#">Fullwidth layout</a>
                    <ul class="navbar-navigation-dropdown rd-navbar-dropdown">
                        <li class="navbar-navigation-back">
                            <button class="navbar-navigation-back-btn">Back</button>
                        </li>
                        <li class="navbar-navigation-dropdown-item">
                            <a class="navbar-navigation-dropdown-link" href="portfolio-fullwidth-3.html">3 columns</a>
                        </li>
                        <li class="navbar-navigation-dropdown-item">
                            <a class="navbar-navigation-dropdown-link" href="portfolio-fullwidth-4.html">4 columns</a>
                        </li>
                        <li class="navbar-navigation-dropdown-item">
                            <a class="navbar-navigation-dropdown-link" href="portfolio-fullwidth-5.html">5 columns</a>
                        </li>
                    </ul>
                </li>
                <li class="navbar-navigation-dropdown-item">
                    <a class="navbar-navigation-dropdown-link" href="#">Custom effects</a>
                    <ul class="navbar-navigation-dropdown rd-navbar-dropdown">
                        <li class="navbar-navigation-back">
                            <button class="navbar-navigation-back-btn">Back</button>
                        </li>
                        <li class="navbar-navigation-dropdown-item">
                            <a class="navbar-navigation-dropdown-link" href="portfolio-bella.html">Bella effect</a>
                        </li>
                        <li class="navbar-navigation-dropdown-item">
                            <a class="navbar-navigation-dropdown-link" href="portfolio-connor.html">Connor effect</a>
                        </li>
                        <li class="navbar-navigation-dropdown-item">
                            <a class="navbar-navigation-dropdown-link" href="portfolio-frode.html">Frode effect</a>
                        </li>
                        <li class="navbar-navigation-dropdown-item">
                            <a class="navbar-navigation-dropdown-link" href="portfolio-janes.html">Janes effect</a>
                        </li>
                        <li class="navbar-navigation-dropdown-item">
                            <a class="navbar-navigation-dropdown-link" href="portfolio-josip.html">Josip effect</a>
                        </li>
                        <li class="navbar-navigation-dropdown-item">
                            <a class="navbar-navigation-dropdown-link" href="portfolio-mike.html">Mike effect</a>
                        </li>
                        <li class="navbar-navigation-dropdown-item">
                            <a class="navbar-navigation-dropdown-link" href="portfolio-tamaz.html">Tamaz effect</a>
                        </li>
                    </ul>
                </li>
                <li class="navbar-navigation-dropdown-item">
                    <a class="navbar-navigation-dropdown-link" href="#">Single project</a>
                    <ul class="navbar-navigation-dropdown rd-navbar-dropdown">
                        <li class="navbar-navigation-back">
                            <button class="navbar-navigation-back-btn">Back</button>
                        </li>
                        <li class="navbar-navigation-dropdown-item">
                            <a class="navbar-navigation-dropdown-link" href="portfolio-single-1.html">Default</a>
                        </li>
                        <li class="navbar-navigation-dropdown-item">
                            <a class="navbar-navigation-dropdown-link" href="portfolio-single-2.html">Variant 2</a>
                        </li>
                        <li class="navbar-navigation-dropdown-item">
                            <a class="navbar-navigation-dropdown-link" href="portfolio-single-3.html">Variant 3</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </li>
        <li class="navbar-navigation-root-item">
            <a class="navbar-navigation-root-link" href="#">Blog</a>
            <ul class="navbar-navigation-dropdown rd-navbar-dropdown">
                <li class="navbar-navigation-back">
                    <button class="navbar-navigation-back-btn">Back</button>
                </li>
                <li class="navbar-navigation-dropdown-item">
                    <a class="navbar-navigation-dropdown-link" href="#">Blog classic</a>
                    <ul class="navbar-navigation-dropdown rd-navbar-dropdown">
                        <li class="navbar-navigation-back">
                            <button class="navbar-navigation-back-btn">Back</button>
                        </li>
                        <li class="navbar-navigation-dropdown-item">
                            <a class="navbar-navigation-dropdown-link" href="blog-classic-no-sidebar.html">No sidebar</a>
                        </li>
                        <li class="navbar-navigation-dropdown-item">
                            <a class="navbar-navigation-dropdown-link" href="blog-classic-with-sidebar.html">With sidebar</a>
                        </li>
                        <li class="navbar-navigation-dropdown-item">
                            <a class="navbar-navigation-dropdown-link" href="blog-classic-2-columns.html">2 columns</a>
                        </li>
                        <li class="navbar-navigation-dropdown-item">
                            <a class="navbar-navigation-dropdown-link" href="blog-classic-3-columns.html">3 columns</a>
                        </li>
                        <li class="navbar-navigation-dropdown-item">
                            <a class="navbar-navigation-dropdown-link" href="blog-classic-creative.html">Creative</a>
                        </li>
                    </ul>
                </li>
                <li class="navbar-navigation-dropdown-item">
                    <a class="navbar-navigation-dropdown-link" href="#">Blog boxed</a>
                    <ul class="navbar-navigation-dropdown rd-navbar-dropdown">
                        <li class="navbar-navigation-back">
                            <button class="navbar-navigation-back-btn">Back</button>
                        </li>
                        <li class="navbar-navigation-dropdown-item">
                            <a class="navbar-navigation-dropdown-link" href="blog-boxed-no-sidebar.html">No sidebar</a>
                        </li>
                        <li class="navbar-navigation-dropdown-item">
                            <a class="navbar-navigation-dropdown-link" href="blog-boxed-with-sidebar.html">With sidebar</a>
                        </li>
                    </ul>
                </li>
                <li class="navbar-navigation-dropdown-item">
                    <a class="navbar-navigation-dropdown-link" href="#">Blog thumbnail</a>
                    <ul class="navbar-navigation-dropdown rd-navbar-dropdown">
                        <li class="navbar-navigation-back">
                            <button class="navbar-navigation-back-btn">Back</button>
                        </li>
                        <li class="navbar-navigation-dropdown-item">
                            <a class="navbar-navigation-dropdown-link" href="blog-thumbnail-no-sidebar.html">No sidebar</a>
                        </li>
                        <li class="navbar-navigation-dropdown-item">
                            <a class="navbar-navigation-dropdown-link" href="blog-thumbnail-with-sidebar.html">With sidebar</a>
                        </li>
                        <li class="navbar-navigation-dropdown-item">
                            <a class="navbar-navigation-dropdown-link" href="blog-thumbnail-2-columns.html">2 columns</a>
                        </li>
                        <li class="navbar-navigation-dropdown-item">
                            <a class="navbar-navigation-dropdown-link" href="blog-thumbnail-creative.html">Creative</a>
                        </li>
                    </ul>
                </li>
                <li class="navbar-navigation-dropdown-item">
                    <a class="navbar-navigation-dropdown-link" href="#">Blog modern</a>
                    <ul class="navbar-navigation-dropdown rd-navbar-dropdown">
                        <li class="navbar-navigation-back">
                            <button class="navbar-navigation-back-btn">Back</button>
                        </li>
                        <li class="navbar-navigation-dropdown-item">
                            <a class="navbar-navigation-dropdown-link" href="blog-modern-no-sidebar.html">No sidebar</a>
                        </li>
                        <li class="navbar-navigation-dropdown-item">
                            <a class="navbar-navigation-dropdown-link" href="blog-modern-with-sidebar.html">With sidebar</a>
                        </li>
                        <li class="navbar-navigation-dropdown-item">
                            <a class="navbar-navigation-dropdown-link" href="blog-modern-rounded.html">Rounded</a>
                        </li>
                        <li class="navbar-navigation-dropdown-item">
                            <a class="navbar-navigation-dropdown-link" href="blog-modern-2-columns.html">2 columns</a>
                        </li>
                        <li class="navbar-navigation-dropdown-item">
                            <a class="navbar-navigation-dropdown-link" href="blog-modern-large.html">Large layout</a>
                        </li>
                    </ul>
                </li>
                <li class="navbar-navigation-dropdown-item">
                    <a class="navbar-navigation-dropdown-link" href="blog-dated.html">Blog dated</a>
                </li>
                <li class="navbar-navigation-dropdown-item">
                    <a class="navbar-navigation-dropdown-link" href="#">Blog hover</a>
                    <ul class="navbar-navigation-dropdown rd-navbar-dropdown">
                        <li class="navbar-navigation-back">
                            <button class="navbar-navigation-back-btn">Back</button>
                        </li>
                        <li class="navbar-navigation-dropdown-item">
                            <a class="navbar-navigation-dropdown-link" href="blog-hover-2-columns.html">2 columns</a>
                        </li>
                        <li class="navbar-navigation-dropdown-item">
                            <a class="navbar-navigation-dropdown-link" href="blog-hover-3-columns.html">3 columns</a>
                        </li>
                    </ul>
                </li>
                <li class="navbar-navigation-dropdown-item">
                    <a class="navbar-navigation-dropdown-link" href="blog-creative.html">Blog creative</a>
                </li>
                <li class="navbar-navigation-dropdown-item">
                    <a class="navbar-navigation-dropdown-link" href="#">Blog post</a>
                    <ul class="navbar-navigation-dropdown rd-navbar-dropdown">
                        <li class="navbar-navigation-back">
                            <button class="navbar-navigation-back-btn">Back</button>
                        </li>
                        <li class="navbar-navigation-dropdown-item">
                            <a class="navbar-navigation-dropdown-link" href="post.html">No sidebar</a>
                        </li>
                        <li class="navbar-navigation-dropdown-item">
                            <a class="navbar-navigation-dropdown-link" href="post-with-sidebar.html">With sidebar</a>
                        </li>
                        <li class="navbar-navigation-dropdown-item">
                            <a class="navbar-navigation-dropdown-link" href="post-intro.html">Intro</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </li>
        <li class="navbar-navigation-root-item">
            <a class="navbar-navigation-root-link" href="#">Shop</a>
            <ul class="navbar-navigation-dropdown rd-navbar-dropdown">
                <li class="navbar-navigation-back">
                    <button class="navbar-navigation-back-btn">Back</button>
                </li>
                <li class="navbar-navigation-dropdown-item">
                    <a class="navbar-navigation-dropdown-link" href="#">Grid view</a>
                    <ul class="navbar-navigation-dropdown rd-navbar-dropdown rd-navbar-open-left">
                        <li class="navbar-navigation-back">
                            <button class="navbar-navigation-back-btn">Back</button>
                        </li>
                        <li class="navbar-navigation-dropdown-item">
                            <a class="navbar-navigation-dropdown-link" href="grid-shop-no-sidebar.html">No sidebar</a>
                        </li>
                        <li class="navbar-navigation-dropdown-item">
                            <a class="navbar-navigation-dropdown-link" href="grid-shop-left-sidebar.html">Left sidebar</a>
                        </li>
                        <li class="navbar-navigation-dropdown-item">
                            <a class="navbar-navigation-dropdown-link" href="grid-shop-right-sidebar.html">Right sidebar</a>
                        </li>
                    </ul>
                </li>
                <li class="navbar-navigation-dropdown-item">
                    <a class="navbar-navigation-dropdown-link" href="#">List view</a>
                    <ul class="navbar-navigation-dropdown rd-navbar-dropdown rd-navbar-open-left">
                        <li class="navbar-navigation-back">
                            <button class="navbar-navigation-back-btn">Back</button>
                        </li>
                        <li class="navbar-navigation-dropdown-item">
                            <a class="navbar-navigation-dropdown-link" href="list-shop-left-sidebar.html">Left sidebar</a>
                        </li>
                        <li class="navbar-navigation-dropdown-item">
                            <a class="navbar-navigation-dropdown-link" href="list-shop-right-sidebar.html">Right sidebar</a>
                        </li>
                    </ul>
                </li>
                <li class="navbar-navigation-dropdown-item">
                    <a class="navbar-navigation-dropdown-link" href="#">Single product</a>
                    <ul class="navbar-navigation-dropdown rd-navbar-dropdown rd-navbar-open-left">
                        <li class="navbar-navigation-back">
                            <button class="navbar-navigation-back-btn">Back</button>
                        </li>
                        <li class="navbar-navigation-dropdown-item">
                            <a class="navbar-navigation-dropdown-link" href="product-page.html">Product page</a>
                        </li>
                        <li class="navbar-navigation-dropdown-item">
                            <a class="navbar-navigation-dropdown-link" href="product-page-2.html">Product page 2</a>
                        </li>
                        <li class="navbar-navigation-dropdown-item">
                            <a class="navbar-navigation-dropdown-link" href="product-page-3.html">Product page 3</a>
                        </li>
                    </ul>
                </li>
                <li class="navbar-navigation-dropdown-item">
                    <a class="navbar-navigation-dropdown-link" href="cart.html">Cart</a>
                </li>
            </ul>
        </li>
        <li class="navbar-navigation-root-item">
            <a class="navbar-navigation-root-link" href="#">Components</a>
            <div class="navbar-navigation-megamenu rd-navbar-megamenu navbar-dark">
                <div class="navbar-navigation-megamenu-container">
                    <div class="navbar-navigation-back">
                        <button class="navbar-navigation-back-btn">Back</button>
                    </div>
                    <h5 class="navbar-navigation-megamenu-heading">Components</h5>
                    <div class="navbar-navigation-megamenu-row">
                        <ul class="navbar-navigation-megamenu-column">
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="typography.html">Typography</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="buttons.html">Buttons</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="counters.html">Counters</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="dividers.html">Dividers</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="form-elements.html">Form elements</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="icons.html">Icons</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="infographics.html">Infographics</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="badges.html">Badges</a>
                            </li>
                        </ul>
                        <ul class="navbar-navigation-megamenu-column">
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="tables.html">Tables</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="tabs.html">Tabs</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="breadcrumbs.html">Breadcrumbs</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="accordions.html">Accordions</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="thumbnails.html">Thumbnails</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="alerts.html">Alerts</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="testimonials.html">Testimonials</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="countdowns.html">Countdowns</a>
                            </li>
                        </ul>
                        <ul class="navbar-navigation-megamenu-column">
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="blurbs.html">Blurbs</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="persons.html">Persons</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="pricing-and-plans.html">Pricing and plans</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="pagination.html">Pagination</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="comments.html">Comments</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="lists.html">Lists</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="grid-system.html">Grid system</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="calls-to-action.html">Calls to action</a>
                            </li>
                        </ul>
                        <ul class="navbar-navigation-megamenu-column">
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="posts.html">Posts</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="widgets.html">Widgets</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="maps.html">Maps</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="offer-boxes.html">Offer boxes</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="media-elements.html">Media elements</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="text-rotator.html">Text rotator</a>
                            </li>
                            <li class="navbar-navigation-megamenu-item">
                                <a class="navbar-navigation-megamenu-link" href="animations.html">Animations</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </li>
    </ul>
</div>
<div class="snackbar snackbar-primary">
                            <div class="snackbar-inner">
                                <div class="snackbar-title">
                                    <span class="icon snackbar-icon novi-icon int-check"></span>
                                    Marked as read
                                </div>
                            </div>
                        </div>

                        <div class="snackbar snackbar-danger">
                            <div class="snackbar-inner">
                                <div class="snackbar-title">
                                    <span class="icon snackbar-icon novi-icon int-warning"></span>
                                    Form submission failed
                                </div>
                            </div>
                        </div>

                        <div class="snackbar snackbar-secondary">
                            <div class="snackbar-inner">
                                <div class="snackbar-title" id="student-status">
                                    <span class="icon snackbar-icon novi-icon fa-circle-o-notch fa-spin"></span>
                                    Please wait! We are submiting your data
                                </div>
                            </div>
                        </div>
                        
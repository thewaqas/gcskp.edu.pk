

<!-- Breadcrumb dark centered-->
<section class="section section-md context-dark bg-image" style="background-image: url( https://gcskp.edu.pk/wp-content/themes/gcskp/dist/assets/images/img.jpg ) ">
    <div class="container">

        <div class="row">
            <div class="col-md-12 col-lg-6 text-center text-lg-left">
                <h4> <?php the_title() ?> </h4>
            </div>

            <div class="col-md-12 col-lg-6 text-center text-lg-right"> 
                <ul class="breadcrumb d-inline-flex justify-content-center">
                    <li class="breadcrumb-item">
                        <a class="breadcrumb-link" href="<?php echo home_url('/') ?>">Home</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a class="breadcrumb-link" href="<?php echo home_url('/news') ?>">News</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a class="breadcrumb-link" href="<?php echo home_url('/news') ?>">page.php</a>
                    </li>
                </ul>
            </div>
        </div>

    </div>
</section>

<?php

function _themename_general_customizer_section( $wp_customize ) 
{

    /*######################    GENERAL SETTINGS  ##########################*/ 
    $wp_customize->add_section('_themename_general_settings', array(
        'title'         => esc_html__( 'General Settings', '_themename' ),
        'description'   => esc_html__( 'These are the General Settings', '_themename' ),
        'priority'      => 30,
        'panel'         => '_themename'
    ));


    // Theme Color Settings
    $wp_customize->add_setting('_themename_primary_color', array(
        'default'           => '#ff4500',
        'sanitize_callabck' => 'sanitize_hex_color',
        'transport'         => 'postMessage'
    ));
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, '_themename_primary_color', 
        array(
            'label'      => __( 'Theme Primary Color', '_themename' ),
            'section'    => '_themename_general_settings',
        ) ) 
    );




    // Custom Slug
    $wp_customize->add_setting('_themename_portfolio_slug', array(
        'default'           => 'portfolio',
        'transport'         => 'postMessage',
        'sanitize_callback' => 'sanitize_text_field',
    ));
    $wp_customize->add_control('_themename_portfolio_slug', array(
        'type'          => 'text',
        'label'         => esc_html__('Portfolio Slug', '_themename'),
        'description'   => esc_html__('Portfolio Slug will appera in archive url', '_themename'),
        'section'       => '_themename_general_settings'
    ));

}
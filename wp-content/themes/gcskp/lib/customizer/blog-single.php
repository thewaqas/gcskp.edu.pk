<?php

function _themename_blogSingle_customizer_section( $wp_customize ) 
{

    /*######################    SINGLE SETTINGS  ##########################*/
    $wp_customize->add_section('_themename_single_blog_options', array(
        'title'         => esc_html__( 'Single Blog Options', '_themename' ),
        'description'   => esc_html__( 'These are the Single Blog Options', '_themename' ),
        'active_callback' => '_themename_show_single_blog_section',
        'priority'      => 30,
        'panel'         => '_themename'
    ));
    $wp_customize->add_setting('_themename_display_author_info', array(
        'default'           => true,
        'sanitize_callabck' => '_themename_sanitize_checkbox',
        'transport'         => 'postMessage'
    ));
    $wp_customize->add_control('_themename_display_author_info', array(
        'type'      => 'checkbox',
        'label'     => esc_html__('Show Author Info', '_themename'),
        'section'   => '_themename_single_blog_options'
    ));

    function _themename_show_single_blog_section() { 
        global $post;
        return is_single() && $post->post_type === 'post';
    }
    function _themename_sanitize_checkbox( $checked ) {
        return ( isset( $checked ) && $checked === true ) ? true : false;
    } 

}
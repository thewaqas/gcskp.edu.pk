<?php

function _themename_footer_customizer_section( $wp_customize ) {

    
    $wp_customize->selective_refresh->add_partial( 
        '_themename_footer_partial', 
        array(
            'settings' => array(
                '_themename_footer_copyright_text', 
                '_themename_footer_bg', 
                '_themename_footer_layout', 
                '_themename_footer_layout'
            ),
            'selector'  => '#footer',
            'container_inclusive' => false,
            'render_callback' => function() {
                get_template_part( 'template-parts/footer/widgets' );
                get_template_part( 'template-parts/footer/info' );
            }
        )
    );

    // Costimizer API setup for footer setting
    $wp_customize->add_section('_themename_footer_section', array(
        'title'         => esc_html__( 'Footer Settings', '_themename' ),
        'description'   => esc_html__( 'These are the Footer setting', '_themename' ),
        'priority'      => 30,
        'panel'         => '_themename'
    ));

    // footer_copyright_text
    $wp_customize->add_setting('_themename_footer_copyright_text', array(
        'default'           => 'Copyright &copy 2021. All rights reserved.',
        // 'sanitize_callabck' => '_themename_sanitize_copyright_text',
        // 'transport'         => 'postMessage'
    ));
    $wp_customize->add_control( new WP_Customize_Control ( 
        $wp_customize, 
        '_themename_footer_copyright_text_input', 
            array(
                'label'          => esc_html__('Website Copyright Text', '_themename'),
                'section'        => '_themename_footer_section',
                'settings'       => '_themename_footer_copyright_text',
                'type'           => 'text',
            )
        ) 
    );

    // Footer Terms of Services Page
    $wp_customize->add_setting('_themename_footer_tos_page', array(
        'default'           => 0,
        'transport'         => 'postMessage'
    ));
    $wp_customize->add_control( new WP_Customize_Control ( 
        $wp_customize, 
        '_themename_footer_tos_page_input', 
            array(
                'label'          => __( 'Show Terms of Services Page', '_themename' ),
                'section'        => '_themename_footer_section',
                'settings'       => '_themename_footer_tos_page',
                'type'           => 'dropdown-pages',
            )
        ) 
    );
    

    // Footer Privacy Policy Page
    $wp_customize->add_setting('_themename_footer_privacy_page', array(
        'default'           => 0,
        'transport'         => 'postMessage'
    ));
    $wp_customize->add_control( new WP_Customize_Control ( 
        $wp_customize, 
        '_themename_footer_privacy_page_input', 
            array(
                'label'          => __( 'Show Privacy Policy Page', '_themename' ),
                'section'        => '_themename_footer_section',
                'settings'       => '_themename_footer_privacy_page',
                'type'           => 'dropdown-pages',
            )
        ) 
    );








    /*######################    FOOTER SETTINGS  ##########################*/ 
    // Footer background
    $wp_customize->add_setting('_themename_footer_bg', array(
        'default'           => 'dark',
        'sanitize_callabck' => '_themename_sanitize_footer_bg',
        'transport'         => 'postMessage'
    ));
    $wp_customize->add_control('_themename_footer_bg', array(
        'type'      => 'select',
        'label'     => esc_html__('Footer Background', '_themename'),
        'choices'   => array(
            'light' => esc_html__('Light', '_themename'),
            'dark' => esc_html__('Dark', '_themename'),
        ),
        'section'   => '_themename_footer_section'
    ));


    // Column Layout
    $wp_customize->add_setting('_themename_footer_layout', array(
        'default'           => '3,3,3,3',
        'sanitize_callback' => 'sanitize_text_field',
        'transport'         => 'postMessage',
        'validate_callback' => '_themename_validate_footer_layout'
    ));
    $wp_customize->add_control('_themename_footer_layout', array(
        'type'      => 'text',
        'label'     => esc_html__('Footer Layout', '_themename'),
        'section'   => '_themename_footer_section'
    ));

}




/*######################    sanitize_site_info  ##########################*/
function _themename_sanitize_copyright_text( $input )
{
    $allowed = array( 'a' => array(
        'href'  => array(),
        'title' => array(),
    ));
    return wp_kses($input, $allowed);
}

/*######################    sanitize_footer_bg  ##########################*/ 
function _themename_sanitize_footer_bg( $input )
{
    $valid = array( 'light', 'dark' );
    if( in_array($input, $valid, true) ) return $input;
    else return 'dark';
} 


/*######################    validate_footer_layout  ##########################*/
function _themename_validate_footer_layout( $validity, $value )
{
    if(!preg_match('/^([1-9]|1[012])(,([1-9]|1[012]))*$/', $value)) 
    {
        $validity->add('invalid_footer_layout', esc_html__( 'Footer layout is invalid', '_themename' ));
    }
    return $validity;
}
<?php

function _themename_social_customizer_section( $wp_customize ) 
{

    /*######################    Social SETTINGS  ##########################*/ 
    
    $wp_customize->add_section('_themename_social_section', array(
        'title'         => esc_html__( 'Social Settings', '_themename' ),
        'description'   => esc_html__( 'These are the Social Links or Settings for Website. Please just provide the names of your social acccount ', '_themename' ),
        'priority'      => 30,
        'panel'         => '_themename'
    ));

    // facebook_handle
    $wp_customize->add_setting('_themename_facebook_handle', array(
        'default'       => '',
        'transport'     => 'postMessage'
    ));
    $wp_customize->add_control( new WP_Customize_Control ( 
        $wp_customize, 
        '_themename_facebook_handle_input', 
            array(
                'label'          => __( 'Facebool Handle', '_themename' ),
                'section'        => '_themename_social_section',
                'settings'       => '_themename_facebook_handle',
                'type'           => 'text'
            )
        ) 
    );

    // twitter_handle
    $wp_customize->add_setting('_themename_twitter_handle', array(
        'default'       => '',
        'transport'     => 'postMessage'
    ));
    $wp_customize->add_control( new WP_Customize_Control ( 
        $wp_customize, 
        '_themename_twitter_handle', 
            array(
                'label'          => __( 'Twitter Handle', '_themename' ),
                'section'        => '_themename_social_section',
                'settings'       => '_themename_twitter_handle',
                'type'           => 'text'
            )
        ) 
    );


    // instagram_handle
    $wp_customize->add_setting('_themename_instagram_handle', array(
        'default'       => '',
        'transport'     => 'postMessage'
    ));
    $wp_customize->add_control( new WP_Customize_Control ( 
        $wp_customize, 
        '_themename_instagram_handle', 
            array(
                'label'          => __( 'Instagram Handle', '_themename' ),
                'section'        => '_themename_social_section',
                'settings'       => '_themename_instagram_handle',
                'type'           => 'text'
            )
        ) 
    );


    // linkedin_handle
    $wp_customize->add_setting('_themename_linkedin_handle', array(
        'default'       => '',
        'transport'     => 'postMessage'
    ));
    $wp_customize->add_control( new WP_Customize_Control ( 
        $wp_customize, 
        '_themename_linkedin_handle', 
            array(
                'label'          => __( 'Linkedin Handle', '_themename' ),
                'section'        => '_themename_social_section',
                'settings'       => '_themename_linkedin_handle',
                'type'           => 'text'
            )
        ) 
    );

    // youtube_handle
    $wp_customize->add_setting('_themename_youtube_handle', array(
        'default'       => '',
        'transport'     => 'postMessage'
    ));
    $wp_customize->add_control( new WP_Customize_Control ( 
        $wp_customize, 
        '_themename_youtube_handle', 
            array(
                'label'          => __( 'Youtube Handle', '_themename' ),
                'section'        => '_themename_social_section',
                'settings'       => '_themename_youtube_handle',
                'type'           => 'text'
            )
        ) 
    );


    
}
<?php

function _themename_misc_customizer_section( $wp_customize ) {

    $wp_customize->add_section('_themename_misc_section', array(
        'title'         => esc_html__( 'Miscellaneous Settings', '_themename' ),
        'description'   => esc_html__( 'These are the Miscellaneous or Settings for Website.', '_themename' ),
        'priority'      => 30,
        'panel'         => '_themename'
    ));


    // header_show_search
    $wp_customize->add_setting('_themename_header_show_search', array(
        'default'       => 'yes',
        'transport'     => 'postMessage'
    ));
    $wp_customize->add_control( new WP_Customize_Control ( 
        $wp_customize, 
        '_themename_header_show_search_input', 
            array(
                'label'          => __( 'Show Search Icon in Header', '_themename' ),
                'section'        => '_themename_misc_section',
                'settings'       => '_themename_header_show_search',
                'type'           => 'checkbox',
                'choices'        => array (
                    'yes'        => 'Yes',
                )
            )
        ) 
    );

    
    // header_show_user
    $wp_customize->add_setting('_themename_header_show_user', array(
        'default'       => 'yes',
        'transport'     => 'postMessage'
    ));
    $wp_customize->add_control( new WP_Customize_Control ( 
        $wp_customize, 
        '_themename_header_show_user_input', 
            array(
                'label'          => __( 'Show User Icon in Header', '_themename' ),
                'section'        => '_themename_misc_section',
                'settings'       => '_themename_header_show_user',
                'type'           => 'checkbox',
                'choices'        => array (
                    'yes'        => 'Yes',
                )
            )
        ) 
    );



    $wp_customize->add_setting('_themename_to_top_color', array(
        'default'       => '#31c77f',
        // 'transport'     => 'postMessage'
    ));
    $wp_customize->add_control( new WP_Customize_Color_Control ( 
        $wp_customize, 
        '_themename_to_top_color_input', 
            array(
                'label'          => __( 'To-top Button Color', '_themename' ),
                'section'        => '_themename_misc_section',
                'settings'       => '_themename_to_top_color',
            )
        ) 
    );


    $wp_customize->add_setting('_themename_report_file', array(
        'default'       => '',
    ));
    $wp_customize->add_control( new WP_Customize_Upload_Control ( 
        $wp_customize, 
        '_themename_report_file_input', 
            array(
                'label'          => __( 'Monthly Report File', '_themename' ),
                'section'        => '_themename_misc_section',
                'settings'       => '_themename_report_file',
            )
        ) 
    );

}
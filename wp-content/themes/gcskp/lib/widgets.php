<?php 

/**
 * Add a sidebar.
 */

function _themename_sidebar_widget() {
    register_sidebar( array(
        'id'            => 'primary-sidebar',
        'name'          => esc_html__('Primary Sidebar', '_themename'),
        'description'   => esc_html__( 'This sidebar widget is shown on all posts page.', '_themename' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h6 class="widget-title">',
        'after_title'   => '</h6>',
    ) );

    register_sidebar( array(
        'id'            => 'event-sidebar',
        'name'          => esc_html__('Event Sidebar', '_themename'),
        'description'   => esc_html__( 'This sidebar widget is shown on all posts page.', '_themename' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h6 class="widget-title">',
        'after_title'   => '</h6>',
    ) );
}


?>
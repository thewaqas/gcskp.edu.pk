<?php
class _themename_Custom_Nav_Walker extends Walker_Nav_Menu {

  public function start_lvl( &$output, $depth = 0, $args = [] ) {
    $output .= '<ul class="navbar-navigation-dropdown rd-navbar-dropdown rd-navbar-open-right"><li class="navbar-navigation-back"><button class="navbar-navigation-back-btn">Back</button></li>';
  }

  public function start_el( &$output, $item, $depth = 0, $args = [], $id = 0 ) {
    $output .= '<li class="navbar-navigation-dropdown-item">';
    $output .= $args->before;
    $output .= '<a class="navbar-navigation-dropdown-link" href="'.$item->url.'">';
    $output .= $args->link_before . $item->title . $args->link_after;
    $output .= '</a>';
    $output .= $args->after;
  }

  public function end_el( &$output, $item, $depth = 0, $args = [], $id = 0 ) {
    $output .= '</li>';
  }

  public function end_lvl( &$output, $depth = 0, $args = [] ) {
    $output .= '</ul>';
  }

}
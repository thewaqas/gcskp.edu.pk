<?php 


function _themename_customize_register( $wp_customize ) 
{

    // echo '<pre>';
    // var_dump( $wp_customize );
    // echo '</pre>';

    $wp_customize->get_section ( 'title_tagline' )->title = "General *";

    $wp_customize->add_panel( '_themename', 
        array (
            'title'         => __('Theme Settings (gcskp)', '_themename'),
            'description'   => 'This is custom panel',
            'priority'      => 10
        ) 
    );



    
    _themename_blogSingle_customizer_section( $wp_customize );

    _themename_general_customizer_section   ( $wp_customize );

    _themename_social_customizer_section    ( $wp_customize );

    _themename_misc_customizer_section      ( $wp_customize );

    _themename_footer_customizer_section    ( $wp_customize );



    $wp_customize->get_setting('blogname')->transport = 'postMessage';

    /*=================    SELECTIVE REFRESH  =================*/ 
    $wp_customize->selective_refresh->add_partial( 'blogname', array(
        // 'settings' => array('blogname'),
        'selector'  => '.navbar-logo-link',
        'container_inclusive' => false,
        'render_callback' => function() {
            bloginfo( 'name' );
        }
    ));


}


 

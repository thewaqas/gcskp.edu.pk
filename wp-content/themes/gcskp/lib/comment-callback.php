<?php

function _themename_comment_callback( $comment, $args, $depth ) {
    
?>

<li id="comment-<?php comment_ID() ?>" <?php comment_class( ['comment-bordered'] ) ?>>
    
    <div class="media media-md flex-column flex-sm-row">

        <div class="media-left">
            <div class="mt-1 d-none d-sm-inline-block">
                <?php 
                    if ( $args['avatar_size'] != 0 ) {
                        $avatar = get_avatar_url( $comment, $args['avatar_size'] ); 
                    } 
                ?>
                <img class="lazy-img comment-image" src="<?php echo $avatar ?>" data-src="<?php echo $avatar ?>" alt="" width="60" height="60">
            </div>
        </div>

        <div id="div-comment-<?php comment_ID() ?>" class="media-body">

            <div class="comment-panel">

                
                <div class="media media-xs align-items-center">
                    <div class="media-left d-sm-none">
                        <?php 
                            if ( $args['avatar_size'] != 0 ) {
                                $avatar = get_avatar_url( $comment, $args['avatar_size'] ); 
                            } 
                        ?>
                        <img class="lazy-img comment-image" src="<?php echo $avatar ?>" data-src="<?php echo $avatar ?>" alt="" width="60" height="60">
                    </div>
                    <div class="media-body">
                        <ul class="comment-meta">
                            <li class="comment-meta-item">
                                By <span class="font-weight-medium"> <?php echo get_comment_author_link( $comment ) ?> </span>
                            </li>
                            <li class="comment-meta-item">
                                Posted 
                                <time class="font-weight-medium"  datetime="<?php comment_time( 'c' )?>">
                                    <?php 
                                        printf ( 
                                            esc_html__( '%s ago', '_themename' ), 
                                            human_time_diff( get_comment_time( 'U' ), 
                                            current_time( 'U' ) ) 
                                        );  
                                    ?>
                                </time>
                            </li>
                        </ul>
                    </div>
                </div>
            

                <?php 
                    comment_reply_link( array_merge( $args, array (
                        'depth'     => $depth,
                        'add_below' => 'div-comment',
                        'before'    => '<div class="comment-link">',
                        'afetr'     => '</div>'
                    ) ) );
                ?>
                <?php edit_comment_link( esc_html__( ' Edit', '_themename' ), '<span class="comment-link">', '</span>' )?>

            </div>
            <br>
            <div class="comment-text">
                
                <?php if( $comment->comment_approved == '0') : ?>
                    <p class=""><?php esc_html_e( 'Your comment is awaiting moderation', '_themename' )?></p>
                <?php endif ?>

                <?php 
                    if($comment->comment_type == '' || (($comment->comment_type == 'pingback' || $comment->comment_type == 'trackback') && !$args['short_ping'])) {
                        comment_text();
                    } else {
                        comment_text();
                    }
                ?>
            </div>
        </div>
          
    </div>


<?php } ?>

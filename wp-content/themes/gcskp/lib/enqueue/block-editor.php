<?php

/*===== Assets for _themename Block Editor =====*/

function _themename_block_editor_assets() {  

    wp_enqueue_style(
        '_themename-block-editor-styles', 
        get_template_directory_uri() . '/dist/assets/css/editor.css', 
        array(), 
        time(), 
        'all'
    );

}
//add_action( 'enqueue_block_editor_assets', '_themename_block_editor_assets');
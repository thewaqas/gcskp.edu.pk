<?php

/*===== Assets for _themename Back-end or admin =====*/
function _themename_admin_assets() {

    $uri = get_template_directory_uri();

    wp_enqueue_style ( '_themename-admin-stylesheet', $uri. '/dist/assets/css/admin.css', array(), time(), 'all' );
    wp_enqueue_script( '_themename-admin-scripts',    $uri. '/dist/assets/js/admin.js',   array(), time(),  true );

}

<?php

/*===== Assets for _themename customize_preview_js =====*/

function _themename_customize_preview_js() {

    wp_enqueue_script(
        '_themename-customize-preview', 
        get_template_directory_uri() . '/dist/assets/js/customize-preview.js', 
        array('customize-preview', 'jquery'), 
        '1.0.0', 
        true
    );

    // include ( get_template_directory(). '/lib/inline-css.php' );
    
    // wp_localize_script( 
    //     '_themename-customize-preview', 
    //     '_themename', 
    //     array( 
    //         'inline-css' => $inline_styles_selectors 
    //     ) 
    // );

    

}
add_action( 'customize_preview_init', '_themename_customize_preview_js');
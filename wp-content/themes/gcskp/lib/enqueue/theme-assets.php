<?php

/*===== Assets for _themename Front-end =====*/
function _themename_assets() {   

    $uri = get_template_directory_uri();

    // Register and Enque Styles for _themename Theme 
    wp_register_style( '_themename-main',     $uri . '/components/base/base.css',    array(), NULL,   'all' );
    wp_register_style( '_themename-bundled',  $uri . '/dist/assets/css/bundle.css',  array(), time(), 'all' );

    wp_enqueue_style( '_themename-main' );
    wp_enqueue_style( '_themename-bundled' );


    // Register and Enque Scripts for _themename Theme 
    wp_register_script( '_themename-core',   $uri . '/components/base/core.js',     array(), NULL,   false );
    wp_register_script( '_themename-main',   $uri . '/components/base/script.js',   array(), NULL,   false );
    wp_register_script( '_themename-bundle', $uri . '/dist/assets/js/bundle.js',    array(), time(), true  );

    wp_enqueue_script( 'jquery' );
    wp_enqueue_script( '_themename-core' );
    wp_enqueue_script( '_themename-main' );
    wp_enqueue_script( '_themename-bundle' );

    // Inline CSS
    $to_top_color = get_theme_mod( '_themename_to_top_color' );
    wp_add_inline_style( 
        '_themename_custom_color', 
        '.btn-primary { border-color: '. $to_top_color .';background-color: '. $to_top_color .';}' 
    );

}
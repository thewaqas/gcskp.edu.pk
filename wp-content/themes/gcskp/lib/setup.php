<?php

// Register Menu Locations
function _themename_register_menus() {

    register_nav_menus( [
        'primary'   => esc_html__( 'Primary Menu', '_themename' ),
        'secondary' => esc_html__( 'Secondary Menu', '_themename' ),
    ]);
}
<?php

// Add Theme Support
function _themename_theme_support() {

    add_theme_support( 'title-tag' );
    add_theme_support( 'post-thumbnails' );
    add_theme_support( 'post-formats', [ 'aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat' ] );
    add_theme_support( 'html5', [ 'search-form', 'comment-list', 'comment-form', 'gallary', 'caption' ] );
    add_theme_support( 'automatic-feed-links' );
    add_theme_support( 'custom-background' );
    add_theme_support( 'custom-header' );
    add_theme_support( 'custom-logo', [ 'width' => 114, 'height' => 27, 'flex-width' => true, 'flex-height' => true ] );
    add_theme_support( 'customize-selective-refresh-widgets' );

    add_theme_support( 'starter-content', [
        'widgets'                   =>  [
            // Place three core-defined widgets in the sidebar area.
            'ju_sidebar'            =>  [
                'text_business_info', 'search', 'text_about',
            ]
        ],

        // Create the custom image attachments used as post thumbnails for pages.
        'attachments'               =>  [
            'image-about'           =>  [
                'post_title'        =>  __( 'About', 'udemy' ),
                'file'              =>  'assets/images/about/1.jpg', // URL relative to the template directory.
            ],
        ],

        // Specify the core-defined pages to create and add custom thumbnails to some of them.
        'posts'                     =>  [
            'home'                  =>  array(
                'thumbnail'         => '{{image-about}}',
            ),
            'about'                 =>  array(
                'thumbnail'         => '{{image-about}}',
            ),
            'contact'               => array(
                'thumbnail'         => '{{image-about}}',
            ),
            'blog'                  => array(
                'thumbnail'         => '{{image-about}}',
            ),
            'homepage-section'      => array(
                'thumbnail'         => '{{image-about}}',
            ),
        ],

        // Default to a static front page and assign the front and posts pages.
        'options'                   =>  [
            'show_on_front'         => 'page',
            'page_on_front'         => '{{home}}',
            'page_for_posts'        => '{{blog}}',
        ],

        // Set the front page section theme mods to the IDs of the core-registered pages.
        'theme_mods'                =>  [
            'ju_facebook_handle'    =>  'udemy',
            'ju_twitter_handle'     =>  'udemy',
            'ju_instagram_handle'   =>  'udemy',
            'ju_email'              =>  'udemy',
            'ju_phone_number'       =>  'udemy',
            'ju_header_show_search' =>  'yes',
            'ju_header_show_cart'   =>  'yes',
        ],

        // Set up nav menus for each of the two areas registered in the theme.
        'nav_menus'                 =>  [
            // Assign a menu to the "top" location.
            'primary'               =>  array(
                'name'              =>  __( 'Primary Menu', 'udemy' ),
                'items'             =>  array(
                    'link_home', // Note that the core "home" page is actually a link in case a static front page is not used.
                    'page_about',
                    'page_blog',
                    'page_contact',
                ),
            ),

            // Assign a menu to the "social" location.
            'secondary'             =>  array(
                'name'              =>  __( 'Secondary Menu', 'udemy' ),
                'items'             =>  array(
                    'link_home', // Note that the core "home" page is actually a link in case a static front page is not used.
                    'page_about',
                    'page_blog',
                    'page_contact',
                ),
            ),
        ],
    ]);




    add_theme_support( 'align-wide' );  // For gutenberg editor

    add_image_size(    '_themename-blog-image', 1280, 720 );
    
}
add_action( 'after_setup_theme', '_themename_theme_support' ); 
<?php get_header() ?>

    <?php if( have_posts() ) : ?>

        <section class="section section-lg bg-transparent" data-preset='{"title":"Post Single With Sidebar","category":"blog","reload":true,"id":"post-single-with-sidebar"}'>
            <div class="container">
                <h1 class="text-center">student-single.php</h1>
                <div class="row row-40 justify-content-xxl-between">
                    <div class="col-md-12">

                        <?php while ( have_posts() ) : the_post(); ?>

                            <article <?php post_class(); ?>>

                                <?php the_content() ?>

                            </article>

                        <?php endwhile ?>

                    </div>    
                </div>
            </div>
        </section>

    <?php else : ?>

        <?php get_template_part( 'template-parts/posts/content', 'none' ) ?>

    <?php endif ?>


<?php get_footer() ?>
<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '+bxYVh08u4Xo1CAcasBASv3U5jarXZBXjOZ1PidLiADSHg/PnP4J6anDPMyLU5JoC9dM26vQ+T9UiL0DxSjWqw==');
define('SECURE_AUTH_KEY',  'NKeiAqctjEbDlhhzYcDV7LMCvdeXr3vUnRSjxuXXxEWNXSZVvuMR37V2rxVJkzdt1U+13NpaekrspMVapMxkVw==');
define('LOGGED_IN_KEY',    '66uc4mTgXO9Z4v/IRxvNvTpspTKp57r5Ny3u0XiWMObjV5j2mvvK1sVomNwQnuALVzujp98u/xcfGZbF4xCmOA==');
define('NONCE_KEY',        'ewyxT3tChXwn+9zghnM5pkeSKYoJJBzip9FNItQ0dy6g2S4y5FobtttdF/5N8JxcXbQ5QvkjHmDnzCmz6u3jig==');
define('AUTH_SALT',        'fxYeq9tycHTd/1Ccsu1hIb+bhwh0ZIzTxeGqdGc/PaBbxA5hxFjD3zlhR0s4fzohWZdBG9AiBeaxnu+Pdsbv2w==');
define('SECURE_AUTH_SALT', 'MDDAnXKpBdgkU3casjSgLy6iM6+67Z8YtTIvFCmP3dazzOdF+hAbq9SyPURqhjukvaqR0foUfQvvoIapPLDiMA==');
define('LOGGED_IN_SALT',   'rNfVu147CYtXelMkBGsGYNc4Nj0+05W8LyTiFH7ZkONUzD6/ocr6Aly4yaliYDxYMkN/PwjKDMHfxOMjai7RSg==');
define('NONCE_SALT',       'JRxH7/n4GelCrcTGnXgewqEUycEMdh/+7QY8ImLVa7Ae0+PD1bIFwtSTj725GutC50o30kEl5z43kyX9MJoldQ==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
